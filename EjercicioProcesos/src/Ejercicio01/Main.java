package Ejercicio01;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		Process wait;
		ProcessBuilder pb;
		String ruta = "C:\\WINDOWS\\system32\\notepad.exe";
		try {
			pb = new ProcessBuilder(ruta);
			wait = pb.start();
			wait.waitFor();

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Finalizado");

	}

}
