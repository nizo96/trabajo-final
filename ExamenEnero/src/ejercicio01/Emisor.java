package ejercicio01;

public class Emisor extends Thread implements Runnable{
	private String mensajes;
	
	public String getMensajes() {
		return mensajes;
	}

	public void setMensajes(String mensajes) {
		this.mensajes = mensajes;
	}

	public Emisor() {
		
	}
	
	public String mensaje() {
		Integer contador = 1;
		while (true) {
			try {
				mensajes = "Mensaje " + contador++;
				return mensajes;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	
	public void run() {
		synchronized (this) {
			System.out.println(mensaje());
			mensaje();
		}
	}
	
	

}
