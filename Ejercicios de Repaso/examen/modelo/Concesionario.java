package modelo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Concesionario {
	public String direccion;
	public List<Coches> listaCoches;
	
	public Concesionario() {
		
	}
	
	public BigDecimal getMediaEdad() {
		List<Coches> listaCoches = new ArrayList<>();
		BigDecimal suma = new BigDecimal(0);
		for (Coches coches : listaCoches) {
			Period period = coches.getFechaFab().until(LocalDate.now());
			suma.add(new BigDecimal(period.getYears()));
		}
		return suma.divide(new BigDecimal(listaCoches.size()));
	}
	
	public Map<String, Coches> getMapaCoches(){
		Map<String, Coches> mapaCoches = new HashMap<>();
		for (Coches coche : listaCoches) {
			mapaCoches.put(coche.matricula, coche);
		}
		return mapaCoches;
	}
	
	public void borrarAntiguos() {
		for (Coches coche : listaCoches) {
			if (coche.isAntiguo()) {
				listaCoches.remove(coche);
			}
		}
	}

}
