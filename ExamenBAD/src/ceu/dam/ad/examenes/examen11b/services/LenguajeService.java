package ceu.dam.ad.examenes.examen11b.services;

import java.util.List;
import java.util.Map;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;

public interface LenguajeService {

	/** Este servicio debe consultar utilizando el DAO del ejercicio 1 los lenguajes de bbdd que terminen por el filtro indicado. 
	 * Devolver� una lista con esos lenguajes. 
	 * Si la lista es vac�a, lanzar� un NotFoundException
	 * Si hay alg�n error al conectar con la bbdd, lanzar� un AccessDataBaseException
	 */
	public List<Lenguaje> consultarLenguajes(String filtroNombre) throws NotFoundException, AccessDatabaseException;


	/** Este servicio devolver� un mapa de lenguajes donde la key ser� el ID del lenguaje y el VALUE ser� el objeto lenguaje correspondiente.
	 * Se implementar� invocando al m�todo anterior. A continuaci�n, recorrer la lista de resultados (OBLIGATORIAMENTE USANDO UN ITERATOR) y construir un mapa. 
	 * Si el mapa es vac�o, lanzar� un NotFoundException
	 * Si hay alg�n error al conectar con la bbdd, lanzar� un AccessDataBaseException
	 */
	public Map<Long, Lenguaje> consultarMapaLenguajes(String filtroNombre) throws NotFoundException, AccessDatabaseException;

	
	
}
