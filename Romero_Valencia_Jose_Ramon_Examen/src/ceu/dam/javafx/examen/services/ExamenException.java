package ceu.dam.javafx.examen.services;

public class ExamenException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8647246023028628145L;

	public ExamenException() {
		super();
	}

	public ExamenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ExamenException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExamenException(String message) {
		super(message);
	}

	public ExamenException(Throwable cause) {
		super(cause);
	}

	
}
