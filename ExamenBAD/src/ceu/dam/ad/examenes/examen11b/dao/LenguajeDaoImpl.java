package ceu.dam.ad.examenes.examen11b.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;

public class LenguajeDaoImpl implements LenguajeDao {

	private List<Lenguaje> lista;

	@Override
	public List<Lenguaje> consultarLenguajes(Connection conn, String filtroNombre) throws SQLException {
		Statement stmt = null;
		ResultSet rs = null;
		
		lista = new ArrayList<>();
		
		try {
			stmt = conn.createStatement();
			if (filtroNombre != null) {
				rs = stmt.executeQuery("SELECT * FROM lenguage WHERE '" + filtroNombre + "%'");
			} else {
				rs = stmt.executeQuery("SELECT * FROM lenguage ");
			}
			
			while (rs.next()) {
				Lenguaje lengua = new Lenguaje();
				lengua.setId(Long.parseLong(rs.getString("language_id")));
				lengua.setLastUpdate(rs.getDate("last_update"));
				lengua.setName(rs.getString("name"));
				lista.add(lengua);
			}
			return lista;
			
		} finally {
			if (stmt != null) {
				stmt.close();

		}
	}
}
}