package xmlWikipedia;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLWikipedia {
	
	public void crearXmlDeWikipedia (String pathFile) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			Document xml = builder.newDocument();
			
			Element quizTag = xml.createElement("quiz");
			Element qandaTag = xml.createElement("qanda");
			Element questionTag = xml.createElement("question");
			Element answerTag = xml.createElement("answer");
			
			xml.appendChild(quizTag);
			quizTag.appendChild(qandaTag);
			qandaTag.appendChild(questionTag);
			qandaTag.appendChild(answerTag);
			
			qandaTag.setAttribute("seq", "1");
			questionTag.setTextContent("�Como te llamas?");
			answerTag.setTextContent("Blas de los montes");
			
			TransformerFactory factoryTransformer = TransformerFactory.newInstance();
			Transformer transformer = factoryTransformer.newTransformer();
			DOMSource source = new DOMSource(xml);
			File file = new File(pathFile);
			StreamResult stream = new StreamResult(file);
			transformer.transform(source, stream);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
