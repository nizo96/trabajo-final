package ejercicios;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		Integer numero = 0;
		SacoNumeros saconumero = new SacoNumeros();
		
		try {
			do {

				System.out.println("Dame un nmero");
				numero = leer.nextInt();
				if (!numero.equals(-1)) {
					saconumero.addNumeros(numero);
				}
			} while (!numero.equals(-1));

		} finally {
			leer.close();
			System.out.println(saconumero.toString());
		}
		

	}

}
