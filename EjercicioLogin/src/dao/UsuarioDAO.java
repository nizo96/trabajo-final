package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import modelo.Usuario;

public class UsuarioDAO {

	private Usuario u;
	private ConnectorProvider connProv;

	List<Usuario> lista = new ArrayList<>();
	Boolean estado;

	public Usuario login(String usuario, String password, Connection conn) {
		Statement stmt = null;
		ResultSet rs = null;

		Usuario u = null;

		try {
			
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM usuarios");

			while (rs.next()) {
				Usuario usuarios = new Usuario();
				usuarios.setId(rs.getString("id"));
				usuarios.setNombre(rs.getString("nombre"));
				usuarios.setApellidos(rs.getString("apellidos"));
				usuarios.setContrasena(rs.getString("password"));
				usuarios.setUsuario(rs.getString("usuario"));
				lista.add(usuarios);
			}

			for (Usuario usu : lista) {
				if (usu.getUsuario().equals(usuario) && usu.getContrasena().equals(password)) {
					u = usu;
				}
			}
			if (stmt != null) {
				stmt.close();
				conn.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e);
		}

		return u;
	}

	public Usuario modificarUsuario(Usuario usu, Connection conn) throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		u = null;

		try {
			conn = connProv.getNewConnection();
			

			String sql = "UPDATE usuarios SET usuario=?, apellidos=?, password=?, nombre=?" + "WHERE id=?";
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, usu.getUsuario());
			stmt.setString(2, usu.getApellidos());
			stmt.setString(3, usu.getContrasena());
			stmt.setString(4, usu.getNombre());
			stmt.setString(5, usu.getId());
			// seguramente tenga que quitar el id
			stmt.execute();
			rs = stmt.getGeneratedKeys();
			if (stmt.executeUpdate() > 0) {
				JOptionPane.showMessageDialog(null, "Los datos han sido modificados");
				u.setUsuario(rs.getString(1));
				u.setApellidos(rs.getString(2));
				u.setContrasena(rs.getString(3));
				u.setNombre(rs.getString(4));
			} else {
				JOptionPane.showMessageDialog(null, "Los datos no han sido modificados");
				throw new SQLException("Usuario no coincide");
			}

			return u;

		} finally {
			if (stmt != null) {
				stmt.close();
			}

		}

	}

}
