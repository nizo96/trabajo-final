package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import app.App;
import modelo.Usuario;
import services.UsuarioService;

public class InitialView extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2097019113333152608L;
	private JTextField txtUsuario;
	private UsuarioService usuarioBase;
	private Usuario usu;
	private App controller;
	private JPasswordField passwordField;

	public InitialView(App controller) {
		this.controller = controller;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		setBounds(100, 100, 450, 300);
		setLayout(null);

		txtUsuario = new JTextField();
		txtUsuario.setBounds(143, 55, 96, 20);
		add(txtUsuario);
		txtUsuario.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(143, 118, 96, 20);
		add(passwordField);
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton.setBounds(65, 187, 89, 23);
		add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Entrar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				usu = usuarioBase.crearLogin();
				if (usu != null) {
					controller.irAPantallaInter(usu);
				} else {
					JOptionPane.showMessageDialog(null, "Datos incorrectos");
				}

			}
		});
		btnNewButton_1.setBounds(200, 187, 89, 23);
		add(btnNewButton_1);

		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setBounds(84, 58, 49, 14);
		add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Contrase\u00F1a");
		lblNewLabel_1.setBounds(65, 121, 68, 14);
		add(lblNewLabel_1);

	}

	public String getTxtUsuario() {
		return txtUsuario.getText();
	}

	public void setTxtUsuario(String texto) {
		this.txtUsuario.setText(texto);
	}

	public String getPasswordField() {
		return passwordField.getPassword().toString();
	}

	public void setPasswordField(String password) {
		this.passwordField.setText(password);
	}
}
