package examen;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

public abstract class Animal {

	public Integer numeroRegistro;
	public LocalDate fechaNac;
	public BigDecimal peso;
	public BigDecimal pesoMinimo;
	public BigDecimal precioVenta;

	public Integer getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public BigDecimal getPeso() {
		return peso;
	}

	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}

	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public BigDecimal getPesoMinimo() {
		return pesoMinimo;
	}

	public void setPesoMinimo(BigDecimal pesoMinimo) {
		this.pesoMinimo = pesoMinimo;
	}

	public LocalDate getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(LocalDate fechaNac) {
		this.fechaNac = fechaNac;
	}

	public Integer edadMes(LocalDate fecha) {
		if (fecha != fechaNac) {
			Period periodo = fecha.until(fechaNac);
			return periodo.getMonths();
		} else {
			return null;
		}
	}

	public Boolean seVende() {
		Integer comparando = pesoMinimo.compareTo(peso);
		if (comparando >= 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(numeroRegistro);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		return Objects.equals(numeroRegistro, other.numeroRegistro);
	}

}
