package ceu.dam.ad.examenes.examen11b.restServicesClients;

import java.util.List;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;

public interface LenguajeClientRestService {

	/** Servicio REST que debe invocar al servicio REST del ejercicio 3. 
	 * Devolver� una lista con los lenguajes devueltos por el servicio. 
	 * Si el servicio REST devuelve un 404, lanzar� un NotFoundException
	 * Si el servicio REST devuelve un 500, lanzar� un AccessDatabaseException
	 */
	public List<Lenguaje> consultarLenguajes(String filtroNombre) throws NotFoundException, AccessDatabaseException;

	
	
}
