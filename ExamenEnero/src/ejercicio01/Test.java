package ejercicio01;

public class Test {
	public static void main(String[] args) {
		Emisor emisor = new Emisor();
		Receptor receptor1 = new Receptor();
		Receptor receptor2 = new Receptor();
		Receptor receptor3 = new Receptor();
		Receptor receptor4 = new Receptor();
		
		Thread h1 = new Thread(emisor);
		Thread h2 = new Thread(receptor1);
		Thread h3 = new Thread(receptor2);
		Thread h4 = new Thread(receptor3);
		Thread h5 = new Thread(receptor4);
		
		h1.start();
		h2.start();
		h3.start();
		h4.start();
		h5.start();
		
	}

}
