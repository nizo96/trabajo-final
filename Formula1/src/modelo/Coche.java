package modelo;

import java.util.Random;

public class Coche implements Runnable{
	
	private String nombre;
	private Integer velocidadPorSegundo;
	private Integer kmRecorridos;
	private Carrera carrera;
	
	public Coche(String nombre, Integer velocidadPorSegundo, Carrera carrera) {
		super();
		this.nombre = nombre;
		this.velocidadPorSegundo = velocidadPorSegundo;
		this.carrera = carrera;
		kmRecorridos = 0;
	}
	
	public void correrCarrera() {
		do {
			pausar();
			avanzar();
			System.out.println(nombre + " >> va por el km " + kmRecorridos);
		}
		while (kmRecorridos < carrera.getKmTotales());
		System.out.println("Fin de carrera !!");
		carrera.subirAlPodio(nombre);		
	}
	
	public void avanzar() {
		kmRecorridos = kmRecorridos + velocidadPorSegundo;
		if (kmRecorridos > carrera.getKmTotales()) {
			kmRecorridos = carrera.getKmTotales();
		}
	}
	
	public void pausar() {
		try {
			wait(esperar(1000, 3000));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	public Integer esperar(Integer inicio, Integer fin) {
		Random random = new Random();
		return random.nextInt(inicio, fin); 
	}
	@Override
	public void run() {
		synchronized (this) {
			correrCarrera();
		}
		
	}
	


	
	
	
}

