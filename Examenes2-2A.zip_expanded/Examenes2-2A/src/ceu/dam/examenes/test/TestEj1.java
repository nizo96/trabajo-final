package ceu.dam.examenes.test;

import java.util.Scanner;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Hospital;
import ceu.dam.examenes.modelo.Medico;
import ceu.dam.examenes.serviceEj1.HospitalService;
import ceu.dam.examenes.serviceEj1.HospitalServiceImpl;

public class TestEj1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("OJO: Si ya has ejecutado la prueba antes y has insertado datos, recuerda borrarlos con los DELETEs que hay en MOODLE");
			System.out.println("Continuamos?? (S/N)");
			if (!sc.nextLine().equalsIgnoreCase("s")) {
				return;
			}

			
			System.out.println(">>>> Probando método crearHospital()....");
			Hospital hospital = DatosBuilder.getHospital();
			HospitalService service = new HospitalServiceImpl();
			Long id;
			try {
				id = service.crearHospital(hospital);
				if (id!=null) {
					System.out.println(">>>> Prueba OK!! Revisa si se ha insertado todo en BBDD. Id = " + id);
				}
				else {
					System.out.println(">>>> Algo no funciona bien. NO DEVUELVES EL ID CORRECTAMENTE");
					return;
				}
			} catch (ErrorServiceException e) {
				System.out.println(">>>> Algo no funciona bien....");
				e.printStackTrace();
				return;
			}
			System.out.println("Continuamos?? (S/N)");
			if (!sc.nextLine().equalsIgnoreCase("s")) {
				return;
			}
	
			System.out.println("\n\n>>>> Probando método consultarHospital()....");
			try {
				hospital = service.consultarHospital(id);
				System.out.println(hospital);
				System.out.println(">>>> Prueba OK!! Hospital consultado. Revisa que todos los datos están OK");
			} catch (ErrorServiceException | NotFoundException e) {
				System.out.println(">>>> Algo no funciona bien....");
				e.printStackTrace();
				return;
			}
			System.out.println("Continuamos?? (S/N)");
			if (!sc.nextLine().equalsIgnoreCase("s")) {
				return;
			}
			try {
				service.consultarHospital(99999L);
				System.out.println(">>>> No funciona: no lanzas excepción cuando no existe el hospital !!!");
				return;
			} catch (ErrorServiceException e) {
				System.out.println(">>>> Algo no funciona bien....");
				e.printStackTrace();
				return;
			} catch (NotFoundException e) {
				System.out.println(">>>> Prueba OK!! Se lanza excepción cuando no existe el hospital");
			}
			System.out.println("Continuamos?? (S/N)");
			if (!sc.nextLine().equalsIgnoreCase("s")) {
				return;
			}
	
			System.out.println("\n\n>>>> Probando método consultarMedico()....");
			try {
				Medico medico = service.consultarMedico(9000002);
				System.out.println(medico);
				System.out.println(">>>> Prueba OK!! médico consultado. Revisa que todos los datos están OK (debe de ser Blas)");
			} catch (ErrorServiceException | NotFoundException e) {
				System.out.println(">>>> Algo no funciona bien....");
				e.printStackTrace();
				return;
			}
			System.out.println("Continuamos?? (S/N)");
			if (!sc.nextLine().equalsIgnoreCase("s")) {
				return;
			}
			try {
				service.consultarMedico(99999);
				System.out.println(">>>> No funciona: no lanzas excepción cuando no existe el médico !!!");
				return;
			} catch (ErrorServiceException e) {
				System.out.println(">>>> Algo no funciona bien....");
				e.printStackTrace();
				return;
			} catch (NotFoundException e) {
				System.out.println(">>>> Prueba OK!! Se lanza excepción cuando no existe el médico");
			}

		}
		finally {
			sc.close();
		}
	}



}
