package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;

import modelo.City;
import service.NotFoundException;

public class CityDAO {
	
	@GetMapping(value = "/city")
	public List<City> filtrarLista(String descrip, Connection conn) throws NotFoundException{
		List<City> ciudades = new ArrayList<>();
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT CITY FROM CITY WHERE CITY LIKE %" + descrip + "%");

			while (rs.next()) {
				City ciudad = new City();
				ciudad.setCountryId(rs.getLong("COUNTRY_ID"));
				ciudad.setDescripcion(rs.getString("CITY"));
				ciudad.setId(rs.getLong("CITY_ID"));
				ciudades.add(ciudad);
			}	
			return ciudades;
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new NotFoundException();
		}
		
	}
	
	public Integer actualizarCiudad(City ciudadParam, Connection conn){
		List<City> ciudad = new ArrayList<>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			String sql = "UPDATE CITY SET CITY = ?, COUNTRY_ID = ?, CITY_ID = ?"; 
			stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(0, ciudadParam.getDescripcion());
			stmt.setLong(1, ciudadParam.getCountryId());
			stmt.setLong(2, ciudadParam.getId());
			
			rs = stmt.getGeneratedKeys();
			
			return stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e);
			return null;
		} finally {
			
		}
		
		
	}
	
	

}
