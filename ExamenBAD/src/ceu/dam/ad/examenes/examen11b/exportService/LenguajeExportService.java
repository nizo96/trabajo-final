package ceu.dam.ad.examenes.examen11b.exportService;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;

public interface LenguajeExportService {

	
	/** Servicio que debe consultar (utilizando el servicio del ejercio 2) los lenguajes de bbdd que terminen por el filtro indicado. 
	 * Exportar en un fichero CSV todos esos lenguajes
	 * El fichero CSV estar� localizado donde indique el par�metro pathFileName
	 * El caracter separador que utilizaremos para el CSV ser� el punto y coma (";") 
	 * El fichero se exportar� siempre aunque sea vac�o porque no haya lenguajes con el filtro indicado.
	 * Si hay alg�n error al conectar con la bbdd o al escribir el fichero, lanzar� un AccessDataBaseException
	 * (No es necesario formatear la fecha para que aparezca de un modo determinado. El de por defecto de Java estar� bien)
	 */
	public void exportarLenguajes(String filtroNombre, String pathFileName) throws AccessDatabaseException;
}
