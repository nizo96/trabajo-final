package ejercicios;

import java.util.Scanner;

public class Ejercicio32 {

	public static void main(String[] args) {
		Reloj reloj1 = new Reloj();
		Integer horas;
		Integer minutos;
		Integer segundos;
		
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Diga la hora del reloj");
		horas = leer.nextInt();
		
		System.out.println("Indique los minutos");
		minutos = leer.nextInt();
		
		System.out.println("Indique los segundos");
		segundos = leer.nextInt();
		
		reloj1.ponerEnHora(horas, minutos, segundos);
		
		reloj1.setFormato24Horas(false);
		
		reloj1.ponerEnHora(24, 17, 0);
		reloj1.ponerEnHora(21, 82, 0);
		reloj1.ponerEnHora(17, 16, 15);
		
		Reloj reloj2 = new Reloj();
		
		reloj2.ponerEnHora(17, 16, 15);
		
		System.out.println(reloj1.equals(reloj2));
		
		leer.close();
		

	}

}