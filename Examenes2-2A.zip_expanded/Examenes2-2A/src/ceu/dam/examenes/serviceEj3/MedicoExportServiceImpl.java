package ceu.dam.examenes.serviceEj3;

import java.io.File;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Medico;

public class MedicoExportServiceImpl implements MedicoExportService {

	@Override
	public void exportarMedico(Medico medico, String filePath) throws ErrorServiceException {
		try {
			File file = new File(filePath);
			XmlMapper mapper = new XmlMapper();
			mapper.writeValue(file, medico);
		} catch (Exception e) {
			throw new ErrorServiceException("No se encuentra el equipo", e);
		}
	}
}

