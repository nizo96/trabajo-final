package ventanas;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Ventana extends JFrame{

	private static final long serialVersionUID = -46332995728781819L;

	public Ventana() {
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Ejercicio 01");
		addComponentes();
	}

	private void addComponentes() {
		// Establecemos el grid principal de la ventana a 3x2
		GridLayout layoutPrincipal = new GridLayout(3,2);
		getContentPane().setLayout(layoutPrincipal);
		
		// Creamos y aadimos los label del 1 al 5
		for(int i=1; i<6; i++) {
			JLabel label = new JLabel();
			label.setText("Texto " + i);
			add(label);
		}
		
		// Creamos el panel para la sexta celda del grid principal
		JPanel panel = new JPanel();
		// Le ponemos un grid de 2x2
		GridLayout layoutPanel = new GridLayout(2,2);
		panel.setLayout(layoutPanel);
		// Aadimos el panel secundario dentro del principal
		add(panel);

		// Creamos y aadimos los 4 labels al panel secundario
		for(int i=1; i<5; i++) {
			JLabel label = new JLabel();
			label.setText("Texto 6-" + i);
			panel.add(label);
		}
		
	}
	
}
