package ejercicios;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		String palabra; 
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Ponga una palabra");
		palabra = leer.nextLine();
		
		String[] split = palabra.split("");
		
		for (int i = split.length - 1; i >= 0; i--) {
			System.out.println(split[i]);
		}
		leer.close();

	}

}
