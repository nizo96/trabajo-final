package gui;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TablaView extends AbstractView{

	/**
	 * 
	 */
	private static final long serialVersionUID = -211178977097611925L;
	private JTable table;
	private String[] coche = {"Marca", "Modelo", "A�o", "Matricula", "Precio"};
	private AppController app;

	/**
	 * Create the application.
	 */
	public TablaView(AppController app) {
		setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(49, 73, 354, 184);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		TableModel modelo = new TableModel();
		table.setModel(modelo);
		
		JLabel lbMarca = new JLabel("Marca");
		lbMarca.setBounds(78, 34, 46, 14);
		add(lbMarca);
		
		JComboBox<String> comboMarcas = new JComboBox<>();
		comboMarcas.setModel(new DefaultComboBoxModel<String>(new String[] {"Peugeot", "Renault", "Seat", "Toyota"}));
		comboMarcas.setBounds(134, 30, 115, 22);
		add(comboMarcas);
		
		JButton btConsultar = new JButton("Consultar");
		btConsultar.setBounds(284, 30, 89, 23);
		add(btConsultar);
		
		JButton btSesion = new JButton("Cerrar Sesion");
		btSesion.setBounds(306, 268, 97, 23);
		add(btSesion);
		initialize();
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}
}
