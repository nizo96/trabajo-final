package ejercicios;

import java.util.Scanner;

public class Ejercicio19 {

	public static final String TIPO_IVA_NORMAL = "NORMAL";
	public static final String TIPO_IVA_REDUCIDO = "REDUCIDO";
	public static final String TIPO_IVA_SUPERREDUCIDO = "SUPERREDUCIDO";
	public static final String TIPO_IVA_EXENTO = "EXENTO";
	
	public static final double PORCENTAJE_IVA_NORMAL = 21.0;
	public static final double PORCENTAJE_IVA_REDUCIDO = 10.0;
	public static final double PORCENTAJE_IVA_SUPERREDUCIDO = 4.0;
	public static final double PORCENTAJE_IVA_EXENTO = 0.0;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dime el precio sin IVA del artculo");
		Double precioSinIva = scanner.nextDouble();
		scanner.nextLine();
		System.out.println("Dime el tipo de IVA: NORMAL, REDUCIDO, SUPERREDUCIDO, EXENTO");
		String tipoIva = scanner.nextLine();
		
		Double porcentaje = null;

		switch (tipoIva) {
		case TIPO_IVA_NORMAL:
			porcentaje = PORCENTAJE_IVA_NORMAL;
			break;
		case TIPO_IVA_REDUCIDO:
			porcentaje = PORCENTAJE_IVA_REDUCIDO;
			break;
		case TIPO_IVA_SUPERREDUCIDO:
			porcentaje = PORCENTAJE_IVA_SUPERREDUCIDO;
			break;
		case TIPO_IVA_EXENTO:
			porcentaje = PORCENTAJE_IVA_EXENTO;
			break;

		default:
			System.out.println("El tipo de IVA indicado es incorrecto.");
			break;
		}
		
		
		if (porcentaje != null) {
			Double iva = precioSinIva * porcentaje / 100;
			Double precioConIva = precioSinIva + iva;
			System.out.println("El iva es " + iva);
			System.out.println("El precio con iva es " + precioConIva);
		}

		scanner.close();
	}
	
	
}
