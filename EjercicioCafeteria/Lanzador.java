

public class Lanzador {

	public static void main(String[] args) {
		Cafeteria cafeteria = new Cafeteria();
		
		for (int i = 0; i < 100; i++) {
			
			Alumno alumno = new Alumno(cafeteria);
			Thread hilo = new Thread(alumno);
			hilo.start();
			
			if (i < 20) {
				Profesor profesor = new Profesor(cafeteria);
				Thread hilo1 = new Thread(profesor);
				hilo1.start();
				
			}
		}
		
	}

}
