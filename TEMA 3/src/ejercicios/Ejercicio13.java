package ejercicios;

public class Ejercicio13 {

	public static void main(String[] args) {
		Integer [][] multiplicar = new Integer [10][10];
		
		for (int i = 0; i < multiplicar.length; i++) {
			for (int j = 0; j < multiplicar[i].length; j++) {
				multiplicar[i][j] = (i + 1) * (j + 1);
			}
			
		}
		
		for (int i = 0; i < multiplicar.length; i++) {
			for (int j = 0; j < multiplicar[i].length; j++) {
				System.out.print(multiplicar[i][j] + "\t");
			}
			System.out.println();
		}
		
		

	}

}
