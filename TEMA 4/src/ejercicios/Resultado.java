package ejercicios;

import java.util.Objects;

public class Resultado {
	private Integer golesLocal;
	private Integer golesVisitante;
	
	public Resultado() {
		this.golesLocal = 0;
		this.golesVisitante = 0;
	}
	
	public Boolean isVictoriaLocal() {
		if (golesLocal>golesVisitante) {
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean isVictoriaVisitante() {
		if (golesVisitante>golesLocal) {
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean isEmpate() {
		if (golesVisitante == golesLocal) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(golesLocal, golesVisitante);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resultado other = (Resultado) obj;
		return Objects.equals(golesLocal, other.golesLocal) && Objects.equals(golesVisitante, other.golesVisitante);
	}

	@Override
	public String toString() {
		return golesLocal + "-" + golesVisitante;
	}

	public Integer getGolesLocal() {
		return golesLocal;
	}

	public void setGolesLocal(Integer golesLocal) {
		this.golesLocal = golesLocal;
	}

	public Integer getGolesVisitante() {
		return golesVisitante;
	}

	public void setGolesVisitante(Integer golesVisitante) {
		this.golesVisitante = golesVisitante;
	}
	
	

}
