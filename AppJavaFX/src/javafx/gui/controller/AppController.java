package javafx.gui.controller;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppController {
	private static Stage primaryStage;
	
	public AppController() {
		
	}
	
	public AppController(Stage stage) {
		primaryStage = stage;
	}
	
	public void cambiarVista(String fxml) {
		try {
			URL url = new URL(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			Scene scene = new Scene(loader.load());
			primaryStage.setScene(scene);
			
		} catch (IOException e) {
			throw new RuntimeException("No se ha podido cargar fxml con la ruta" + fxml, e);
		}
	}
	
	

}
