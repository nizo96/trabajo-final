/**
 * 
 */
/**
 * @author TECH
 *
 */
module TrabajoFinalInterfaces {
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.controls;
	requires org.apache.logging.log4j;
	requires org.mongodb.bson;
	requires org.mongodb.driver.core;
	requires org.mongodb.driver.sync.client;
}