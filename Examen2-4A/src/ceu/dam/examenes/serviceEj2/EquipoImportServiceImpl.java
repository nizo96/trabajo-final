package ceu.dam.examenes.serviceEj2;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Entrenador;
import ceu.dam.examenes.modelo.Equipo;
import ceu.dam.examenes.modelo.Jugador;

public class EquipoImportServiceImpl implements EquipoImportService {

	@Override
	public List<Equipo> importar(String filePath) throws ErrorServiceException {
		// TODO: TRABAJO POR HACER DEL ALUMNO...
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			File file = new File(filePath);
			Document xml = builder.parse(file);

			Equipo equipo = new Equipo();
			List<Equipo> equipos = new ArrayList<>();

			Element equipoTag = xml.getDocumentElement();
			equipo.setJugadores(new ArrayList<>());

			NodeList equipoTagList = equipoTag.getElementsByTagName("articulos");
			for (int i = 0; i < equipoTagList.getLength(); i++) {
				equipo.setCodigo(equipoTag.getAttribute("codigo"));
				Entrenador entrenador = new Entrenador();
				Element entrenadorTag = (Element) equipoTag.getElementsByTagName("entrenador");
				entrenador.setId(Long.parseLong(entrenadorTag.getAttribute("id")));
				Element nombreTag = (Element) entrenadorTag.getElementsByTagName("nombre");
				entrenador.setNombre(nombreTag.getTextContent());
				Element sueldoTag = (Element) entrenadorTag.getElementsByTagName("sueldo");
				entrenador.setSueldo(new BigDecimal(sueldoTag.getTextContent()));
				equipo.setEntrenador(entrenador);
				Element jugadoresTag = (Element) equipoTag.getElementsByTagName("jugadores");
				List<Jugador> jugadorList = new ArrayList<>();
				NodeList jugadorTagList = jugadoresTag.getElementsByTagName("jugador");
				for (int j = 0; j < jugadorTagList.getLength(); j++) {
					Jugador jugador = new Jugador();
					Element jugadorTag = (Element) jugadorTagList.item(j);

					Element dorsalTag = (Element) jugadorTag.getElementsByTagName("dorsal");
					jugador.setDorsal(Integer.parseInt(dorsalTag.getTextContent()));

					Element apellidoTag = (Element) jugadorTag.getElementsByTagName("apellido");
					jugador.setNombre(apellidoTag.getTextContent());

					jugadorList.add(jugador);
				}
				equipo.setJugadores(jugadorList);
			}
			equipos.add(equipo);
			return equipos;

		}
		
		catch (Exception e) {
			e.printStackTrace();
			throw new ErrorServiceException("No se ha podido importar el XML", e);
	}
}

}
