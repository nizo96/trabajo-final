package ejercicios;

public class Semaforo {
	private String color;
	private Boolean parpadeando;

	public static final String ROJO = "ROJO";
	public static final String VERDE = "VERDE";
	public static final String AMBAR = "AMBAR";

	public Semaforo() {
		this.color = ROJO;
		this.parpadeando = false;
	}

	@Override
	public String toString() {
		switch (this.color) {
		case ROJO:
			return "Semaforo en color" + ROJO;
		case VERDE:
			return "Semaforo en color" + VERDE;
		case AMBAR:
			if (this.parpadeando) {
				return "Semaforo en color" + AMBAR + "parpadeando";
			} else {
				return "Semaforo en color" + AMBAR;
			}

		default:
			return "Semaforo sin color";
		}
	}

	public void cambiarEstado() {
		if (this.color.equals(VERDE)) {
			this.color = AMBAR;
			this.parpadeando = true;
		}
		if ((this.color.equals(AMBAR)) && (this.parpadeando)) {
			this.color = AMBAR;
			this.parpadeando = false;
		}
		if ((this.color.equals(AMBAR)) && !(this.parpadeando)) {
			this.color = ROJO;
		}
		if (this.color.equals(ROJO)) {
			this.color = VERDE;
		}
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		if ((color == ROJO) || (color == VERDE) || (color == AMBAR)) {
			this.color = color;
		}
	}

	public Boolean getParpadeando() {
		return parpadeando;
	}

	public void setParpadeando(Boolean parpadeando) {
		if (this.color.equals(AMBAR)) {
			this.parpadeando = parpadeando;
		}
	}

}
