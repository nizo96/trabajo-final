package modelo;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Libreria {
	private String nombre;
	private Map<Libro, String> libros;

	public Libreria() {
		libros = new HashMap<>();
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Libreria other = (Libreria) obj;
		return Objects.equals(nombre, other.nombre);
	}
	
	public BigDecimal getTasacionCompleta() {
		BigDecimal suma = new BigDecimal(0);
		Set<Libro> keys = libros.keySet();
		for (Libro key : keys) {
			suma = key.getPrecio().add(suma);
			
		}
		return suma;
	}
	
	public BigDecimal getPrecioMedio() {
		BigDecimal media = new BigDecimal(0);
		Set<Libro> keys = libros.keySet();
		for (Libro key : keys) {
			media = key.getPrecio().add(media);	
		}
		media = media.divide(media, keys.size(), RoundingMode.HALF_DOWN);
		return media;
	}
	public List<Libro> getLibrosBaratos() {
		Set<Libro> keys = libros.keySet();
		List<Libro> libro1 = new ArrayList<>();
		for (Libro key : keys) {
			if ((key.getPrecio()).equals(10)) {
				libro1.add(key);
			}
		}
		return libro1;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Map<Libro, String> getLibros() {
		return libros;
	}

	public void setLibros(Map<Libro, String> libros) {
		this.libros = libros;
	}

}
