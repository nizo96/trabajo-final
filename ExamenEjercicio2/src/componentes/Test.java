package componentes;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Test extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7377431923104660976L;
	private JPanel contentPane;
	private JIntegerFieldButtonImpl intFieldButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nota de mi examen:");
		lblNombre.setBounds(51, 65, 124, 34);
		contentPane.add(lblNombre);
		
		intFieldButton = new JIntegerFieldButtonImpl(5, 0, 20);
		intFieldButton.setBounds(185, 65, 86, 34);
		contentPane.add(intFieldButton);
		
		JButton btnActivar = new JButton("Cambiar m�nimo a 5");
		btnActivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				intFieldButton.setMinValue(5);
			}
		});
		btnActivar.setBounds(102, 140, 183, 23);
		contentPane.add(btnActivar);
		
		JButton btnDesactivar = new JButton("Cambiar m�ximo a 10");
		btnDesactivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				intFieldButton.setMaxValue(10);
			}
		});
		btnDesactivar.setBounds(102, 169, 183, 23);
		contentPane.add(btnDesactivar);
		
		JButton btnGetText = new JButton("Get Value");
		btnGetText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(intFieldButton.getValue());
			}
		});
		btnGetText.setBounds(102, 204, 183, 23);
		contentPane.add(btnGetText);
	}
	
	
}
