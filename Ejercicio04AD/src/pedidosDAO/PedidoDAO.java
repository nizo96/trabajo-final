package pedidosDAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import modelo.Pedido;

public class PedidoDAO {
	
	public void insertarTablaPedido(Connection conn, Pedido pedido) {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO PEDIDO VALUES(?, ?, ?, ?)");
			stmt.setString(1, pedido.getIdPedido());
			stmt.setDate(2, Date.valueOf(pedido.getFechaEntrega()));
			stmt.setDate(3, Date.valueOf(pedido.getFechaPedido()));
			stmt.setString(4, pedido.getCliente());
			stmt.execute();
			
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			
		}
	}

}
