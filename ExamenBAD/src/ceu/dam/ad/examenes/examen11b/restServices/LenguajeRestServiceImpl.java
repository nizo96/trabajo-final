package ceu.dam.ad.examenes.examen11b.restServices;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;
import ceu.dam.ad.examenes.examen11b.services.LenguajeServiceImpl;

public class LenguajeRestServiceImpl implements LenguajeRestService{
	private LenguajeServiceImpl service;
	private List<Lenguaje> lista;

	@Override
	@GetMapping("/lenguaje")
	public List<Lenguaje> consultarLenguajes(@RequestParam String filtroNombre) throws NotFoundException, AccessDatabaseException {
		// No sabia si poner que fuera requerido el RequestParam, asi que mejor lo dejo vacio, tampoco se especifica
		service = new LenguajeServiceImpl();
		lista = new ArrayList<>();
		try {
			lista = service.consultarLenguajes(filtroNombre);
			if (lista == null) {
				throw new NotFoundException("404 NOT FOUND");
			}
			return lista;
			
		} catch (AccessDatabaseException e) {
			throw new AccessDatabaseException("ERROR 500");
		}
	}

}
