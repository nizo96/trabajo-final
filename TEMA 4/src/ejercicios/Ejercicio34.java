package ejercicios;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Ejercicio34 {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		List<String> cadenas = new ArrayList<>();
		
		for (int i = 0; i < 5; i++) {
			System.out.println("Dime una palabra");
			String cadena = leer.nextLine();
			cadenas.add(cadena);
		}
		System.out.println(cadenas);
		
		for (int i = 0; i < cadenas.size(); i++) {
			String cadena = cadenas.get(i);
			String cadenaMay = cadena.toUpperCase();
			cadenas.set(i, cadenaMay);
		}
		
		System.out.println(cadenas);
		
		if (cadenas.contains("")) {
			System.out.println("Hay alguna cadena vacia en la lista");
		}
		Iterator<String> it = cadenas.iterator();
		while (it.hasNext()) {
			if (it.next().length()<6) {
				it.remove();
			}
		}
		System.out.println(cadenas);
		
		leer.close();

	}

}
