package ejercicios;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Alumno extends Persona {

	@Override
	public String toString() {
		return "Alumno [getCurso()=" + getCurso() + ", getNota()=" + getNota() + ", getDni()=" + getDni()
				+ ", getNombre()=" + getNombre() + ", getEdad()=" + getEdad() + "]";
	}

	private String dni;
	private Integer nota;
	private Curso curso;

	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumno other = (Alumno) obj;
		return Objects.equals(dni, other.dni);
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Alumno(String  dni) {
		this.dni = dni.toUpperCase();
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni.toUpperCase();
	}

	public String aprobar() {
		if (this.nota >= 5) {
			return "aprobado";
		} else {
			return "suspenso";
		}

	}
	
	public Boolean validarDni() {
		if (dni != null && dni.length() == 9) {
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean validarDni2() {
		Pattern patron = Pattern.compile("[0-9]{7,8}[A-Z]");
		Matcher match = patron.matcher(dni);
		if(dni != null && match.matches()) {
		 return true;
		}
		else {
		 return false;
		} 
		
	}
	public Boolean validar() {
		if ((validarDni()) && (curso != null) && (getNombre() != null && getNombre().length()>= 10) && ((getEdad() >= 12 && getEdad() <= 65) && getEdad() != null)) {
			return true;
		} else {
			return false;
		}
	}

}
