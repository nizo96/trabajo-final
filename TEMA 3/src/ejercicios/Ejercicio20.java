package ejercicios;

import java.util.Scanner;

public class Ejercicio20 {

	public static void main(String[] args) {
		String[] palabras = new String[] { "azulejo", "tenedor", "saltamontes", "carretilla", "molinero", "sofisticado",
				"terremoto", "culinario", "teclado", "primavera" };
		Integer contador = 10;
		String letraUsu;
		Boolean verificar = true;
		Scanner leer = new Scanner(System.in);

		int numeroAleatorio = (int) (Math.random() * 10);
		String[] palabraElegida = palabras[numeroAleatorio].split("");

		String[] mostrarAhor = new String[palabraElegida.length];

		mostrarAhor[0] = palabraElegida[0];

		for (int i = 1; i < mostrarAhor.length; i++) {
			mostrarAhor[i] = "_";
		}
		System.out.println("Que comience el juego");
		System.out.println("Esta es tu palabra para esta partida, solo tienes 10 intentos");
		System.out.println("");
		System.out.println("");
		do {
			Boolean prueba;
			Boolean vidas = false;
			verificar = true;

			for (int i = 0; i < mostrarAhor.length; i++) {
				System.out.print(mostrarAhor[i] + " ");
			}

			System.out.println("");
			System.out.println("");
			System.out.println("Diga una letra: ");
			letraUsu = leer.nextLine();

			for (int i = 0; i < palabraElegida.length; i++) {
				if (palabraElegida[i].contains(letraUsu)) {
					mostrarAhor[i] = palabraElegida[i];
					vidas = true;
				}
				
			}
			if (vidas) {
				System.out.println(
						"Enhorabuena, has acertado y no has pedido ninguna vida. " + contador + " vidas te quedan");
			} else {
				contador--;
				System.out.println("Esa palabra no es, prueba con otra, te quedan " + contador + " vidas");
			}

			for (int i = 0; i < mostrarAhor.length; i++) {
				if (mostrarAhor[i].equals(palabraElegida[i])) {
					verificar = true;
				} else {
					verificar = false;
				}
				
			}
			
			if (verificar) {
				break;

			} else {
				verificar = false;
			}
			

		} while (contador != 0 && !verificar);

		if (contador == 0) {
			System.out.println("Lo siento has perdido");
			System.out.println(" ");
			System.out.println(" ");
			System.out.println("La palabra era: ");
			for (int i = 0; i < mostrarAhor.length; i++) {
				System.out.print(palabraElegida[i]);
			}
			
			
		} else {
			for (int i = 0; i < mostrarAhor.length; i++) {
				System.out.print(mostrarAhor[i]);
			}
			System.out.println(" ");
			System.out.println(" ");
			System.out.println("Enhorabuena, eres una maquina");

		}
	}
}