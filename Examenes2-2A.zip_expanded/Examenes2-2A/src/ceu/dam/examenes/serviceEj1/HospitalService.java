package ceu.dam.examenes.serviceEj1;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Hospital;
import ceu.dam.examenes.modelo.Medico;

public interface HospitalService {

	
	/** El método recibe un hospital con todos sus datos rellenos. Devuelve el id del hospital creado.
	 * Tendrá que guardarlo en base de datos, y sus entidades asociadas.
	 * Si hay algún error, lanzará ErrorServiceException
	 */
	public Long crearHospital (Hospital hospital) throws ErrorServiceException;

	
	
	/** El método recibe un identificador de hospital. Tendrá que consultarlo en base de datos
	 * y devolver un objeto Hospital con todas sus entidades rellenas.
	 * Si no existe el hospital con ese ID, lanzará NotFoundException.
	 * Si hay algún error, lanzará ErrorServiceException
	 */
	public Hospital consultarHospital (Long id) throws NotFoundException, ErrorServiceException;

	
	

	/** El método recibe el número de colegiado de un médico/a. Tendrá que consultarlo en base de datos
	 * y devolver un objeto Medico con su entidad Hospital rellena.
	 * Si no existe el médico/a con ese ID, lanzará NotFoundException.
	 * Si hay algún error, lanzará ErrorServiceException
	 */
	public Medico consultarMedico (Integer numColegiado) throws NotFoundException, ErrorServiceException;

	
}
