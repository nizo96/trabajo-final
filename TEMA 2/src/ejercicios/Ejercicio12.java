package ejercicios;

import java.util.Scanner;

public class Ejercicio12 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String usuario;
		Integer longitud;
		Boolean contieneEspacios;
		do {
			System.out.println("Dame un usuario");
			usuario = scanner.nextLine();
			
			usuario = usuario.trim();
			
			usuario = usuario.toUpperCase();
			
			longitud = usuario.length();
			contieneEspacios = usuario.contains(" ");
			if (longitud < 10) {
				System.out.println("La longitud es menor a 10");
			}
			if (contieneEspacios) {
				System.out.println("No puede contener espacios");
			}
			
		}
		while(longitud<10 || contieneEspacios);
		
		System.out.println("Todo OK. Tu username es " + usuario);
		
	}
}
