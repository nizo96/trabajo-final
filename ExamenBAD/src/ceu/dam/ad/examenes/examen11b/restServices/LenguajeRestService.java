package ceu.dam.ad.examenes.examen11b.restServices;

import java.util.List;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;

public interface LenguajeRestService {

	/** Servicio REST que debe consultar (utilizando el servicio del ejercio 2) los lenguajes de bbdd que terminen por el filtro indicado. 
	 * Devolver� una lista con esos lenguajes. 
	 * Si la lista es vac�a, lanzar� un 404 (NOT_FOUND)
	 * Si hay alg�n error al conectar con la bbdd, lanzar� un 500 (INTERNAL_SERVER_ERROR)
	 * Se invocar� mediante un GET a la url /lenguajes
	 * El par�metro filtro se indicar� como par�metro en la URL (no como path, sino como request param) 
	 */
	public List<Lenguaje> consultarLenguajes(String filtroNombre) throws NotFoundException, AccessDatabaseException;

	
	
}
