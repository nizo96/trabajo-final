package ejercicios;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class SacoNumeros {
	public List<Integer> numeros;

	public SacoNumeros() {
		numeros = new ArrayList<>();
	}

	public void addNumeros(Integer numero) {
		numeros.add(numero);
	}

	public Integer getNumero(Integer posicion) {
		try {
			return numeros.get(posicion);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}

	}

	public BigDecimal dividir() {
		BigDecimal resultado = new BigDecimal(numeros.get(0));
		try {
			for (int i = 0; i < numeros.size(); i++) {
				BigDecimal siguiente = new BigDecimal(numeros.get(i));
				resultado = resultado.divide(siguiente, 2, RoundingMode.HALF_UP);
			}
			return resultado;
		}
		catch (ArithmeticException e) {
			return BigDecimal.ZERO;
		}
		finally {
			System.out.println("Division completa");
		}
	}

	@Override
	public String toString() {
		return numeros.toString();
	}

}
