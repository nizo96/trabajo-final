package ejercicios;

public class Ejercicio38 {

	public static void main(String[] args) {
		PilaCadenas pila = new PilaCadenas();
		System.out.println(pila);

		pila.aņadirCadena("primero");
		pila.aņadirCadena("segundo");
		System.out.println(pila);

		String elemento = pila.sacarCadena();
		System.out.println(elemento);
		System.out.println(pila);

		pila.aņadirCadena("tercero");
		System.out.println(pila);

		while (pila.getTamao() > 0) {
			elemento = pila.sacarCadena();
			System.out.println(elemento);
		}
		System.out.println(pila);

		pila.aņadirCadena("cuarto");
		System.out.println(pila);

	}

}
