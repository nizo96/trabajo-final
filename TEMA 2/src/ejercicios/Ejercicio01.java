package ejercicios;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		int a�o;
		
		Scanner leer = new Scanner(System.in);
		
		System.out.println("�En que a�o naciste?");
		
		a�o = leer.nextInt();
		
		if ((a�o >= 1883) && (a�o <= 1900)) {
			System.out.println("Eres de la generaci�n perdida");
		}
		else if ((a�o >= 1901) && (a�o <= 1927)) {
			System.out.println("Eres de la generaci�n grandiosa");	
		}
		
		else if ((a�o >= 1928) && (a�o <= 1945)) {
			System.out.println("Eres de la Generaci�n silenciosa");
		}
		
		else if ((a�o >= 1946) && (a�o <= 1964)) {
			System.out.println("Eres parte de los Baby Boomers");
		}
		
		else if ((a�o >= 1965) && (a�o <= 1980)) {
			System.out.println("Eres parte de la Generaci�n X");
		}
		else if ((a�o >= 1981) && (a�o <= 1996)) {
			System.out.println("Eres parte de la Generaci�n Y");
		}
		else if ((a�o >= 1997) && (a�o <= 2012)) {
			System.out.println("Eres parte de la Generaci�n Z");
		}
		else if (a�o >= 2013) {
			System.out.println("Eres parte de la Generaci�n ALFA");
		}

	}

}
