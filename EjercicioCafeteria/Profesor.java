import java.util.Random;

public class Profesor implements Runnable {

	private Cafeteria cafeteria;
	
	public Profesor(Cafeteria cafeteria) {
		this.cafeteria = cafeteria;
	}
	
	public void consumir() {
		
		while (true) {
			try {
				wait(hiloEspera(1000, 3001));
				if (cafeteria.haySitio()) {
					cafeteria.entraPersona();
					wait(hiloEspera(2000,5001));
					cafeteria.salePersona();
				} else {
					System.out.println("No hay ningun sitio");
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println("Error cafeteria");
			}
		}
		
	}
	
	public Integer hiloEspera(Integer inicio, Integer fin) {
		Random random = new Random();
		return random.nextInt(inicio, fin);
	}
	
	public void run() {
		synchronized (this) {
			consumir();
		}
	}
}
