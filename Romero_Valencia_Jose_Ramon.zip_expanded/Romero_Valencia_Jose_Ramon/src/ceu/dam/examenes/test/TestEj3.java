package ceu.dam.examenes.test;

import java.util.Scanner;

import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.serviceEj3.EquipoExportService;
import ceu.dam.examenes.serviceEj3.EquipoExportServiceImpl;

public class TestEj3 {

	// EL FICHERO SE GUARDARÁ AQUÍ. TIENE QUE EXISTIR ESTA RUTA:
	private static final String RUTA_XML_TEST = "c:/temporal";
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("Vamos a probar a consultar el equipo con código RM en la BBDD. Si no existe o no tienes terminado el ej1, esto no va a funcionar. ¿Continuamos?  (S/N)" );
			if (!sc.nextLine().equalsIgnoreCase("S")) {
				return;
			}
			System.out.println("El XML de exportación se va a generar en esta ruta: (" + RUTA_XML_TEST + "). Revisa que exista la carpeta. El fichero no tiene que existir. ¿Continuamos?  (S/N)" );
			if (!sc.nextLine().equalsIgnoreCase("S")) {
				return;
			}
			EquipoExportService service = new EquipoExportServiceImpl();
			service.exportar(RUTA_XML_TEST+"/xml-ejemplo-exportar.xml", "RM");
			System.out.println("\n>> Revisa que el fichero se haya creado correctamente y tenga todos los datos del equipo RM que hay en BBDD. ");
			System.out.println(">>¿Está todo OK y quieres continuar con más pruebas? (S/N)");
			if (!sc.nextLine().equalsIgnoreCase("S")) {
				return;
			}
			System.out.println(">>>> Probando a consultar equipo que NO existe con código = BLAS ");
			try {
				service.exportar(RUTA_XML_TEST+"/noDebeExistir.xml", "BLASBLAU");
				System.out.println(">>>> No estás lanzando error para un equipo que no existe. Revisa tu código !!!");
			}
			catch(NotFoundException e) {
				System.out.println(">>>> Error controlado correctamente para equipo inexistente. ");
				System.out.println(">>>> Revisa que no se haya creado ningún fichero XML con nombre \"noDebeExistir.xml\". Si es así, tendrías que corregirlo.");
				System.out.println("Si no se ha creado ningún fichero adicional... TODAS LAS PRUEBAS OK !!");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Algo no va bien.... Revisa tu código!!");
		}
		finally {
			sc.close();
		}
	}

}
