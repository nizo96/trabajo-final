package ejercicio02;

import java.util.concurrent.Semaphore;

public class Hilo implements Runnable{
	private Semaphore semaforo;
	
	public Hilo(Semaphore semaforo) {
		this.semaforo = semaforo;
	}

	@Override
	public void run() {
		try {
			semaforo.acquire();
			System.out.println(Thread.currentThread().getName() + " Entra el hilo");
			Thread.sleep(2000);
			semaforo.release();
			System.out.println(Thread.currentThread().getName() + " Sale el hilo");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	

}
