package modelo;

public class Articulo {
	
	public Integer id;
	public String sku;
	public String codigoBarras;
	public Ubicaciones ubicacion;
	
	public Articulo() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public Ubicaciones getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Ubicaciones ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	

}
