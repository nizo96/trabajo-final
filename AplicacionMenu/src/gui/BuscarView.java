package gui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import modelo.City;
import servicio.NotFoundException;

public class BuscarView extends AbstractView {

	/**
	 * Launch the application.
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtBuscar;
	private JTable table;
	private TableModel modelo;
	private List<City> lista;
	
	public BuscarView(AppController appController) {
		setLayout(null);
		
		lista = new ArrayList<>();
		JLabel lbBuscar = new JLabel("Buscar");
		lbBuscar.setBounds(101, 11, 49, 14);
		add(lbBuscar);
		
		txtBuscar = new JTextField();
		txtBuscar.setBounds(160, 8, 96, 20);
		add(txtBuscar);
		txtBuscar.setColumns(10);
		
		JButton btBuscar = new JButton("Buscar");
		btBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					lista = appController.buscar(txtBuscar.getText());
					modelo.setDatos(lista);
				} catch (NotFoundException e1) {
					appController.mostrarError("No existe");
				}
				modelo.fireTableDataChanged();
			}
		});
		btBuscar.setBounds(266, 7, 89, 23);
		add(btBuscar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 45, 430, 244);
		add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		modelo = new TableModel();
		table.setModel(modelo);
		
		
	}
	
	@Override
	public void initialize() {
		
	}
}
