package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import modelo.Libro;

public class LibreriaDAO {
	
	public LibreriaDAO(ConexionNueva conexion){
		conexion = new ConexionNueva();
	}
	
	
	public void insertarLibroNuevo(Connection conn, Libro libro) throws SQLException{
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("INSERT INTO LIBROS VALUES(?,?,?,?)");
			stmt.setString(1, libro.getTitulo());
			stmt.setString(2, libro.getAutor());
			stmt.setString(3, libro.getIsbn());
			stmt.setBigDecimal(4, libro.getPrecio());
			stmt.execute();
			
		} finally {
			if (stmt != null) {
				stmt.close();
				
			}
		}
	}
	
	public Boolean actualizarPrecioLibroNuevo(Connection conn, Libro libro) throws SQLException{
		PreparedStatement stmt = null;
		Boolean isUpdate = false;
		try {
			stmt = conn.prepareStatement("UPDATE LIBROS SET PRECIO =? WHERE UPPER(ISBN) =?");
			stmt.setBigDecimal(1, libro.getPrecio());
			stmt.executeUpdate();
			
			if (stmt.executeUpdate() == 1) {
				isUpdate = true;
			} else {
				isUpdate = false;
			}
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return isUpdate;
	}

}
