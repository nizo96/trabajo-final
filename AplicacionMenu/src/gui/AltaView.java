package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import modelo.City;
import servicio.NotFoundException;

public class AltaView extends AbstractView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5755912303581351080L;
	private JTextField txtId;
	private JTextField txtCiudad;
	private JTextField txtIdPais;

	/**
	 * Create the application.
	 * 
	 * @param appController
	 */
	public AltaView(AppController appController) {
		setLayout(null);
		txtId = new JTextField();
		txtId.setBounds(171, 41, 86, 20);
		add(txtId);
		txtId.setColumns(10);
		txtId.setEditable(false);
		txtId.setText("");

		txtCiudad = new JTextField();
		txtCiudad.setBounds(171, 91, 86, 20);
		add(txtCiudad);
		txtCiudad.setColumns(10);
		txtCiudad.setText("");

		txtIdPais = new JTextField();
		txtIdPais.setBounds(171, 136, 86, 20);
		add(txtIdPais);
		txtIdPais.setColumns(10);
		txtIdPais.setText("");

		JButton btSalvar = new JButton("Salvar");
		btSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				City city = new City();
				try {
					city = appController.salvar();
					txtId.setText(city.getId().toString());
				} catch (NotFoundException e1) {
					e1.printStackTrace();
				}
				txtCiudad.requestFocus();
			}
		});
		btSalvar.setBounds(171, 202, 89, 23);
		add(btSalvar);

		JLabel lbId = new JLabel("ID");
		lbId.setBounds(111, 44, 30, 14);
		add(lbId);

		JLabel lbCiudad = new JLabel("Ciudad");
		lbCiudad.setBounds(111, 94, 46, 14);
		add(lbCiudad);

		JLabel lbIdPais = new JLabel("ID Pais");
		lbIdPais.setBounds(111, 139, 46, 14);
		add(lbIdPais);

		JLabel lbInfo = new JLabel("Alta de la nueva ciudad");
		lbInfo.setBounds(10, 11, 147, 14);
		add(lbInfo);
	}

	public void initialize() {
		txtCiudad.requestFocus();
	}

	public void setTxtId(String txtId) {
		this.txtId.setText(txtId);
	}

	public void setTxtCiudad(String txtCiudad) {
		this.txtCiudad.setText(txtCiudad);
	}

	public void setTxtIdPais(String txtIdPais) {
		this.txtIdPais.setText(txtIdPais);
	}

	public String getTxtCiudad() {
		return txtCiudad.getText();
	}

	public String getTxtIdPais() {
		return txtIdPais.getText();
	}

}
