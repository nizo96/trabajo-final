package modelo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.Set;

public class Pedido {
	public Long numero;
	public Cliente cliente;
	public Set<Articulo> articulo;
	public LocalDate fecha;

	public Pedido() {
		
	}
	
	public BigDecimal getTotalPedido() {
		articulo = new HashSet<>();
		BigDecimal suma = new BigDecimal(0);
		for (Articulo articulos : articulo) {
			suma.add(articulos.getPrecioMasiva());
		}
		return suma;
	}
	
	public LocalDate getFechaEntregaEstimada() {
		LocalDate dias = LocalDate.now();
		fecha.plusDays(10);
		if (fecha.isBefore(dias)) {
			return dias.plusDays(1);
		} else {
			return fecha;
		}
	}
	
	public Set<Articulo> getMasBarato(){
		articulo = new HashSet<>();
		Set<BigDecimal> numeroMinimo = new HashSet<>();
		BigDecimal numero = new BigDecimal(0);
		
		for (Articulo articulos : articulo) {
			numeroMinimo.add(numero.min(articulos.getPrecioSinIva()));
			if (articulo.equals(numeroMinimo)) {
				return articulo;
			}
		}
		return articulo;
	}
	
	public Boolean isUrgente() {
		Period period = fecha.until(LocalDate.now());
		Integer mes = period.getMonths();
		if (mes >= 3) {
			return true;
		} else {
			return false;
		}
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Set<Articulo> getArticulo() {
		return articulo;
	}

	public void setArticulo(Set<Articulo> articulo) {
		this.articulo = articulo;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	

}
