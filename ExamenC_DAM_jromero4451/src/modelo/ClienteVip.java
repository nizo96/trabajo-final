package modelo;

import java.math.BigDecimal;

public class ClienteVip extends Cliente{

	public ClienteVip() {
		
	}
	
	public BigDecimal getPorcentajeDescuento() {
		return porcentajeDes.multiply(new BigDecimal(2));
	}

}
