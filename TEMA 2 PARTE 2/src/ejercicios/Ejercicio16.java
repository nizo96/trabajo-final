package ejercicios;

import java.util.Scanner;

public class Ejercicio16 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame un texto");
		String texto = scanner.nextLine();
		Integer x = 0;
		Integer y = 1;
		Integer tamaņoTotal = texto.length();
		while (y <= tamaņoTotal) {
			System.out.println(texto.substring(x, y));
			y++;
			x++;
		}
		System.out.println(texto.substring(x));
		scanner.close();
	}
}