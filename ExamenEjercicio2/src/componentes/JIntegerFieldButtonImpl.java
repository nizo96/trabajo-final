package componentes;

import javax.swing.JButton;
import javax.swing.JTextField;

public class JIntegerFieldButtonImpl extends JTextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4069748457221636147L;
	private JTextField txtNumero;
	private Integer numero;
	private Integer numeroMax;
	private Integer numeroMin;

	/**
	 * Launch the application.
	 */

	public JIntegerFieldButtonImpl(Integer initValue, Integer minValue, Integer maxValue) {

		numeroMax = maxValue;
		numeroMin = minValue;
		numero = initValue;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		setBounds(100, 100, 450, 300);
		setLayout(null);

		txtNumero = new JTextField();
		txtNumero.setBounds(0, 0, 86, 42);
		txtNumero.setEditable(false);
		add(txtNumero);
		txtNumero.setColumns(10);

		JButton BotonMas = new JButton("+");
		BotonMas.setBounds(85, -1, 52, 23);
		add(BotonMas);

		JButton BotonMenos = new JButton("-");
		BotonMenos.setBounds(85, 21, 52, 23);
		add(BotonMenos);
	}

	public Integer getValue() {
		if (numero != null) {
			return numero;
		} else {
			return null;
		}

	}

	public void setValue(Integer value) {
		if (!(value >= numeroMax || value <= numeroMin)) {
			numero = value;
		}

	}

	public void setMinValue(Integer minValue) {
		if (numero < minValue) {
			numero = minValue;
		}

	}

	public void setMaxValue(Integer maxValue) {
		if (numero > maxValue) {
			numero = maxValue;
		}

	}

}
