package ceu.dam.examenes.test;

import java.util.List;
import java.util.Scanner;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Equipo;
import ceu.dam.examenes.serviceEj2.EquipoImportService;
import ceu.dam.examenes.serviceEj2.EquipoImportServiceImpl;

public class TestEj2 {

	// EL FICHERO DE PRUEBA PARA EL EJ.2 TIENE QUE ESTAR AQUÍ:
	private static final String RUTA_XML_TEST = "c:/temporal/xml-ejemplo-importar.xml";

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("Revisa que el XML está en esta ruta: " + RUTA_XML_TEST + " // O cámbiala en el código para poner otra y reinicia.");
			System.out.println("Pulsa ENTER para continuar...");
			sc.nextLine();
			EquipoImportService service = new EquipoImportServiceImpl();
			List<Equipo> equipos = service.importar(RUTA_XML_TEST);
			if (equipos == null || equipos.isEmpty()) {
				System.out.println("Algo va mal. La lista que devuelves es nula o vacía");
				return;
			}
			for (Equipo equipo : equipos) {
				System.out.println(equipo);
			}
			System.out.println("\n>> Revisa que los datos que se imprimen corresponden con el XML y no falta nada");

		} catch (ErrorServiceException e) {
			e.printStackTrace();
			System.out.println("Algo no va bien.... Revisa tu código!!");
		}
		finally {
			sc.close();
		}
	}

}
