package ejercicios;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Boolean repetir = true;
		int numero;
		Scanner leer = new Scanner(System.in);

		System.out.println("Ponga un numero del 1 al 12: ");
		numero = leer.nextInt();

		while (repetir == true) {
			repetir = false;

			switch (numero) {

			case 1:
				System.out.println("Tu numero es el 1");
				break;
			case 2:
				System.out.println("Tu numero es el 2");
				break;
			case 3:
				System.out.println("Tu numero es el 3");
				break;
			case 4:
				System.out.println("Tu numero es el 4");
				break;
			case 5:
				System.out.println("Tu numero es el 5");
				break;
			case 6:
				System.out.println("Tu numero es el 6");
				break;
			case 7:
				System.out.println("Tu numero es el 7");
				break;
			case 8:
				System.out.println("Tu numero es el 8");
				break;
			case 9:
				System.out.println("Tu numero es el 9");
				break;
			case 10:
				System.out.println("Tu numero es el 10");
				break;
			case 11:
				System.out.println("Tu numero es el 11");
				break;
			case 12:
				System.out.println("Tu numero es el 12");
				break;
			default:
				repetir = true;
				System.out.println("Vuelva a introducir el n�mero: ");
				numero = leer.nextInt();
				break;
			}
		}
		leer.close();

	}

}
