package ceu.dam.examenes.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import ceu.dam.examenes.modelo.Entrenador;
import ceu.dam.examenes.modelo.Equipo;
import ceu.dam.examenes.modelo.Jugador;

public class HibernateUtil {
	private static SessionFactory sessionFactoy;

	public static SessionFactory getSessionFactoy() {
		if (sessionFactoy == null) {
			init();
		}
		return sessionFactoy;
	}

	private static void init() {
		try {
			ServiceRegistry registry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			Metadata metadata = new MetadataSources(registry)
					// Aqu� a�adimos las entidades que queremos mapear
					.addAnnotatedClass(Equipo.class)
					.addAnnotatedClass(Entrenador.class)
					.addAnnotatedClass(Jugador.class)
					.getMetadataBuilder().build();
			sessionFactoy = metadata.getSessionFactoryBuilder().build();
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}
}
