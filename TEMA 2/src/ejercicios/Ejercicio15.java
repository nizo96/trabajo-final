package ejercicios;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		int multiplo;
		int contador;
		int resultado;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("�Cual es el n�mero que quiere multiplicar?");
		multiplo = leer.nextInt();
		
		for (contador = 0; contador < 11; contador++) {
			resultado = contador * multiplo;
			System.out.println(multiplo + " x " + contador + " = " + resultado);
		}
		
		leer.close();

	}

}
