package ejercicios;

public class RestoDivision {

	public static void main(String[] args) {
		int dividendo = 139, divisor = 7;

		int operacion = dividendo / divisor;
		int resto = dividendo % divisor;

		System.out.println("La operacion es = " + operacion);
		System.out.println("El resto es = " + resto);
	}

}
