package javafx.gui.controller;

import ceu.dam.javafx.examen.model.Coche;
import ceu.dam.javafx.examen.services.ExamenService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class NuevoCocheController extends AppController{
	
	@FXML
	public TextField txMarca;
	
	@FXML
	public TextField txModelo;
	
	@FXML
	public TextField txMatricula;
	
	@FXML
	public Button btGuardar;
	
	private ExamenService service;
	
	public Coche coche;
	
	public Thread hilo;
	
	
	@FXML
	public void initialize() {
		txMarca.setDisable(true);
		//btGuardar.setDisable(true);
		//if (txModelo.getLength() >= 1 && txMatricula.getLength() >= 1) {
		//	btGuardar.setDisable(false);
		//}
	}
	
	@FXML
	public void cancelar(ActionEvent e) {
		cambiarVista(FxmlPaths.FXML_TABLA);
	}
	
	@FXML
	public void guardar(ActionEvent e) {
		try {
			coche.setModelo(txModelo.getText());
			coche.setMatricula(txMatricula.getText());
			service = new ExamenService();
			service.guardarNuevoCoche(coche);
			//hilo.wait(2000);
			//mostrarConfirmacion("Se guardo el coche");
			cambiarVista(FxmlPaths.FXML_TABLA);
			
		} catch (Exception e2) {
			mostrarError("No se pudo guardar el coche");
		}
	}
	
	public void guardarDatos(String marca) {
		coche = new Coche(null, null, null, null, null);
		coche.setMarca(marca);
		txMarca.setText(marca);
		
	}
	

}
