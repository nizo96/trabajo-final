package ejercicios;

import java.util.ArrayList;
import java.util.List;

public class PilaCadenas {
	private List<String> pila;
	
	public PilaCadenas( ) {
		pila = new ArrayList<>();
	}
	
	public void aņadirCadena(String cadena) {
		pila.add(0, cadena);
	}
	
	public String sacarCadena() {
		if(pila.isEmpty()) {
			return null;
		}
		String cadena = pila.get(0);
		pila.remove(0);
		return cadena;
	}
	public Integer getTamao() {
		return pila.size();
	}
	
	@Override
	public String toString() {
		return pila.toString();
	}
	
	

}
