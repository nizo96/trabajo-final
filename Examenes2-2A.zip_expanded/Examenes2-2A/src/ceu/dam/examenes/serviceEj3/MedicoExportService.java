package ceu.dam.examenes.serviceEj3;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Medico;

public interface MedicoExportService {

	/** El método recibe un Medico que se debe exportar en formato XML en un fichero en la ruta indicada.
	 * El formato del fichero XML tiene que ser como el ejemplo subido en Moodle en el EJ.3
	 * La exportación a XML Tendrá que realizarse utilizando JACKSON
	 * Si hay cualquier error, lanzará ErrorServiceException
	 */
	public void exportarMedico(Medico medico, String filePath) throws ErrorServiceException;
	
}
