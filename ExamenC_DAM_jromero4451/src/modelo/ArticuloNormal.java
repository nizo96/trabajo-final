package modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ArticuloNormal extends Articulo{

	public ArticuloNormal() {
		
	}
	
	public BigDecimal getIva() {
		return new BigDecimal(0.21);
		
	}
	
	public BigDecimal getPrecioMasiva() {
		BigDecimal suma = (precioSinIva.multiply(getIva()));
		return suma.add(precioSinIva).setScale(2, RoundingMode.HALF_UP);			
	}
	
	

}
