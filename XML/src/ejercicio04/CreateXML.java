package ejercicio04;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreateXML {
	public void crearXML(List<Libro> libro, String pathFile) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document xml = builder.newDocument();

			Element librosTag = xml.createElement("libros");

			xml.appendChild(librosTag);

			for (Libro libro2 : libro) {
				List<Edicion> ediciones = libro2.getEdiciones();
				List<String> autores = libro2.getAutores();

				Element libroTag = xml.createElement("libro");
				Element tituloTag = xml.createElement("titulo");
				Element edicionesTag = xml.createElement("ediciones");
				Element autoresTag = xml.createElement("autores");

				libroTag.appendChild(autoresTag);
				librosTag.appendChild(libroTag);
				libroTag.appendChild(tituloTag);
				libroTag.appendChild(edicionesTag);

				libroTag.setAttribute("isbn", libro2.getIsbn().toString());
				tituloTag.setTextContent(libro2.getTitulo());

				for (String autor : autores) {

					Element autorTag = xml.createElement("autor");
					autoresTag.appendChild(autorTag);
					autorTag.setTextContent(autor);
				}

				for (Edicion edicion : ediciones) {
					Element edicionTag = xml.createElement("edicion");
					Element a�oTag = xml.createElement("a�o");
					Element editorialTag = xml.createElement("editorial");

					edicionesTag.appendChild(edicionTag);
					edicionTag.appendChild(a�oTag);
					edicionTag.appendChild(editorialTag);

					a�oTag.setTextContent(edicion.getAo().toString());
					editorialTag.setTextContent(edicion.getEditorial());
				}

			}

			TransformerFactory factoryTransformer = TransformerFactory.newInstance();
			Transformer transformer = factoryTransformer.newTransformer();
			DOMSource source = new DOMSource(xml);
			File file = new File(pathFile);
			StreamResult stream = new StreamResult(file);
			transformer.transform(source, stream);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
