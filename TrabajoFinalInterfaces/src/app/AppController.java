package app;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import modelo.Usuario;
import service.MongoService;

public class AppController {
	public static Stage primaryStage;
	
	public static Usuario usuarioEstatico;
	
	public MongoService service;

//private static final Logger log = LogManager.getLogger(); // siempre importar con log4j.Logger (No el core)

	public AppController() {

	}

	public AppController(Stage stage) {
		primaryStage = stage;
	}

	public AppController cambiarVista(String fxml) {
		try {

			// log.log(Level.DEBUG, "Cambiando de vista"); // esta opcion
			// log.debug("Cambiando de vista"); // es lo mismo que arriba
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			Scene scene = new Scene(loader.load());
			primaryStage.setScene(scene);
			return loader.getController();
		} catch (IOException e) {
			// log.error("Error al cambiar de vista", e);
			throw new RuntimeException("No se ha podido cargar fxml con ruta" + fxml, e);

		}
	}

	public Parent cargarVista(String fxml) {
		try {
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			return loader.load();
		} catch (IOException e) {
			throw new RuntimeException("No se ha podido cargar fxml con ruta" + fxml, e);
		}

	}
	
	public void recogerUsuario(Integer id) {
		usuarioEstatico = new Usuario();
		service = new MongoService();
		usuarioEstatico = service.devuelveUsuario(id);
	}
	
	public void mostrarError(String mensaje) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setHeaderText(null);
		alert.setTitle("Error");
		alert.setContentText(mensaje);
		alert.showAndWait();
	}
	
	public void mostrarConfirmacion(String mensaje) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setHeaderText(null);
		alert.setTitle("GUARDADO");
		alert.setContentText(mensaje);
		alert.showAndWait();
	}
}
