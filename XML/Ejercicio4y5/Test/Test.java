package Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import modelo.Libro;
import services.Ejercicio04Service;

public class Test {

	public static void main(String[] args) {
		Ejercicio04Service service = new Ejercicio04Service();
		try {
			service.exportXML(Libro.createRandomList(10), "c:/temporal/librosTest.xml");
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

}

