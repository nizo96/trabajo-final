package ejercicios;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		Integer[] listacuadrado = new Integer[6];
		Integer numUsuario;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Introduzca un numero: ");
		numUsuario = leer.nextInt();
		
		listacuadrado[0] = numUsuario * numUsuario;
		
		for (int i = 1; i < listacuadrado.length; i++) {
			Integer sucesivo = ++numUsuario;
			Integer cuadrado = sucesivo * sucesivo;
			listacuadrado[i] = cuadrado;
		}
		
		for (int i = 0; i < listacuadrado.length; i++) {
			System.out.println(listacuadrado[i] + " ");
		}

	}

}
