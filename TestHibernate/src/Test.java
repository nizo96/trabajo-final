import modelo.Equipo;
import modelo.Estadio;
import service.EquipoService;

public class Test {
	
	public static void main(String[] args) {
		Equipo e = new Equipo();
		Estadio estadio = new Estadio();
		
		e.setNombre("Blas&decker F.C.");
		e.setColor("CaquiBlanco");
		e.setAņoFundacion(1992);
		estadio.setCodigo("001");
		estadio.setNombre("BlasHome Arena");
		estadio.setAforo(2990932);
		
		e.setEstadio(estadio);
		estadio.setEquipo(e);
		
		System.out.println("Insertando");
		EquipoService service = new EquipoService();
		service.insertarEquipo(e);
		
		System.out.println("Actualizado");
		e.setColor("Azul");
		service.actualizarEquipo(e);
		
		//System.out.println("Borrado");
		//service.borrarEquipo(e);
	}
	
	
		
	

}
