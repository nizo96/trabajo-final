package test;

import modelo.Articulo;
import modelo.Catalogo;
import servicio.CatalogoXMLException;
import servicio.Ejercicio12Service;

public class Test {

	public static void main(String[] args) {
		// cambia esta ruta para que coincida con una carpeta de tu disco
		String ruta = "c:/temporal/catalogo.xml";
		
		try {
			Catalogo catalogo = Catalogo.createRandomObject(10);
			Ejercicio12Service service = new Ejercicio12Service();
			service.exportXML(catalogo, ruta);
			
			// Comprueba que el XML generado es correcto e igual al que se solicita
			
			catalogo = service.importXML(ruta);
			System.out.println("Catlogo ledo con tamao: " + catalogo.getTamao());
			System.out.println("Cantidad de artculos (DEBE SER IGUAL AL TAMAO): " + catalogo.getArticulos().size());
			for (Articulo articulo : catalogo.getArticulos()) {
				System.out.println("\t" + articulo);
			}
			
			// Comprueba que lo impreso es igual al contenido del XML
			
			
		} catch (CatalogoXMLException e) {
			System.err.println("Error al intentar leer o escribir XML");
			e.printStackTrace();
		}
	}

}

