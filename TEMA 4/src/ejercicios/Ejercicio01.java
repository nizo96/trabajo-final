package ejercicios;

import java.util.Scanner;

public class Ejercicio01 {
	
public static String getMayusculas(String frase) {
	frase = frase.replaceAll(" ", "");
	frase = frase.toUpperCase();
	
	return frase;
}
	public static void main(String[] args) {
		String pruebafrase;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Ponga una frase");
		pruebafrase = leer.nextLine();
		
		System.out.println(getMayusculas(pruebafrase));
		
		leer.close();

	}

}
