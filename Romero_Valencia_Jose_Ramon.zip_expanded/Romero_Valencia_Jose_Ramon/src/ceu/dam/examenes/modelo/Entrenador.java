package ceu.dam.examenes.modelo;

import java.math.BigDecimal;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
@Entity
@Table(name = "entrenadores")
@JacksonXmlRootElement(localName = "entrenadores")  // TODO: ABEL: esto sobra
public class Entrenador {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 // TODO: ABEL: El id no es ninguna entidad, es un campo de la tabla. Esto sobra.
	@Column(name = "id_entrenador")
	// TODO: ABEL: te falta @Column para poner el nombre de este campo en la tabla "id_entrenador"
	private Long id; // TODO: ABEL: te falta indicar para jackson que esto va como atributo en el tag
	private String nombre;
	private BigDecimal sueldo;
	
	public Entrenador() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getSueldo() {
		return sueldo;
	}

	public void setSueldo(BigDecimal sueldo) {
		this.sueldo = sueldo;
	}

	@Override
	public String toString() {
		return "Entrenador [id=" + id + ", nombre=" + nombre + ", sueldo=" + sueldo + "]";
	}
	
	

}
