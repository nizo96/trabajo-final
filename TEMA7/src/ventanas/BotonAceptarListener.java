package ventanas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class BotonAceptarListener implements ActionListener {
	
	private JLabel label;

	public BotonAceptarListener(JLabel label) {
		this.label = label;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("boton 1")) {
			label.setText("Has pulsado el boton 1");
		}
		
		if (e.getActionCommand().equals("boton 2")) {
			label.setText("Has pulsado el boton 2");
		}
		
		if (e.getActionCommand().equals("boton 3")) {
			label.setText("Has pulsado el boton 3");
		}
		
	}

}
