package app;

import java.awt.EventQueue;

import javax.swing.JFrame;

import gui.InitialView;
import gui.Login01View;
import gui.Login02View;
import modelo.Usuario;

public class App {

	private InitialView pantallaInicio;
	private Login01View pantallaInter;
	private Login02View pantallaFinal;
	private JFrame frame;

	public App() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		pantallaInicio = new InitialView(this);
		pantallaInter = new Login01View(this);
		pantallaFinal = new Login02View(this);

		frame.setContentPane(pantallaInicio);

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void irAPantallaInicio(String usuario, String password) {
		frame.setContentPane(pantallaInicio);
		frame.revalidate();
		pantallaInicio.setTxtUsuario(usuario);
		pantallaInicio.setPasswordField(password);
	}

	public void irAPantallaInter(Usuario usuario) {
		frame.setContentPane(pantallaInter);
		frame.revalidate();
		pantallaInter.setTextField(usuario.getUsuario());
		pantallaInter.setTextField_1(usuario.getNombre());
		pantallaInter.setTextField_2(usuario.getApellidos());
	}

	public void irAPantallaFinal(Usuario usuario) {
		frame.setContentPane(pantallaFinal);
		frame.revalidate();
		pantallaFinal.setTextField(usuario.getUsuario());
		pantallaFinal.setPasswordField(usuario.getContrasena());
		pantallaFinal.setTextField_2(usuario.getNombre());
		pantallaFinal.setTextField_3(usuario.getApellidos());
	}

}
