package ejercicioSocket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		ServerSocket server = null;
		try {
			server = new ServerSocket(8888);
			System.out.println("Esperando peticiones...");
			Socket socket = server.accept();
			System.out.println("Procesando petici�n");
			InputStream is = socket.getInputStream();
			OutputStream os = socket.getOutputStream();
			Scanner sc = new Scanner(is).useDelimiter("#");
			String mensaje = sc.next();
			System.out.println("Mensaje recibido: " + mensaje);
			PrintWriter pw = new PrintWriter(os);
			pw.write(mensaje.toUpperCase() + "@");
			pw.flush();
			
			sc.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	


}
