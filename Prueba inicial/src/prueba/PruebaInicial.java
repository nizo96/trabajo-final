package prueba;

import java.util.Scanner;

public class PruebaInicial {

	public static void main(String[] args) {
		int hora;
		
		Scanner leer = new Scanner(System.in);
		
		System.out.println("�Que hora es? Debe estar comprendido entre 0 y 24");
		hora = leer.nextInt();
		
		if ((hora >= 0) && (hora < 12)) {
			System.out.println("�Buenos dias!");
		}
		else if ((hora >= 12) && (hora < 18)) {
			System.out.println("Buenas tardes");
		}
		else if ((hora >= 18) && (hora < 24)) {
			System.out.println("Buenas noches");
		}
		else if (hora > 24) {
			System.out.println("Hora fuera de rango");
		}
		
		leer.close();
	}

}
