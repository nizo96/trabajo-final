package ceu.dam.javafx.examen.model;

public class Coche {

	private String marca;
	private String modelo;
	private String ano;
	private String matricula;
	private Integer precio;
	
	public Coche(String marca, String modelo, String ano, String matricula, Integer precio) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.ano = ano;
		this.matricula = matricula;
		this.precio = precio;
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	
	

}
