package ejercicios;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Integer numero1;
		Integer numero2;
		Integer decision;
		Integer operacion;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Ponga el primer numero: ");
		numero1 = leer.nextInt();
		
		System.out.println("Ponga el segundo numero: ");
		numero2 = leer.nextInt();
		
		do {
			System.out.println("** Men� **");
			System.out.println("\t 1. Sumar");
			System.out.println("\t 2. Restar");
			System.out.println("\t 3. Multiplicar");
			System.out.println("\t 4. Dividir");
			System.out.println("\t 0. Salir");
			System.out.println(" ");
			System.out.println("Indique el numero de la opcion");
			decision = leer.nextInt();
			
			switch (decision) {
			case 0:
				
				break;
			case 1:
				operacion = numero1 + numero2;
				System.out.println(operacion);
				System.out.println(" ");
				break;
			case 2:
				operacion = numero1 - numero2;
				System.out.println(operacion);
				System.out.println(" ");
				break;
			case 3:
				operacion = numero1 * numero2;
				System.out.println(operacion);
				System.out.println(" ");
				break;
			case 4:
				if (numero2 == 0) {
					System.out.println("No se puede realizar la operacion");
					System.out.println(" ");
				} else {
				operacion = numero1 / numero2;
				System.out.println(operacion);
				System.out.println(" ");
				}
				break;
			}
			
		} while (decision != 0);
		
		
		
		leer.close();

	}

}
