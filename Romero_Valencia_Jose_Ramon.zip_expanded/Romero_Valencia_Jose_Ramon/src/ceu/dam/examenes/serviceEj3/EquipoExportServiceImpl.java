package ceu.dam.examenes.serviceEj3;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Entrenador;
import ceu.dam.examenes.modelo.Equipo;
import ceu.dam.examenes.modelo.Jugador;
import ceu.dam.examenes.serviceEj1.EquipoServiceImpl;

public class EquipoExportServiceImpl implements EquipoExportService{

	
	public void exportar(String filePath, String codigoEquipo) throws NotFoundException, ErrorServiceException{
		EquipoServiceImpl service = new EquipoServiceImpl();
		try {
//			Equipo equipo = new Equipo();
//			equipo = service.consultarEquipo(codigoEquipo);
			
			// Equipo de pruebas:
			Equipo equipo = new Equipo();
			equipo.setCodigo("RM");
			equipo.setNombre("Real Madrid");
			Entrenador entrenador = new Entrenador();
			entrenador.setId(99L);
			entrenador.setNombre("Friedrich Nietzsche");
			entrenador.setSueldo(new BigDecimal(45646.56));
			equipo.setEntrenador(entrenador);
			equipo.setJugadores(new ArrayList<>());
			for (int i = 0; i < 3; i++) {
				Jugador j = new Jugador();
				j.setId(Long.valueOf(i));
				j.setDorsal(i+10);
				j.setNombre("blas " + i + 1);
				equipo.getJugadores().add(j);
			}
			
			
			// TODO: ABEL: las anotaciones que has metido en el modelo no están bien. Este es el XML resultado:
		
			/*
<Equipo>
	<codigo>RM</codigo>
	<nombre>Real Madrid</nombre>
	<entrenador>
		<id>99</id>
		<nombre>Friedrich Nietzsche</nombre>
		<sueldo>45646.5599999999976716935634613037109375</sueldo>
	</entrenador>
	<libro id="0">
		<nombre>blas 01</nombre>
		<dorsal>10</dorsal>
	</libro>
	<libro id="1">
		<nombre>blas 11</nombre>
		<dorsal>11</dorsal>
	</libro>
	<libro id="2">
		<nombre>blas 21</nombre>
		<dorsal>12</dorsal>
	</libro>
</Equipo>
			*/
			
			File file = new File(filePath);
			XmlMapper mapper = new XmlMapper();
			mapper.writeValue(file, equipo);
		} catch (Exception e) { 
			e.printStackTrace();
			throw new NotFoundException("No se encuentra el equipo", e); // TODO:ABEL: no puedes lanzar un NotFound siempre. Esto es solo si no existe el equipo, y ya lo lanza el otro servicio de consulta. Aquí tendrías que lanzar ErrorService
		}
	}
	
}
