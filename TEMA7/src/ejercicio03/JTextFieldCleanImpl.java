package ejercicio03;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JTextFieldCleanImpl extends JPanel implements JTextFieldClean, ActionListener{

	public JButton botonLimpiar;
	public JTextField escribir;
	public int contador;

	public JTextFieldCleanImpl() {
		setLayout(null);

		escribir = new JTextField();
		escribir.setBounds(0, 0, 86, 20);
		add(escribir);
		escribir.setColumns(10);

		botonLimpiar = new JButton("Limpiar");
		botonLimpiar.setBounds(93, -1, 89, 23);
		add(botonLimpiar);
		escribir.setColumns(10);
		
		botonLimpiar.addActionListener(this);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5017859151067721789L;

	/**
	 * @wbp.nonvisual location=-38,109
	 */

	@Override
	public void clean() {
			escribir.setText("");
	}

	@Override
	public void enableButton(Boolean enable) {
		botonLimpiar.setEnabled(enable);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		clean();
		escribir.requestFocus();
		
	}

}
