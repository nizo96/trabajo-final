package javafx.gui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

public class PantallaMenuController extends AppController{
	
	@FXML 
	private BorderPane borderPanel;
	
	@FXML
	public void cargarPantallaDataPicker(ActionEvent event) {
		Parent vista = cargarVista(FxmlPaths.FXML_DATAPICKER);
		borderPanel.setCenter(vista);
	}
	@FXML
	public void cargarPantallaComboBox(ActionEvent event) {
		Parent vista = cargarVista(FxmlPaths.FXML_COMBO_BOX);
		borderPanel.setCenter(vista);
	}

}
