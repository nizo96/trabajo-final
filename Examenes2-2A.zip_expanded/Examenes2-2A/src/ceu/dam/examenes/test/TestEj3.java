package ceu.dam.examenes.test;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Hospital;
import ceu.dam.examenes.modelo.Medico;
import ceu.dam.examenes.serviceEj3.MedicoExportService;
import ceu.dam.examenes.serviceEj3.MedicoExportServiceImpl;

public class TestEj3 {

	private static final String RUTA_FICHERO_XML_SALIDA = "d:/prueba.xml";
	
	public static void main(String[] args) {
		System.out.println(">>>> Probando método exportarMedico()....");
		MedicoExportService service = new MedicoExportServiceImpl();
		Hospital hospital = DatosBuilder.getHospital();
		hospital.setId(69L);
		Medico medico = hospital.getCuadroMedico().get(1);
		try {
			service.exportarMedico(medico, RUTA_FICHERO_XML_SALIDA);
			System.out.println(">>>> Prueba OK!! Revisa si el XML generado tiene todos los datos OK");
		} catch (ErrorServiceException e) {
			System.out.println(">>>> Algo no funciona bien....");
			e.printStackTrace();
		}
	}

}
