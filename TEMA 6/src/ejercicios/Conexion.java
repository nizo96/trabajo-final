package ejercicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {

	public Conexion() {

	}

	public static Connection getNewConnection() throws SQLException {

		String urlConexion = "jdbc:oracle:thin:@//172.16.39.200:1521/XE";
		String claserDriver = "oracle.jdbc.driver.OracleDriver";
		String usuario = "ABEL";
		String password = "ABEL";
		
//		String urlConexion = " jdbc:mysql://172.16.39.1:3306/ABEL";
//		String claserDriver = " org.mariadb.jdbc.Driver";
//		String usuario = "ABEL";
// 		String password = "ABEL";
//------------------------------------------------------------------------------
		try {
			Class.forName(claserDriver);
		} catch (ClassNotFoundException e) {

			System.err.println("No se encuentra el driver JDBC, revisa la configuración");
			throw new RuntimeException(e);
		}
// -----------------------------------------------------------------------------
		Connection conn = DriverManager.getConnection(urlConexion, usuario, password);

		return conn;
	}

	public void consultarPersonasDao() throws SQLException {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = getNewConnection();

			stmt = conn.createStatement();

			rs = stmt.executeQuery("SELECT*FROM PERSONAS");

			while (rs.next()) {

				System.out
						.println(rs.getString("NOMBRE") + " " + rs.getString("APELLIDOS") + " " + rs.getString("DNI"));

			}
		} finally {
			if (stmt != null) {
				stmt.close();

			}

			if (conn != null) {
				conn.close();
			}
		}
	}

}