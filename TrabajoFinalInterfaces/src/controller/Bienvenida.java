package controller;

import app.AppController;
import app.FxmlPaths;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import modelo.Usuario;
import service.MongoService;

public class Bienvenida extends AppController{
	
	@FXML
	private AnchorPane panel;
	
	@FXML
	public Label txBienvenida;
	
	public Usuario usuario;
	
	public MongoService service;
	
	public Usuario usuarioCompleto;
	
	@FXML
	public void ubicaciones() {
		cambiarVista(FxmlPaths.FXML_TABLA);
	}
	
	@FXML
	public void cambiarModo() {
		panel.getStylesheets().add("/javafx/gui/fxml/dark.theme.css");
	}
	
	@FXML
	public void cerrarSesion() {
		cambiarVista(FxmlPaths.FXML_LOGIN);
	}
	
	@FXML
	public void initialize() {
		txBienvenida.setText("Bienvenido " + usuarioEstatico.getNombre());
	}
	@FXML
	public void salir() {
		System.exit(0);
	}
	

}
