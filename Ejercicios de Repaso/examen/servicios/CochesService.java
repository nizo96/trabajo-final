package servicios;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import modelo.CocheRenting;
import modelo.Coches;
import persistencia.CochesDAO;
import persistencia.ConexionNueva;

public class CochesService {

	public CochesService() {

	}

	public void extraerCoches(String marca, String filePath) throws IOException, SQLException {
		CochesDAO coche = new CochesDAO();
		ConexionNueva conProv = new ConexionNueva();
		Connection conn = conProv.getNewConnection();

		File file = new File(filePath);
		FileWriter writer = null;

		try {
			List<Coches> cocheMarca = coche.buscarCoche(conn, marca);

			if (cocheMarca.isEmpty()) {
				System.out.println("No hay coches con esa marca");
			} else {
				writer = new FileWriter(file);
				SimpleDateFormat fecFormat = new SimpleDateFormat("dd/MM/yyyy");
				DecimalFormat decFormat = new DecimalFormat("#, ###.00");

				for (Coches coches : cocheMarca) {
					writer.write(coches.getMatricula() + " # " + fecFormat.format(coches.getFechaFab()) + " # "
							+ coches.getMarca() + " # " + decFormat.format(coches.getPrecioCoste()) + " # "
							+ decFormat.format(coches.getTarifa()) + "\n");
				}
			}
		} catch (IOException e) {
			System.out.println("Error escrbiendo el fichero");
		} finally {
			if (writer != null) {
				writer.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

	}

	public Map<String, Coches> leerCochesRenting(String rutaFile) throws IOException, LeerCochesException {
		File file = new File(rutaFile);
		Scanner scanner = null;
		Map<String, Coches> coche = new HashMap<>();
		try {
			scanner = new Scanner(file);

			while (scanner.hasNext()) {
				String linea = (String) scanner.next();
				String[] dato = linea.split("-");
				CocheRenting cocheRenting = new CocheRenting();

				cocheRenting.setMatricula(dato[0]);
				cocheRenting.setMarca(dato[1]);
				DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");

				cocheRenting.setFechaFab(LocalDate.parse(dato[2], format));

				coche.put(dato[0], cocheRenting);

			}
		} catch (FileNotFoundException e) {
			throw new LeerCochesException("No se pudo leer el fichero");
		} finally {
			if (scanner != null) {
				scanner.close();
			}

		}
		return coche;
	}
}
