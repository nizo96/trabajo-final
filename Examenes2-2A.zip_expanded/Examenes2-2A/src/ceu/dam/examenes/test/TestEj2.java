package ceu.dam.examenes.test;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Hospital;
import ceu.dam.examenes.serviceEj2.HospitalExportService;
import ceu.dam.examenes.serviceEj2.HospitalExportServiceImpl;

public class TestEj2 {

	private static final String RUTA_FICHERO_XML_SALIDA = "d:/prueba.xml";
	
	public static void main(String[] args) {
		System.out.println(">>>> Probando método exportarHospital()....");
		HospitalExportService service = new HospitalExportServiceImpl();
		Hospital hospital = DatosBuilder.getHospital();
		hospital.setId(69L);
		try {
			service.exportarHospital(hospital, RUTA_FICHERO_XML_SALIDA);
			System.out.println(">>>> Prueba OK!! Revisa si el XML generado tiene todos los datos OK");
		} catch (ErrorServiceException e) {
			System.out.println(">>>> Algo no funciona bien....");
			e.printStackTrace();
		}
	}

}
