package ceu.dam.examenes.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Provincia {
	@Id
	@Column(name = "cod_provincia")
	private String codigo;
	@Column(name = "des_provincia")
	private String descripcion;
	
	public Provincia() {
	}

	
	
	public Provincia(String codigo, String descripcion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}



	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	@Override
	public String toString() {
		return "Provincia [codigo=" + codigo + ", descripcion=" + descripcion + "]";
	}
	
	

}
