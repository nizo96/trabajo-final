package ejercicio03;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Test extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7377431923104660976L;
	private JPanel contentPane;
	private JTextFieldCleanImpl textFieldClean;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(54, 65, 86, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(54, 110, 86, 14);
		contentPane.add(lblApellidos);
		
		textFieldClean = new JTextFieldCleanImpl();
		textFieldClean.setBounds(185, 65, 200, 20);
		contentPane.add(textFieldClean);
		
		textField = new JTextField();
		textField.setBounds(185, 110, 120, 20);
		contentPane.add(textField);
		textField.setColumns(8);
		
		JButton btnActivar = new JButton("Activar");
		btnActivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldClean.enableButton(true);
			}
		});
		btnActivar.setBounds(51, 188, 89, 23);
		contentPane.add(btnActivar);
		
		JButton btnDesactivar = new JButton("Desactivar");
		btnDesactivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldClean.enableButton(false);
			}
		});
		btnDesactivar.setBounds(159, 188, 100, 23);
		contentPane.add(btnDesactivar);
		
		JButton btnLimpiar = new JButton("Probar limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldClean.clean();
			}
		});
		btnLimpiar.setBounds(290, 188, 120, 23);
		contentPane.add(btnLimpiar);


	}
}
