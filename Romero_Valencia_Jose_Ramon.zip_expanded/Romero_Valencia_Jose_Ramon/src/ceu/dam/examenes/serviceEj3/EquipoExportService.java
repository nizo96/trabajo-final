package ceu.dam.examenes.serviceEj3;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;

public interface EquipoExportService {

	/** El método recibe la ruta de un fichero que aún no existe. Tendrá que ser creado en este método.
	 * También recibe el código de un equipo que debe estar registrado en la base de datos.
	 * Lo que hay que hacer es:
	 * 		1. Consultar en BBDD el equipo con dicho código (usa el servicio que has creado en el ej.1)
	 * 		2. Escribir en un XML los datos de ese equipo con el formato indicado en moodle para el ej.3
	 * 
	 * Para escribir el XML podrás utilizar el método que prefieras (DOM o Jackson)  
	 * Si no hay ningún equipo con el código indicado se lanzará NotFoundExcepción. 
	 * Si hay cualquier otro error, lanzará ErrorServiceException
	 */
	public void exportar(String filePath, String codigoEquipo) throws NotFoundException, ErrorServiceException;
	
}
