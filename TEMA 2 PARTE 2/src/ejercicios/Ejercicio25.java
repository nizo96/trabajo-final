package ejercicios;

import java.util.Scanner;
import java.lang.Math;

public class Ejercicio25 {

	private static final String PIEDRA = "piedra";
	private static final String PAPEL = "papel";
	private static final String TIJERAS = "tijeras";

	public static void main(String[] args) {
		String eleccion;
		String partida;
		int vicMaquina = 0;
		int vicPersona = 0;
		Scanner leer = new Scanner(System.in);
		
		do {
		System.out.println("Elija piedra, papel o tijeras: ");
		eleccion = leer.nextLine();
		
		
		int numeroAleatorio = (int) (Math.random()*3+1);
		
		switch (numeroAleatorio) {
		case 1:
			if (eleccion.equals(TIJERAS)) {
				System.out.println("Empate");
			}
			else if (eleccion.equals(PAPEL)) {
				System.out.println("Perdiste");
				++vicMaquina;
			}
			else if (eleccion.equals(PIEDRA)) {
				System.out.println("Ganaste");
				++vicPersona;
			}
			break;
		case 2:
			if (eleccion.equals(TIJERAS)) {
				System.out.println("Ganaste");
				++vicPersona;
			}
			else if (eleccion.equals(PAPEL)) {
				System.out.println("Empate");
			}
			else if (eleccion.equals(PIEDRA)) {
				System.out.println("Perdiste");
				++vicMaquina;
			}
			break;
		case 3: 
			if (eleccion.equals(TIJERAS)) {
				System.out.println("Perdiste");
				++vicMaquina;
			}
			else if (eleccion.equals(PAPEL)) {
				System.out.println("Ganaste");
				++vicPersona;
			}
			else if (eleccion.equals(PIEDRA)) {
				System.out.println("Empate");
			}
			break;
		}
			System.out.println("¿Quieres jugar otra partida?");
			partida = leer.nextLine();
			
		
		} 
		
		while (partida.equals("si"));
		
		System.out.println("Resultado total. Maquina: " + vicMaquina + " Usuario: " + vicPersona);
		
		
		
		
		
		

	}

}
