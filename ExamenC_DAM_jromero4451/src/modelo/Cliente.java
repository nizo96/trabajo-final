package modelo;

import java.math.BigDecimal;

public abstract class Cliente {
	public BigDecimal nombre;
	public BigDecimal porcentajeDes;

	public Cliente() {
		
	}

	public BigDecimal getNombre() {
		return nombre;
	}

	public void setNombre(BigDecimal nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getPorcentajeDes() {
		return porcentajeDes;
	}

	public void setPorcentajeDes(BigDecimal porcentajeDes) {
		this.porcentajeDes = porcentajeDes;
	}
	
	public BigDecimal getPorcentajeDescuento() {
		return porcentajeDes;
	}
	
	

}
