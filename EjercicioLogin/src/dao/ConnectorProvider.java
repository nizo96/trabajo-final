package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectorProvider {
	
	public Connection getNewConnection() throws SQLException {
		String usuario = "usuario";
		String password = "usuario";

		String url = "jdbc:mariadb://127.0.0.1:3306/usuarios";
		String driverClass = "org.mariadb.jdbc.Driver";
		
		try {
			Class.forName(driverClass);
		} catch (ClassNotFoundException e) {
			System.err.println("No se encuentra el driver JDBC. Revisa su configuracion");
			throw new RuntimeException(e);
		}

		Connection conn = DriverManager.getConnection(url, usuario, password);
		return conn;
	}
	



}
