package ejercicio01;

public class Buzon extends Thread{
	private Boolean estado;
	private Emisor emisor;
	private String mensajeGuardado;
	
	public Buzon() {
		emisor = new Emisor();
		estado = true;
	}
	
	public void escribir() throws InterruptedException {
		if (!(Thread.currentThread().getState().equals(Thread.State.WAITING)) && mensajeGuardado.equals("")) {
			estado = true;
			mensajeGuardado = emisor.mensaje();
			Thread.currentThread().wait();
			mensajeGuardado = "";
		} else {
			estado = false;
		}
		
	}
	
	public String leer() {
		if (estado) {
			if (Thread.currentThread().getState().equals(Thread.State.WAITING)) {
				notifyAll();
			}
			return mensajeGuardado;
		}
		return null;
	}

}
