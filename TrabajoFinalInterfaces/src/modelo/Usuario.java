package modelo;

public class Usuario {
	public String nombre;
	public String apellidos;
	public String contrasena;
	public Integer identificacion;
	public Integer permisos;
	
	public Usuario() {
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public Integer getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(Integer id) {
		this.identificacion = id;
	}

	public Integer getPermisos() {
		return permisos;
	}

	public void setPermisos(Integer permisos) {
		this.permisos = permisos;
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", apellidos=" + apellidos + ", contraseņa=" + contrasena + ", id=" + identificacion
				+ ", permisos=" + permisos + "]";
	}
	

}
