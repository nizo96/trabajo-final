package ceu.dam.examenes.serviceEj1;

import org.hibernate.Session;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Equipo;
import ceu.dam.examenes.util.HibernateUtil;
import jakarta.persistence.PersistenceException;

public class EquipoServiceImpl implements EquipoService{

	@Override
	public void crearEquipo(Equipo equipo) throws ErrorServiceException {
		// TODO: TRABAJO POR HACER DEL ALUMNO... 
		Session session = null;
		
		try {
			session = HibernateUtil.getSessionFactoy().openSession();
			session.getTransaction().begin();
			session.persist(equipo);
			session.getTransaction().commit();
			
		} catch (PersistenceException e) {
			// TODO: ABEL: deberías comprobar que la sesión no es null
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new ErrorServiceException("No se pudo crear el equipo", e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
	}

	@Override
	public Equipo consultarEquipo(String codigo) throws ErrorServiceException, NotFoundException {
		// TODO: TRABAJO POR HACER DEL ALUMNO...
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactoy().openSession();
			Equipo e = session.get(Equipo.class, codigo);
			if (e == null) {
				throw new NotFoundException("No existe ningun equipo con ese codigo");
			} else {
				return e;
			}
			
		}
		catch(PersistenceException e) {
			session.getTransaction().rollback(); // TODO: ABEL: sobra, no puedes hacer un rollback si no has iniciado transacción antes
			e.printStackTrace();
			throw new ErrorServiceException("No se pudo consultar el equipo", e);
		}
		finally {
			if (session!=null) {
				session.close();
			}
		}
	}
	


	

}
