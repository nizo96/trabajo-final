package service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PeliculasDao {

	
	public List<Pelicula> consultarPeliculas(Connection conn) throws SQLException{
		// 0. Crear una lista para meter los resultados
		List<Pelicula> resultados = new ArrayList<>();
		Statement stmt = null;
		try {
			// 1. Obtener stmt
			stmt = conn.createStatement();
			
			// 2. Lanzar query y obtener resultSet
			ResultSet rs = stmt.executeQuery("select * from film");
			
			// 3. Recorrer resultset e ir leyendo columnas para ir creando peliculas
			//		cada pel�cula nueva la a�adimos a la lista
			
			while(rs.next()) {
				Pelicula pelicula = new Pelicula();
				pelicula.setId(rs.getLong("film_id"));
				pelicula.setTitulo(rs.getString("title"));
				pelicula.setLongitud(rs.getInt("length"));
				resultados.add(pelicula);
			}
			// 4. Devolver la lista con los resultados
			return resultados;
		}
		finally {
			if (stmt!=null) {
				stmt.close();
			}
		}
			
		
	}
	
}
