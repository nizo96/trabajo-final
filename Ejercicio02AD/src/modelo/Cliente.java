package modelo;

public class Cliente {
	
	String firstName;
	String lastName;
	String email;
	Boolean activo;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String i) {
		this.firstName = i;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	
	

}
