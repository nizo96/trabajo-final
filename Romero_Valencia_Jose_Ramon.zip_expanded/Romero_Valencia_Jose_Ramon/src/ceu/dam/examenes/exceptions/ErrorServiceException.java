package ceu.dam.examenes.exceptions;

public class ErrorServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2144789114923366304L;

	public ErrorServiceException() {
	}

	public ErrorServiceException(String message) {
		super(message);
	}

	public ErrorServiceException(Throwable cause) {
		super(cause);
	}

	public ErrorServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ErrorServiceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
