import java.util.Random;

public class Alumno implements Runnable {

	private Cafeteria cafeteria;
	
	public Alumno(Cafeteria cafeteria) {
		this.cafeteria = cafeteria;
	}
	
	public void consumir() {
		
		while (true) {
			try {
				wait(esperar(0, 3001));
				if (cafeteria.haySitio()) {
					cafeteria.entraPersona();
					wait(esperar(0, 7001));
					cafeteria.salePersona();
				} else {
					System.out.println("no hay sitio");
				}
				
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println("Error al acceder a la cafeteria");
			}
		}
		
	}
	
	public Integer esperar(Integer inicio, Integer fin) {
		Random random = new Random();
		return random.nextInt(inicio, fin);
	}
	
	public void run() {
		synchronized (this) {
			consumir();
		}
	}
}
