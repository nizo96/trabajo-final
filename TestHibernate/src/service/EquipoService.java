package service;

import jakarta.persistence.PersistenceException;

import org.hibernate.Session;

import modelo.Equipo;
import util.HibernateUtil;

public class EquipoService {

	public void insertarEquipo(Equipo equipo) {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactoy().openSession();
			session.getTransaction().begin();
			session.persist(equipo);
			session.getTransaction().commit();

		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			e.printStackTrace();

		} finally {
			if (session != null) {
				session.close();
			}

		}
	}
	
	public void actualizarEquipo(Equipo equipo) {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactoy().openSession();
			session.getTransaction().begin();
			session.merge(equipo);
			session.getTransaction().commit();

		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			e.printStackTrace();

		} finally {
			if (session != null) {
				session.close();
			}

		}
	}
	
	public void borrarEquipo(Equipo equipo) {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactoy().openSession();
			session.getTransaction().begin();
			session.remove(equipo);
			session.getTransaction().commit();

		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			e.printStackTrace();

		} finally {
			if (session != null) {
				session.close();
			}

		}
	}
	
	

}
