package ceu.dam.examenes.serviceEj2;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Hospital;
import ceu.dam.examenes.modelo.Medico;

public class HospitalExportServiceImpl implements HospitalExportService {

	@Override
	public void exportarHospital(Hospital hospital, String filePath) throws ErrorServiceException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document xml = builder.newDocument();

			Element root = xml.createElement("hospital");
			root.setAttribute("id", hospital.getId().toString());
			xml.appendChild(root);

			Element descripcionTag = xml.createElement("descripcion");
			descripcionTag.setTextContent(hospital.getDescripcion());
			root.appendChild(descripcionTag);

			Element provinciaTag = xml.createElement("provincia");
			provinciaTag.setAttribute("codigo", hospital.getLocalizacion().getCodigo());
			provinciaTag.setTextContent(hospital.getLocalizacion().getDescripcion());
			root.appendChild(provinciaTag);

			Element equipoMedicoTag = xml.createElement("EquipoMedico");
			root.appendChild(equipoMedicoTag);

			List<Medico> medicos = hospital.getCuadroMedico();

			for (Medico medico : medicos) {
				Element profesionalTag = xml.createElement("profesional");
				equipoMedicoTag.appendChild(profesionalTag);

				Element colegiadoTag = xml.createElement("colegiado");
				colegiadoTag.setTextContent(medico.getNumColegiado().toString());
				profesionalTag.appendChild(colegiadoTag);

				Element nombreTag = xml.createElement("nombre");
				nombreTag.setTextContent(medico.getNombre());
				profesionalTag.appendChild(nombreTag);

				Element especialidadTag = xml.createElement("precio");
				especialidadTag.setTextContent(medico.getEspecialidad());
				profesionalTag.appendChild(especialidadTag);

				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(xml);
				StreamResult result = new StreamResult(new File(filePath));
				transformer.transform(source, result);
			}
		} catch (Exception e) {
			throw new ErrorServiceException("No se ha podido crear el XML", e);
		}

	}

}
