package modelo;

public class Lanzador {
	public static void main(String[] args) {
		Carrera carrera = new Carrera(1000);
		Coche cocheAlonso = new Coche("Alonso", 100, carrera);
		Coche cocheHamilton = new Coche("Hamilton", 101 , carrera);
		Coche cocheSainz = new Coche("Sainz", 99 , carrera);
		cocheAlonso.correrCarrera();
		cocheHamilton.correrCarrera();
		cocheSainz.correrCarrera();
		carrera.imprimirPodio();
	}
}

