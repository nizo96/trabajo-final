package javafx.gui.controller;

import java.util.ArrayList;
import java.util.List;

import ceu.dam.javafx.examen.model.Coche;
import ceu.dam.javafx.examen.services.ExamenService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class TablaController extends AppController {

	@FXML
	public ComboBox<String> comboBox;
	
	@FXML
	public TableView<Coche> tabla;
	
	@FXML
	public TableColumn<Coche, String> marca;
	
	@FXML
	public TableColumn<Coche, String> modelo;
	
	@FXML
	public TableColumn<Coche, String> matricula;
	
	@FXML
	public Button btNuevo;
	
	private ObservableList<Coche> datos;
	
	private ExamenService service;
	@FXML
	public void initialize() {
		datos = FXCollections.observableArrayList();
		tabla.setItems(datos);
		
		PropertyValueFactory<Coche, String> factMarca = new PropertyValueFactory<>("marca");
		PropertyValueFactory<Coche, String> factModelo = new PropertyValueFactory<>("modelo");
		PropertyValueFactory<Coche, String> factMatricula = new PropertyValueFactory<>("matricula");
		marca.setCellValueFactory(factMarca);
		modelo.setCellValueFactory(factModelo);
		matricula.setCellValueFactory(factMatricula);
		
		comboBox.getItems().add("Todas...");
		comboBox.getItems().add("Toyota");
		comboBox.getItems().add("Seat");
		comboBox.getItems().add("Renault");
		comboBox.getItems().add("BMW");
		
		comboBox.setValue("Seleccione una marca..");
		btNuevo.setDisable(true);
		
		
	}
	
	@FXML
	public void nuevo(ActionEvent e) {
		NuevoCocheController datos = (NuevoCocheController) cambiarVista(FxmlPaths.FXML_NUEVO);
		datos.guardarDatos(comboBox.getSelectionModel().getSelectedItem());
		}
	
	public void seleccionar(ActionEvent e) {
		service = new ExamenService();
		List<Coche> cocheList = new ArrayList<>();
		switch (comboBox.getSelectionModel().getSelectedItem()) {
		case "Todas...":
			btNuevo.setDisable(true);
			try {
				cocheList = service.consultarCoches();
				datos.setAll(cocheList);
			} catch (Exception e2) {
				mostrarError("No se pudo consultar todos los coches");
			}
			break;
		case "Toyota":
			btNuevo.setDisable(false);
			try {
				cocheList = service.consultarCoches("Toyota");
				datos.setAll(cocheList);
			} catch (Exception e2) {
				mostrarError("No se pudo consultar todos los coches");
			}
			break;
		case "Seat":
			btNuevo.setDisable(false);
			try {
				cocheList = service.consultarCoches("Seat");
				datos.setAll(cocheList);
			} catch (Exception e2) {
				mostrarError("No se pudo consultar todos los coches");
			}
			break;
		case "Renault":
			btNuevo.setDisable(false);
			try {
				cocheList = service.consultarCoches("Renault");
				datos.setAll(cocheList);
			} catch (Exception e2) {
				mostrarError("No se pudo consultar todos los coches");
			}
			break;
		case "BMW":
			btNuevo.setDisable(false);
			try {
				cocheList = service.consultarCoches("BMW");
				datos.setAll(cocheList);
			} catch (Exception e2) {
				mostrarError("No se pudo consultar todos los coches");
			}
			break;	
		default:
			btNuevo.setDisable(true);
			break;
		}
	}

}
