package modelo;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Pedido {
	String idPedido;
	LocalDate fechaPedido;
	LocalDate fechaEntrega;
	String cliente;
	List<LineaPedido> lista;
	
	
	
	public Pedido() {
		
	}
	public String getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(String idPedido) {
		this.idPedido = idPedido;
	}
	public LocalDate getFechaPedido() {
		return fechaPedido;
	}
	public void setFechaPedido(LocalDate fechaPedido) {
		this.fechaPedido = fechaPedido;
	}
	public LocalDate getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(LocalDate fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public List<LineaPedido> getLista() {
		return lista;
	}
	public void setLista(List<LineaPedido> lista) {
		this.lista = lista;
	}
	@Override
	public int hashCode() {
		return Objects.hash(cliente, fechaEntrega, fechaPedido, idPedido, lista);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		return Objects.equals(cliente, other.cliente) && Objects.equals(fechaEntrega, other.fechaEntrega)
				&& Objects.equals(fechaPedido, other.fechaPedido) && Objects.equals(idPedido, other.idPedido)
				&& Objects.equals(lista, other.lista);
	}
	
	

}
