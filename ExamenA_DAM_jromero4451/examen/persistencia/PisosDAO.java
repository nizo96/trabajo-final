package persistencia;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PisosDAO {

	public PisosDAO(ConexionNueva conexion) {
		conexion = new ConexionNueva();
	}

	public static void consultarPisos(Connection conexion1, String ciudad) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conexion1.prepareStatement("SELECT CIUDAD FROM PISOS WHERE UPPER(CIUDAD) = UPPER('?')");
			stmt.setString(1, ciudad);
			stmt.execute();

		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}

	}

	public Integer actualizarPrecioPisos(Connection conn, Integer identificador, BigDecimal precioNuevo)
			throws SQLException {
		PreparedStatement stmt = null;
		Integer isUpdate = 0;
		try {
			stmt = conn.prepareStatement("UPDATE PISOS SET PRECIOTASACION =? WHERE UPPER(ID) =?");
			stmt.setBigDecimal(1, precioNuevo);
			stmt.setInt(2, identificador);
			stmt.executeUpdate();

			if (stmt.executeUpdate() == 1) {
				++isUpdate;
			}

		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return isUpdate;
	}

}
