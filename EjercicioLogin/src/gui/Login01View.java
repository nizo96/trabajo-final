package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import app.App;
import dao.ConnectorProvider;
import dao.UsuarioDAO;
import modelo.Usuario;

public class Login01View extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6470515427566407213L;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private App controller;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private Usuario usu;
	private UsuarioDAO usuarioBase;
	private ConnectorProvider conn;

	/**
	 * Create the application.
	 */
	public Login01View(App controller) {
		this.controller = controller;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(100, 100, 450, 300);

		setLayout(null);

		textField = new JTextField();
		textField.setBounds(156, 60, 96, 20);
		add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(156, 111, 96, 20);
		add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(156, 152, 96, 20);
		add(textField_2);
		textField_2.setColumns(10);

		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					usu = usuarioBase.login(textField.getText(), textField_1.getText(), conn.getNewConnection());
					if (usu != null) {
						controller.irAPantallaInicio(textField.getText(), usu.getContrasena());
					}
				} catch (SQLException e2) {
					System.err.println();
				}
				
			}
		});
		btnNewButton.setBounds(80, 212, 89, 23);
		add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Modificar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					usu = usuarioBase.login(textField.getText(), textField_1.getText(), conn.getNewConnection());
					if (usu != null) {
						controller.irAPantallaFinal(usu);
					}
				} catch (SQLException e2) {
					System.err.println();
				}
			}
		});
		btnNewButton_1.setBounds(232, 212, 89, 23);
		add(btnNewButton_1);

		lblNewLabel = new JLabel("Username");
		lblNewLabel.setBounds(80, 63, 59, 14);
		add(lblNewLabel);

		lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(80, 114, 49, 14);
		add(lblNewLabel_1);

		lblNewLabel_2 = new JLabel("Apellidos");
		lblNewLabel_2.setBounds(80, 155, 49, 14);
		add(lblNewLabel_2);
	}

	public String getTextField() {
		return textField.getText();
	}

	public void setTextField(String texto) {
		this.textField.setText(texto);
	}

	public String getTextField_1() {
		return textField_1.getText();
	}

	public void setTextField_1(String texto) {
		this.textField_1.setText(texto);
	}

	public String getTextField_2() {
		return textField_2.getText();
	}

	public void setTextField_2(String texto) {
		this.textField_2.setText(texto);
	}

}
