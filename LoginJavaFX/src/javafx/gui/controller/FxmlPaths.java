package javafx.gui.controller;

public class FxmlPaths {
	
	private static final String PATH_BASE = "/javafx/gui/fxml/";
	public static final String FXML_LOGIN = PATH_BASE + "login.fxml";
	public static final String FXML_MENU = PATH_BASE + "menu.fxml";
	public static final String FXML_COMBO_BOX = PATH_BASE + "combobox.fxml";
	public static final String FXML_DATAPICKER = PATH_BASE + "datapicker.fxml";
	public static final String FXML_TABLA = PATH_BASE + "tabla.fxml";
	public static final String FXML_TIPO = PATH_BASE + "tipo.fxml";
	public static final String FXML_DATOS = PATH_BASE + "datos.fxml";

}
