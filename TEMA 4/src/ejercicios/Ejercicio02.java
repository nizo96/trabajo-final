package ejercicios;

import java.util.Scanner;

public class Ejercicio02 {
	
public static String getMinusculas(String frase) {
	frase = frase.replaceAll(" ", "");
	frase = frase.toLowerCase();
	
	return frase;
}

	public static void main(String[] args) {
		
		String pruebafrase;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Escriba una frase");
		pruebafrase = leer.nextLine();
		
		System.out.println(getMinusculas(pruebafrase));
		
		leer.close();
	}

}
