package ejercicios;

import java.util.Scanner;

public class Ej2jromero4451 {

	public static void main(String[] args) {
		String frase;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Ponga una palabra: ");
		frase = leer.nextLine();
		
		String [] fraseSeparada = frase.split("");
		
		for (int i = 0; i < fraseSeparada.length; i++) {
			fraseSeparada[i] += "-";
		}
		
		fraseSeparada[fraseSeparada.length - 1] = fraseSeparada[fraseSeparada.length - 1].replaceAll("-", "");
		for (int i = 0; i < fraseSeparada.length; i++) {
			System.out.print(fraseSeparada[i]);
		}
		leer.close();
	}

}
