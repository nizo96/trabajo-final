package services;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

import modelo.Libro;
import persistencia.ConexionNueva;
import persistencia.LibreriaDAO;

public class LibreriaService {
	
	private ConexionNueva conexion;
	private LibreriaDAO libreriaDao;
	Scanner scanner;
	
	public LibreriaService() {
		this.conexion = new ConexionNueva();
		libreriaDao = new LibreriaDAO(conexion);
	}
	
	public void importarLibroNuevo(String nombreFichero) {
		File file = new File(nombreFichero);
		Libro libro = new Libro();
		try {
			Connection conn = conexion.getNewConnection();
			scanner = new Scanner(file);
			while (scanner.hasNext()) {
				String linea = scanner.nextLine();
				String [] trozosLinea = linea.split(";");
				libro.setIsbn(trozosLinea[0]);
				libro.setTitulo(trozosLinea[1]);
				libro.setAutor(trozosLinea[2]);
				libro.setPrecio(new BigDecimal(trozosLinea[3].trim()));
				libreriaDao.insertarLibroNuevo(conn, libro);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	

}
