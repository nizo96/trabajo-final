package app;

import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application{
	
	@Override
	public void start(Stage primaryStage) {
			AppController controller = new AppController(primaryStage);
			controller.cambiarVista(FxmlPaths.FXML_LOGIN);
			primaryStage.show();
		}
	
	public static void main(String[] args) {
		launch();
	}
}


