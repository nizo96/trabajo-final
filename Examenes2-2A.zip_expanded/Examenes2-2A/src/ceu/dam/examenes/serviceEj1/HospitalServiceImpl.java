package ceu.dam.examenes.serviceEj1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Hospital;
import ceu.dam.examenes.modelo.Medico;
import ceu.dam.examenes.util.HibernateUtil;
import jakarta.persistence.PersistenceException;

public class HospitalServiceImpl implements HospitalService {

	@Override
	public Long crearHospital(Hospital hospital) throws ErrorServiceException {
		SessionFactory factory = null;
		Session session = null;

		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();

			session.getTransaction().begin();
			session.persist(hospital);
			session.getTransaction().commit();

			return hospital.getId();

		} catch (PersistenceException e) {
			if (session != null) {
				session.getTransaction().rollback();
			}
			throw new ErrorServiceException("Error registrando el nuevo hospital " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	@Override
	public Hospital consultarHospital(Long id) throws NotFoundException, ErrorServiceException {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			
			Hospital hospital = session.get(Hospital.class, id);
			if (hospital == null) {
				throw new NotFoundException("No existe el id: " + id);
			}
			return hospital;
		} catch (PersistenceException e) {
			throw new ErrorServiceException("Error consultando el hospital " + e.getMessage());
		}
	}

	@Override
	public Medico consultarMedico(Integer numColegiado) throws NotFoundException, ErrorServiceException {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			
			Medico medico = session.get(Medico.class, numColegiado);
			if (medico == null) {
				throw new NotFoundException("No existe el numero de colegiado: " + numColegiado);
			}
			return medico;
		} catch (PersistenceException e) {
			throw new ErrorServiceException("Error consultando el medico " + e.getMessage());
		}
	}

}
