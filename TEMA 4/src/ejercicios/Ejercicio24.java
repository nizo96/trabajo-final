package ejercicios;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {
		
		Profesor profesor = new Profesor();
		Alumno alumno = new Alumno("44652865H");
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Introduce el nombre del alumno");
		alumno.setNombre(leer.nextLine());
		
		System.out.println("Introduce el nombre del profesor");
		profesor.setNombre(leer.nextLine());
		
		System.out.println("Introduce la edad del alumno");
		alumno.setEdad(leer.nextInt());
		
		System.out.println("Introduce la edad del profesor");
		profesor.setEdad(leer.nextInt());
		
		
		
		
		System.out.println(profesor.toString());
		System.out.println(alumno.toString());
		
		leer.close();
		

	}

}
