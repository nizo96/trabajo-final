package ejercicios;

import java.math.BigDecimal;

public class Persona {
	private String genero;
	private BigDecimal altura;
	
	@Override
	public String toString() {
		return "Persona [genero=" + genero + ", altura=" + altura + "]";
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) throws ParametroIncorrectoException {
		if (genero.equals("H") || genero.equals("M")) {
			this.genero = genero;
		} else {
			throw new ParametroIncorrectoException("No se obtuvo H o M");
		}
	}

	public BigDecimal getAltura() {
		return altura;
	}

	public void setAltura(BigDecimal altura) throws ParametroIncorrectoException {
		
		if (altura.doubleValue() > 0 || altura.doubleValue() < 3) {
			this.altura = altura;
		} else {
			throw new ParametroIncorrectoException("El numero es mayor o menos al soportado");
		}
		
	}
	
	
	
	
	
	
	

}
