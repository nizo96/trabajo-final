package ejercicios;

import java.math.BigDecimal;

public class Hucha implements Gente, Gente2{
	private BigDecimal importe;
	
	public Hucha() {
		this.importe = new BigDecimal(0);
	}
	
	public BigDecimal meterDinero(BigDecimal importe1) {
		this.importe = importe.add(importe1);
		return importe;		
	}
	
	public BigDecimal sacarDinero(BigDecimal importe1) {
		this.importe = importe.subtract(importe1);
		return this.importe;
	}
	
	public BigDecimal contarDinero() {
		return this.importe;
	}
	
	public BigDecimal romperHucha() {
		BigDecimal sacado = new BigDecimal(0);
		sacado = this.importe;
		this.importe = this.importe.subtract(importe);
		return sacado;
	}

	@Override
	public String toString() {
		return "Hucha [importe=" + importe + "]";
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	
	

}
