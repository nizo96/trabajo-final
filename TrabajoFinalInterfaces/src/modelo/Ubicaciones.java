package modelo;

public class Ubicaciones {
	
	public Integer id;
	public Integer calle;
	public String hueco;
	public Integer columna;
	
	public Ubicaciones() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCalle() {
		return calle;
	}

	public void setCalle(Integer calle) {
		this.calle = calle;
	}

	public String getHueco() {
		return hueco;
	}

	public void setHueco(String hueco) {
		this.hueco = hueco;
	}

	public Integer getColumna() {
		return columna;
	}

	public void setColumna(Integer columna) {
		this.columna = columna;
	}

	@Override
	public String toString() {
		return "Ubicaciones [id=" + id + ", calle=" + calle + ", hueco=" + hueco + ", columna=" + columna + "]";
	}
	
	

}
