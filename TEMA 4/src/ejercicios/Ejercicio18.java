package ejercicios;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		Alumno alumno = new Alumno("123431453v");
		
		System.out.println("Introduzca el nombre del alumno");
		alumno.setNombre(leer.nextLine());
		
		System.out.println("Introduzca la edad del alumno");
		alumno.setEdad(leer.nextInt());
		
		leer.nextLine();
		
		System.out.println("Introduzca el DNI del alumno");
		alumno.setDni(leer.nextLine());
		
		System.out.println("Introduzca la nota del alumno");
		alumno.setNota(leer.nextInt());
		
		System.out.println("Estos datos son los introducidos");
		System.out.println("Nombre: " + alumno.getNombre());
		System.out.println("Edad: " + alumno.getEdad());
		System.out.println("DNI: " + alumno.getDni());
		System.out.println("Nota: " + alumno.aprobar());
		
		leer.close();
		
		
		

	}

}
