package ejercicio16;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class TableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1017671657674343772L;
	
	private List<Persona> datos;
	
	public TableModel() {
		datos = new ArrayList<>();
	}

	@Override
	public int getRowCount() {
		return datos.size();
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public String getColumnName(int column) {
		if (column == 0) {
			return "Nombre";
		}
		else {
			return "Apellidos";
		}
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Persona persona = datos.get(rowIndex);
		if (columnIndex == 0) {
			return persona.getNombre();
		}
		else {
			return persona.getApellido();
		}
		
	}
	
	public void addPersona(Persona persona) {
		datos.add(persona);
	}

	
	
	
	
}

