package ejercicio16;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import modelo.Pelicula;

public class TableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1017671657674343772L;
	
	private List<Persona> datos;
	
	public TableModel() {
		datos = new ArrayList<>();
	}

	@Override
	public int getRowCount() {
		return datos.size();
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public String getColumnName(int column) {
		if (column == 0) {
			return "Nombre";
		}
		else {
			return "Apellidos";
		}
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Pelicula pelicula = datos.get(rowIndex);
		if (columnIndex == 0) {
			return pelicula.getNombre();
		}
		else {
			return pelicula.getApellido();
		}
		
	}
	
	public void addPersona(Pelicula persona) {
		datos.add(persona);
	}

	
	
	
	
}
