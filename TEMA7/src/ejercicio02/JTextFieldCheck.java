package ejercicio02;

public interface JTextFieldCheck {


	/**
	 * Devuelve el texto contenido en el componente si el check est� marcado. En
	 * caso contrario, devuelve NULL
	 * @return - Texto o NULL
	 */
	public String getText();

	/**
	 * Cambia el texto del componente
	 * @param txt - Texto del componente
	 */
	public void setText(String txt);

	/** Devuelve el estado del componente. Si est� checkeado como activo, devuelve TRUE. En caso contrario, FALSE
	 * @return Boolean
	 */
	public Boolean isActivo();

	/** Cambia el estado del componente. Si se activa, el check se marcar� y el campo de texto se habilita. Si se desactiva, 
	 * el campo de texto se inhabilita y el check se desmarca.
	 * @param activo
	 */
	public void setActivo(Boolean activo);

}
