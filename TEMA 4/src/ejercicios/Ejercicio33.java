package ejercicios;

public class Ejercicio33 {

	public static void main(String[] args) {
		Semaforo semaforo1 = new Semaforo();

		semaforo1.setColor("AZUL");

		semaforo1.setColor("VERDE");

		semaforo1.setParpadeando(true);

		semaforo1.setColor("AMBAR");

		semaforo1.setParpadeando(true);

		for (int i = 0; i < 5; i++) {
			semaforo1.cambiarEstado();
		}
		System.out.println(semaforo1);

		Semaforo copia = new Semaforo();
		copia.setColor(semaforo1.getColor());
		copia.setParpadeando(semaforo1.getParpadeando());
		System.out.println(copia);
	}

}
