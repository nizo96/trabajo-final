package ejercicios;

import java.util.Scanner;

public class Ejercicio25 {

	public static void main(String[] args) {

		Scanner leer = new Scanner(System.in);
		Alumno[] alumnos = new Alumno[3];
		
		System.out.println("introduzca el tama�o");
		Integer tama�o = leer.nextInt();
		Curso curso = new Curso(tama�o);
		curso.setIdentificador("1");
		curso.setDescripcion("DAM-DAW");

		for (int i = 0; i < alumnos.length; i++) {
			Alumno alumno = new Alumno("0000000000");
			do {
				System.out.println("Introduce el DNI del alumno");
				alumno.setDni(leer.nextLine());
			} while (!alumno.validarDni2());

			System.out.println("Introduce el nombre");
			alumno.setNombre(leer.nextLine());
			System.out.println("Introduce la edad");
			alumno.setEdad(leer.nextInt());
			System.out.println("Introduce la nota");
			alumno.setNota(leer.nextInt());
			leer.nextLine();
			alumno.setCurso(curso);
			alumnos[i] = alumno;
			curso.addAlumno(alumno);
		}
		for (Alumno alumno : alumnos) {
			System.out.println(alumno);
		}
		if (alumnos[0].equals(alumnos[1]) || alumnos[0].equals(alumnos[2]) || alumnos[1].equals(alumnos[2])) {
			System.err.println("Error, hay al menos 2 alumnos repetidos");

		} else {
			System.out.println("Felicidades: todos los alumnos son distintos!!");

		}

		leer.close();
	}

}
