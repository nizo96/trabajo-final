package servicio;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import modelo.Articulo;
import modelo.Catalogo;
import modelo.Categoria;
import modelo.Modelo;
import modelo.Pvp;

public class Ejercicio12Service {

	/**
	 * Recibe un cat疝ogo y una ruta a un fichero XML donde debe exportarse. El
	 * formato del XML tiene que ser el indicado en el enunciado del ejercicio Si
	 * hay cualquier error, el servicio tendr� que lanzar una CatalogoXMLException
	 * 
	 * @param catalogo
	 * @param pathFile
	 * @throws CatalogoXMLException
	 */
	public void exportXML(Catalogo catalogo, String pathFile) throws CatalogoXMLException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document xml = builder.parse(new File(pathFile));

			Element root = xml.createElement("catalogo");
			root.setAttribute("tamaño", catalogo.getTamao().toString());
			xml.appendChild(root);

			Element articulosTag = xml.createElement("articulos");
			root.appendChild(articulosTag);

			List<Articulo> articulos = catalogo.getArticulos();

			for (Articulo articulo : articulos) {
				Element articuloTag = xml.createElement("articulo");
				articulosTag.appendChild(articuloTag);

				Element descripcionTag = xml.createElement("descripcion");
				descripcionTag.setTextContent(articulo.getDescripcion());
				articuloTag.appendChild(descripcionTag);

				Element identificadorTag = xml.createElement("identificador");
				identificadorTag.setTextContent(articulo.getSku());
				articuloTag.appendChild(identificadorTag);

				Element precioTag = xml.createElement("precio");
				precioTag.setTextContent(articulo.getPvp().getPrecio().toString());
				articuloTag.appendChild(precioTag);

				Element modelosTag = xml.createElement("modelosDisponibles");
				articuloTag.appendChild(modelosTag);

				for (Modelo modelo : articulo.getModelos()) {
					Element modeloTag = xml.createElement("modelo");
					modelosTag.appendChild(modeloTag);

					Element tallaTag = xml.createElement("talla");
					tallaTag.setTextContent(modelo.getTalla());
					modeloTag.appendChild(tallaTag);

					Element colorTag = xml.createElement("color");
					colorTag.setTextContent(modelo.getColor());
					modeloTag.appendChild(tallaTag);

					Element codigoBarraTag = xml.createElement("codigosBarra");
					modeloTag.appendChild(codigoBarraTag);

					for (String codigo : modelo.getCodigosBarra()) {
						Element codigoTag = xml.createElement("codigo");
						codigoTag.setTextContent(codigo);
						codigoBarraTag.appendChild(codigoTag);
					}
				}

				Element categoriasTag = xml.createElement("categorias");
				articuloTag.appendChild(categoriasTag);

				for (Categoria categoria : articulo.getCategorias()) {
					Element categoriaTag = xml.createElement("categoria");
					categoriaTag.setTextContent(categoria.getNombre());
					categoriasTag.appendChild(categoriaTag);
				}
				
				//Cuando termino xml lo quiero guardar en fichero
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(xml);
				StreamResult result = new StreamResult(new File(pathFile));
				transformer.transform(source, result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new CatalogoXMLException("No se ha podido crear el XML", e);
		}
		
		


	}

	/**
	 * Recibe una ruta donde hay un fichero XML que debe leerse para obtener un
	 * objeto Catalogo. El formato del XML ser� el indicado en el enunciado del
	 * ejercicio Si hay cualquier error, el servicio tendr� que lanzar una
	 * CatalogoXMLException
	 * 
	 * @param catalogo
	 * @param pathFile
	 * @throws CatalogoXMLException
	 */
	public Catalogo importXML(String pathFile) throws CatalogoXMLException {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			File file = new File(pathFile);
			Document xml = builder.parse(file);

			Catalogo catalogo = new Catalogo();

			Element catalogosTag = xml.getDocumentElement();
			Integer tamao = Integer.parseInt(catalogosTag.getAttribute("tamao"));
			catalogo.setTamao(tamao);
			catalogo.setArticulos(new ArrayList<>());

			NodeList articulosTagList = catalogosTag.getElementsByTagName("articulos");
			for (int i = 0; i < articulosTagList.getLength(); i++) {
				Element articulosTag = (Element) catalogosTag.getElementsByTagName("articulos").item(0);

				List<Articulo> articulosTagList1 = new ArrayList<>();
				NodeList articuloTagList = articulosTag.getElementsByTagName("articulo");
				for (int j = 0; j < articuloTagList.getLength(); j++) {
					Articulo articulo = new Articulo();
					Element articuloTag = (Element) articuloTagList.item(j);

					Element descripcionTag = (Element) articuloTag.getElementsByTagName("descripcion");
					articulo.setDescripcion(descripcionTag.getTextContent());

					Element identificadorTag = (Element) articuloTag.getElementsByTagName("identificador");
					articulo.setSku(identificadorTag.getTextContent());

					Element precioTag = (Element) articuloTag.getElementsByTagName("precio");
					articulo.setPvp(new Pvp());
					articulo.getPvp().setPrecio(new BigDecimal(precioTag.getTextContent()));

					Element modelosDisponiblesTag = (Element) articuloTag.getElementsByTagName("modelosDisponibles");

					List<Modelo> modeloList = new ArrayList<>();
					NodeList modeloTagList = modelosDisponiblesTag.getElementsByTagName("modelo");
					for (int k = 0; k < modeloTagList.getLength(); k++) {
						Modelo modelo = new Modelo();
						Element modeloTag = (Element) modeloTagList.item(k);
						Element tallaTag = (Element) articuloTag.getElementsByTagName("talla");
						
						modelo.setTalla(tallaTag.getTextContent());
						
						Element colorTag = (Element) articuloTag.getElementsByTagName("color");
						modelo.setColor(colorTag.getTextContent());
						
						NodeList codigoBarraTag = articuloTag.getElementsByTagName("codigosBarra");
						
						List<String> codigoList = modelo.getCodigosBarra();
						
						for (int l = 0; l < codigoBarraTag.getLength(); l++) {
							Element codigoTag = (Element) modeloTag.getElementsByTagName("codigo");
							codigoList.add(codigoTag.getTextContent());
						}
						modelo.setCodigosBarra(codigoList);
						modeloList.add(modelo);
						articulo.setModelos(modeloList);
					}
					List<Categoria> categoriaList = new ArrayList<>();
					Element categoriasTag = (Element) articuloTag.getElementsByTagName("categorias");
					NodeList categoriaListTag = categoriasTag.getElementsByTagName("categoria");
					for (int k = 0; k < categoriaListTag.getLength(); k++) {
						Categoria categoria = new Categoria();
						Element categoriaTag = (Element) categoriaListTag.item(k);
						categoria.setNombre(categoriaTag.getTextContent());
						categoriaList.add(categoria);
						articulo.setCategorias(categoriaList);
					}
					
					articulosTagList1.add(articulo);
					catalogo.setArticulos(articulosTagList1);
					return catalogo;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new CatalogoXMLException("No se ha podido crear el XML", e);
		}
		return null;
	}

}
