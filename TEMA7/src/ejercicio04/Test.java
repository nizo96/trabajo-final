package ejercicio04;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


public class Test extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7377431923104660976L;
	private JPanel contentPane;
	private JButtonComboBoxImpl buttonComboBox;
	private JTextField textField;
	private int contador = 0;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(54, 65, 86, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(54, 110, 86, 14);
		contentPane.add(lblApellidos);
		
		buttonComboBox = new JButtonComboBoxImpl();
		buttonComboBox.setBounds(185, 65, 200, 20);
		contentPane.add(buttonComboBox);
		
		textField = new JTextField();
		textField.setBounds(185, 110, 120, 20);
		contentPane.add(textField);
		textField.setColumns(8);
		
		JButton btnActivar = new JButton("getValue()");
		btnActivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(buttonComboBox.getValue());
			}
		});
		btnActivar.setBounds(51, 188, 100, 23);
		contentPane.add(btnActivar);
		
		JButton btnDesactivar = new JButton("clean()");
		btnDesactivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonComboBox.clean();
			}
		});
		btnDesactivar.setBounds(169, 188, 100, 23);
		contentPane.add(btnDesactivar);
		
		JButton btnLimpiar = new JButton("addItem(...)");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contador++;
				buttonComboBox.addValue("Blas " + contador);
			}
		});
		btnLimpiar.setBounds(290, 188, 120, 23);
		contentPane.add(btnLimpiar);

	}
}
