package service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import jakarta.persistence.PersistenceException;
import jpa.HibernateUtil;
import modelo.Articulo;
import modelo.Cliente;
import modelo.Pedido;
import modelo.PedidoLinea;

public class PedidosClientesServiceImpl implements PedidosClientesService {


	@Override
	public void crearCliente(Cliente cliente) throws PedidosClientesServiceException {
		SessionFactory factory = null;
		Session session = null;
		
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			
			session.getTransaction().begin();
			session.persist(cliente);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			System.err.println("Error registrando el nuevo cliente " + e.getMessage());
			e.printStackTrace();
			throw new PedidosClientesServiceException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}


	@Override
	public Pedido crearPedido(Pedido pedido) throws PedidosClientesServiceException {
		SessionFactory factory = null;
		Session session = null;
		List<PedidoLinea> listaPedido = new ArrayList<>();
		PedidoLinea pedidoLinea = new PedidoLinea();
		if (pedido != null) {
			listaPedido = pedido.getLineas();
			for (int i = 0; i < listaPedido.size(); i++) {
				pedidoLinea = listaPedido.get(i);
				pedidoLinea.setNumLinea(i);
				listaPedido.add(pedidoLinea);
			}
			pedido.setLineas(listaPedido);
		}
		
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			

			session.getTransaction().begin();
			session.persist(pedido);
			session.getTransaction().commit();
			
			return pedido;
		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			System.err.println("Error registrando el nuevo pedido " + e.getMessage());
			e.printStackTrace();
			throw new PedidosClientesServiceException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}


	@Override
	public Articulo crearArticulo(Articulo articulo) throws PedidosClientesServiceException {
		SessionFactory factory = null;
		Session session = null;
		
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			

			session.getTransaction().begin();
			session.persist(articulo);
			session.getTransaction().commit();
			return articulo;
		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			System.err.println("Error registrando el nuevo articulo " + e.getMessage());
			e.printStackTrace();
			throw new PedidosClientesServiceException(e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	@Override
	public void actualizarCliente(Cliente cliente) throws PedidosClientesServiceException {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			
			session.getTransaction().begin();
			session.merge(cliente);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			session.getTransaction().rollback();
			System.err.println("Error actualizando cliente: " + e.getMessage());
			e.printStackTrace();
			throw new PedidosClientesServiceException();
		}
	}


	@Override
	public Cliente consultarCliente(String dni) throws NotFoundException, PedidosClientesServiceException {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			
			Cliente cliente = session.get(Cliente.class, dni);
			if (cliente == null) {
				throw new NotFoundException("No existe el dni: " + dni);
			}
			return cliente;
		} catch (PersistenceException e) {
			System.err.println("Error consultando cliente: " + e.getMessage());
			e.printStackTrace();
			throw new PedidosClientesServiceException();
		}
	}


	@Override
	public Articulo consultarArticulo(Long idArticulo) throws NotFoundException, PedidosClientesServiceException {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			
			Articulo articulo = session.get(Articulo.class, idArticulo);
			if (articulo == null) {
				throw new NotFoundException("No existe el articulo: " + idArticulo);
			}
			return articulo;
		} catch (PersistenceException e) {
			System.err.println("Error consultando el articulo: " + e.getMessage());
			e.printStackTrace();
			throw new PedidosClientesServiceException();
		}
	}


	@Override
	public Pedido consultarPedido(String uuid) throws NotFoundException, PedidosClientesServiceException {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtil.getSessionFactoy();
			session = factory.openSession();
			
			Pedido pedido = session.get(Pedido.class, uuid);
			if (pedido == null) {
				throw new NotFoundException("No existe el uuid: " + uuid);
			}
			return pedido;
		} catch (PersistenceException e) {
			System.err.println("Error consultando pedido: " + e.getMessage());
			e.printStackTrace();
			throw new PedidosClientesServiceException();
		}
	}
	
	
}

