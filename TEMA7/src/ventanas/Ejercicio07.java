package ventanas;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Ejercicio07 extends JFrame implements FocusListener, ActionListener, ItemListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7971671603972634742L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	String color;
	JComboBox<String> comboBox;
	String seleccion;
	JRadioButton RadioGreen;
	JRadioButton RadioYellow;
	JRadioButton RadioBlack;
	JRadioButton RadioRed;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio07 frame = new Ejercicio07();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio07() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.addFocusListener(this);
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		textField.addKeyListener(new KeyAdapter() {

			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					textField_1.requestFocus();
				}

				if (e.getKeyCode() == KeyEvent.VK_UP) {
					textField_3.requestFocus();

				}

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					textField_1.requestFocus();

				}
			}
		});
		textField.setBounds(165, 28, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.addFocusListener(this);
		textField_1.addKeyListener(new KeyAdapter() {

			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					textField_2.requestFocus();
				}

				if (e.getKeyCode() == KeyEvent.VK_UP) {
					textField.requestFocus();

				}

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					textField_2.requestFocus();

				}
			}
		});

		textField_1.setBounds(165, 77, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.addFocusListener(this);
		textField_2.addKeyListener(new KeyAdapter() {

			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					textField_3.requestFocus();
				}

				if (e.getKeyCode() == KeyEvent.VK_UP) {
					textField_1.requestFocus();

				}

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					textField_3.requestFocus();

				}
			}
		});
		textField_2.setBounds(165, 145, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.addFocusListener(this);
		textField_3.addKeyListener(new KeyAdapter() {

			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					textField.requestFocus();
				}

				if (e.getKeyCode() == KeyEvent.VK_UP) {
					textField_2.requestFocus();

				}

				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					textField.requestFocus();

				}
			}
		});
		textField_3.setBounds(165, 204, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		comboBox = new JComboBox<>();
		comboBox.setBounds(290, 101, 113, 30);
		contentPane.add(comboBox);
		comboBox.addItem("yellow");
		comboBox.addItem("black");
		comboBox.addItem("red");
		comboBox.addItem("green");
		comboBox.addItemListener(this);

		RadioYellow = new JRadioButton("yellow");
		RadioYellow.setBounds(6, 27, 109, 23);
		contentPane.add(RadioYellow);
		RadioYellow.addActionListener(this);

		RadioBlack = new JRadioButton("black");
		RadioBlack.setBounds(6, 88, 109, 23);
		contentPane.add(RadioBlack);
		RadioBlack.addActionListener(this);

		RadioRed = new JRadioButton("red");
		RadioRed.setBounds(6, 142, 109, 23);
		contentPane.add(RadioRed);
		RadioRed.addActionListener(this);

		RadioGreen = new JRadioButton("green");
		RadioGreen.setBounds(6, 203, 109, 23);
		contentPane.add(RadioGreen);
		RadioGreen.addActionListener(this);
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(RadioBlack);
		bg.add(RadioGreen);
		bg.add(RadioRed);
		bg.add(RadioYellow);

	}

	@Override
	public void focusGained(FocusEvent e) {
		color = (String) comboBox.getSelectedItem();
		JTextField origen = (JTextField) e.getSource();
		switch (color) {
		case "green":
			origen.setBackground(Color.GREEN);
			break;
		case "yellow":
			origen.setBackground(Color.YELLOW);
			break;
		case "black":
			origen.setBackground(Color.BLACK);
			break;
		case "red":
			origen.setBackground(Color.RED);
			break;
		default:
			break;
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		JTextField origen = (JTextField) e.getSource();
		origen.setBackground(Color.WHITE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (RadioGreen.isSelected()) {
			comboBox.setSelectedItem("green");
		}
		if (RadioYellow.isSelected()) {
			comboBox.setSelectedItem("yellow");
		}

		if (RadioBlack.isSelected()) {
			comboBox.setSelectedItem("black");
		}
		if(RadioRed.isSelected()) {
			comboBox.setSelectedItem("red");
			
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (comboBox.getSelectedItem().equals("green")) {
			RadioGreen.setSelected(true);
		}
		if (comboBox.getSelectedItem().equals("yellow")) {
			RadioYellow.setSelected(true);
		}
		if (comboBox.getSelectedItem().equals("black")) {
			RadioBlack.setSelected(true);
		}
		if (comboBox.getSelectedItem().equals("red")) {
			RadioRed.setSelected(true);
		}
		
	}
}
