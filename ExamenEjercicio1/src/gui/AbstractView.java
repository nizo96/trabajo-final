package gui;

import javax.swing.JPanel;

public abstract class AbstractView extends JPanel{
	
	private static final long serialVersionUID = 2469231611045548992L;
	
	public AppController appController;
	
	public abstract void initialize();

}