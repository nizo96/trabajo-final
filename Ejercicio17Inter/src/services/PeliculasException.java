package services;

public class PeliculasException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9082816161889247448L;

	public PeliculasException() {
		super();
	}

	public PeliculasException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PeliculasException(String message, Throwable cause) {
		super(message, cause);
	}

	public PeliculasException(String message) {
		super(message);
	}

	public PeliculasException(Throwable cause) {
		super(cause);
	}

	

}
