package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProveedorConexiones {

	public Connection getNewConnection() throws SQLException {
		try {
			String claseDriver = "org.mariadb.jdbc.Driver";
			String urlConexion = "jdbc:mariadb://localhost:3306/sakila";
			String usuario = "sakila";
			String password = "sakila";

			Class.forName(claseDriver);
			Connection conn = DriverManager.getConnection(urlConexion, usuario, password);
			return conn;
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new SQLException(e);
		}
	}
}
