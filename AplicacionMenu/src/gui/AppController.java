package gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import modelo.City;
import servicio.CityServiceImpl;
import servicio.NotFoundException;

public class AppController {

	private JFrame frame;
	private InitialView initialView;
	private AltaView altaView;
	private BuscarView buscarView;
	private CityServiceImpl service;
	private City city;
	private static Dimension dimension;

	public static void main(String[] args) {
		// Intente hacerlo responsive, pero no lo he conseguido

		dimension = new Dimension();
		dimension = Toolkit.getDefaultToolkit().getScreenSize();
		EventQueue.invokeLater(new Runnable() {

			public void run() {
				try {
					AppController window = new AppController();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public AppController() {
		initialize();
	}

	private void initialize() {
		initialView = new InitialView(this);
		altaView = new AltaView(this);
		buscarView = new BuscarView(this);

		service = new CityServiceImpl();

		frame = new JFrame();

		setInitialSize(frame, 30, 50);
		centerWindow(frame);
		// frame.setBounds(100, 100, 450, 400);
		frame.pack();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu Menu1 = new JMenu("Ciudades");
		menuBar.add(Menu1);

		JMenuItem btInicio = new JMenuItem("Inicio");
		btInicio.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				changeView(initialView);
			}
		});
		Menu1.add(btInicio);

		JMenuItem btAlta = new JMenuItem("Alta");

		btAlta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				altaView.setTxtCiudad("");
				altaView.setTxtId("");
				altaView.setTxtIdPais("");
				changeView(altaView);
			}
		});
		Menu1.add(btAlta);

		JMenuItem btBuscar = new JMenuItem("Buscar");

		btBuscar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				changeView(buscarView);
			}
		});
		Menu1.add(btBuscar);

		JMenu Menu2 = new JMenu("Aplicacion");
		menuBar.add(Menu2);

		JMenuItem btAcerca = new JMenuItem("Acerca de");

		btAcerca.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				acercaDe();
			}
		});
		Menu2.add(btAcerca);

		JMenuItem btExit = new JMenuItem("Exit");
		btExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salir();
			}
		});
		btExit.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		Menu2.add(btExit);
		changeView(initialView);
	}

	private void changeView(AbstractView view) { // AbstractView es una clase propia
		frame.setContentPane(view);
		frame.revalidate();
		view.initialize();
	}

	public void mostrarError(String mensaje) {
		JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
	}

	private void mostrarConfirmacion(String mensaje) {
		JOptionPane.showMessageDialog(frame, mensaje, "OK", JOptionPane.INFORMATION_MESSAGE);
	}

	public void salir() {
		System.exit(0);
	}

	public void acercaDe() {
		JOptionPane.showMessageDialog(null, "Es una aplicacion de practica", "Acerca de..",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public City salvar() throws NotFoundException {
		try {
			city = new City();
			city.setDescripcion(altaView.getTxtCiudad());
			city.setCountryId(Long.parseLong(altaView.getTxtIdPais()));
			// altaView.setTxtId(city.getId().toString());
			mostrarConfirmacion("Se ha guardado correctamente");
			return service.createCity(city);
		} catch (Exception e) {
			mostrarError("No se pudieron actualizar los datos");
			throw new NotFoundException();
		}

	}

	private static void setInitialSize(JFrame frame, double widthPercent, double heightPercent) {
		Dimension newSize = new Dimension();

		newSize.setSize(((dimension.width * widthPercent) / 100), ((dimension.height * heightPercent) / 100));

		frame.setPreferredSize(newSize);
	}

	private static void centerWindow(JFrame frame) {
		Rectangle centerBounds = frame.getBounds();

		centerBounds.x = (dimension.width / 2) - (frame.getPreferredSize().width / 2);
		centerBounds.y = (dimension.height / 2) - (frame.getPreferredSize().height / 2);

		frame.setBounds(centerBounds);
	}

	public List<City> buscar(String cadena) throws NotFoundException {
		try {
			return service.getCities(cadena);
			
		} catch (Exception e) {
			throw new NotFoundException();
		}

	}

	// crear nuevos metodos

}
