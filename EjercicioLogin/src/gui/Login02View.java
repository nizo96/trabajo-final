package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import app.App;
import dao.ConnectorProvider;
import dao.UsuarioDAO;
import modelo.Usuario;

public class Login02View extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6470515427566407213L;
	private JTextField textField;
	private JTextField textField_2;
	private App controller;
	private JTextField textField_3;
	private Usuario usu;
	private UsuarioDAO usuarioBase;
	private ConnectorProvider conn;
	private JPasswordField passwordField;

	

	/**
	 * Create the application.
	 */
	public Login02View(App controller) {
		this.controller = controller;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(100, 100, 450, 300);
		
		setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(156, 45, 96, 20);
		add(textField);
		textField.setColumns(10);
		
		JPasswordField passwordField = new JPasswordField();
		passwordField.setBounds(143, 118, 96, 20);
		add(passwordField);
		
		textField_2 = new JTextField();
		textField_2.setBounds(156, 140, 96, 20);
		add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.irAPantallaInicio(textField.getText(),passwordField.getPassword().toString());
			}
		});
		btnNewButton.setBounds(102, 243, 89, 23);
		add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Guardar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuarioNuevo = new Usuario();
				usu = null;
				try {
					usuarioNuevo.setUsuario(textField.getText());
					usuarioNuevo.setContrasena(passwordField.getPassword().toString());
					usuarioNuevo.setNombre(textField_2.getText());
					usuarioNuevo.setApellidos(textField_3.getText());
					usu = usuarioBase.modificarUsuario(usuarioNuevo, conn.getNewConnection());
					
					if (usu != null) {
						controller.irAPantallaInter(usu);
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(238, 243, 89, 23);
		add(btnNewButton_1);
		
		textField_3 = new JTextField();
		textField_3.setBounds(156, 189, 96, 20);
		add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Username");
		lblNewLabel.setBounds(81, 48, 65, 14);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setBounds(81, 95, 49, 14);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Nombre");
		lblNewLabel_2.setBounds(81, 143, 49, 14);
		add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Apellidos");
		lblNewLabel_3.setBounds(81, 192, 49, 14);
		add(lblNewLabel_3);
	}

	public String getTextField() {
		return textField.getText();
	}

	public void setTextField(String texto) {
		this.textField.setText(texto);
	}

	public String getPasswordField() {
		return passwordField.getPassword().toString();
	}

	public void setPasswordField(String password) {
		this.passwordField.setText(password);
	}

	public String getTextField_2() {
		return textField_2.getText();
	}

	public void setTextField_2(String texto) {
		this.textField_2.setText(texto);
	}

	public String getTextField_3() {
		return textField_3.getText();
	}

	public void setTextField_3(String texto) {
		this.textField_3.setText(texto);
	}
	
}
