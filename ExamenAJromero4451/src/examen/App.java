package examen;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class App {

	public static void main(String[] args) {
		Granja granja1 = new Granja("Sevilla");
		System.out.println(granja1);

		Cerdo cerdo1 = new Cerdo(1);
		cerdo1.setRaza("Iberico bellota");
		Cerdo cerdo2 = new Cerdo(2);
		cerdo2.setRaza("Iberico cebo");

		Pollo pollo1 = new Pollo(3);
		Pollo pollo2 = new Pollo(4);
		Pollo pollo3 = new Pollo(5);

		granja1.addAnimal(cerdo1);
		granja1.addAnimal(cerdo2);
		granja1.addAnimal(pollo1);
		granja1.addAnimal(pollo2);
		granja1.addAnimal(pollo3);
		System.out.println(granja1);

		BigDecimal peso = new BigDecimal(120.4);
		cerdo1.setPeso(peso);
		cerdo1.setFechaNac(LocalDate.of(2021, 01, 15));
		BigDecimal peso1 = new BigDecimal(84);
		cerdo2.setPeso(peso1);

		pollo1.setFechaNac(cerdo1.fechaNac.plusMonths(3));
		pollo2.setFechaNac(cerdo1.fechaNac.plusMonths(3));
		pollo3.setFechaNac(cerdo1.fechaNac.plusMonths(3));

		BigDecimal peso2 = new BigDecimal(3);
		pollo1.setPeso(peso2);
		pollo2.setPeso(peso2);
		BigDecimal peso3 = new BigDecimal(13);
		pollo3.setPeso(peso3);

		System.out.println(pollo1.fechaNac.getMonthValue());
		LocalDate fecha = pollo1.fechaNac;
		DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		System.out.println(fecha.format(formato));

		System.out.println(granja1.calcularPrecioVenta() + "�");

		granja1.venderAnimales();

		System.out.println(granja1);

	}

}
