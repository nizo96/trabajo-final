package ejercicio01;

public class Receptor extends Thread implements Runnable{
	private Buzon buzon;
	private String mensajes;
	
	public Receptor() {
		buzon = new Buzon();
	}
	
	public void leerMensaje() {
		while(true) {
			try {
				mensajes = "";
				mensajes = buzon.leer();
				sleep(1000);
				if (buzon.leer() != null) {
					System.out.println("Recibido " + mensajes);
				} else {
					System.out.println("El buzon no tiene mensajes");
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		synchronized (this) {
			System.out.println("Receptor");
			leerMensaje();
		}
		
	}

}
