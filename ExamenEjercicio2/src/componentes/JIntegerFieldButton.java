package ceu.dam.interfaces.componentes;

public interface JIntegerFieldButton {

	

	/**
	 * Devuelve el entero contenido en el componente. En
	 * caso contrario, devuelve NULL
	 * @return - Texto o NULL
	 */
	public Integer getValue();

	
	
	
	/**
	 * Cambia el entero del componente si el indicado est� entre los valores configurados de minValue
	 * y maxValue (inclusives). En caso contrario, no cambiar� nada
	 */
	public void setValue(Integer value);

	
	
	
	/**
	 * Cambia el entero m�nimo que puede tomar el componente. Si el valor actual del componente es menor, se cambiar�
	 * por el minValue indicado
	 */
	public void setMinValue(Integer minValue);

	
	
	
	/**
	 * Cambia el entero m�ximo que puede tomar el componente. Si el valor actual del componente es mayor, se cambiar�
	 * por el maxValue indicado
	 */
	public void setMaxValue(Integer maxValue);

	
}
