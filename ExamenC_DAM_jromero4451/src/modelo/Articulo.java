package modelo;

import java.math.BigDecimal;

public abstract class Articulo {
	public String codigoBarra;
	public String descripcion;
	public BigDecimal precioSinIva;

	public Articulo() {

	}

	public BigDecimal getPrecioMasiva() {
		return getPrecioMasiva();
	}

	public BigDecimal getIva() {
		return getIva();
	}

	public String getCodigoBarra() {
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getPrecioSinIva() {
		return precioSinIva;
	}

	public void setPrecioSinIva(BigDecimal precioSinIva) {
		this.precioSinIva = precioSinIva;
	}

}
