package ceu.dam.ad.examenes.examen11b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootConfiguration 
@EnableAutoConfiguration 
public class AppRestService {
	//Me sigue sin funcionar y no puedo probar el REST

	public static void main(String[] args) {
		SpringApplication.run(AppRestService.class, args); 
	
	}

}
