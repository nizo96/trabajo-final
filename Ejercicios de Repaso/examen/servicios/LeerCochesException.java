package servicios;

public class LeerCochesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3565989794015985670L;

	public LeerCochesException() {
		// TODO Auto-generated constructor stub
	}

	public LeerCochesException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public LeerCochesException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public LeerCochesException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LeerCochesException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
