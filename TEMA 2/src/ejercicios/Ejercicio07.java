package ejercicios;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numero1, numero2;
		
		do {
			System.out.println("Necesito que me des dos n�meros iguales");
			System.out.println("Dame un primer n�mero");
			numero1 = scanner.nextInt();
			System.out.println("Dame un segundo n�mero");
			numero2 = scanner.nextInt();
		}
		while(numero1 != numero2);
		
		System.out.println("Los n�mero ahora s� son iguales");
		
		scanner.close();
	}

}
