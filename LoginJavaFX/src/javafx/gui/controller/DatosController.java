package javafx.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import modelo.Animal;
import service.AnimalesServices;

public class DatosController extends AppController{
	
	@FXML
	public TextField nombre;
	@FXML
	public TextField edad;
	
	public Animal animal;
	
	@FXML
	public void initialize() {
		
	}
	@FXML
	public void siguiente() {
		try {
			animal.setEdad(Integer.parseInt(edad.getText()));
			animal.setNombre(nombre.getText());
			AnimalesServices service = new AnimalesServices();
			service.addAnimal(animal);
			cambiarVista(FxmlPaths.FXML_TABLA);
		} catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText(null);
			alert.setTitle("Error");
			alert.setContentText("No puede ser vacio");
			alert.showAndWait();
		}
		
	}
	
	public void guardarDatos(String tipo) {
		animal = new Animal();
		animal.setTipo(tipo);
	}
	

}
