package modelo;

import java.math.BigDecimal;

public class CocheVenta extends Coches {

	public CocheVenta() {
		super();
	}

	public BigDecimal getTarifa() {
		if (isAntiguo()) {
			return precioCoste;
		} else {
			return precioCoste.multiply(new BigDecimal(0.30));
		}

	}

}
