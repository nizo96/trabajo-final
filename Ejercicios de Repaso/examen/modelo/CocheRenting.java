package modelo;

import java.math.BigDecimal;

public class CocheRenting extends Coches {

	public CocheRenting() {
		super();
	}

	public BigDecimal getTarifa() {
		if (isGamaAlta()) {
			return new BigDecimal(600);
		} else {
			return new BigDecimal(300);
		}

	}

}
