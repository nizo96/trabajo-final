package servicios;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import modelo.ArticuloSanitario;
import modelo.Cliente;
import persistencia.ArticulosDao;
import persistencia.ConexionNueva;

public class PedidosService {

	public PedidosService() {
		
	}
	
	public void guardarClienteEnFichero(Map<String ,Cliente> cliente, String rutaFichero) throws IOException {
		Cliente clientes;
		File file = new File(rutaFichero);
		FileWriter writer = null;
		try {
			Map<String, Cliente> cliente1 = new HashMap<>();
			writer = new FileWriter(file);
			DecimalFormat decFormat = new DecimalFormat("#, ##.## %");
			Collection<Cliente> values = cliente1.values();
			for (Cliente cliente2 : values) {
				writer.write(values + "--->" + cliente2.getNombre() + " / " + cliente2.getPorcentajeDescuento());
			}
		} catch (IOException e) {
			System.out.println("Error escribiendo el fichero");
			
		} finally {
			if (writer != null) {
				writer.close();
			}
			
		}
	}
	
	public Integer importarArticulos(String rutaFichero) throws SQLException, FileNotFoundException{
		File file = new File(rutaFichero);
		FileWriter writer = null;
		Scanner leer = new Scanner(file);
		ConexionNueva conProv = new ConexionNueva();
		Connection conn = conProv.getNewConnection();
		Integer contador = 0;
		try {
			if (file.canRead()) {
				while (leer.hasNext()) {
					String linea = (String) leer.next();
					String[] dato = linea.split("_");
					if (dato[0].compareTo(LocalDate.now().toString()) <= 0) {
						contador ++;
						ArticuloSanitario articulo = new ArticuloSanitario();
						ArticulosDao dao = new ArticulosDao();
						dao.insertarArticulo(conn, articulo);
					}
				}
			} 
		} catch (SQLException e) {
			
		}
		
		return contador;
		
	}

}

