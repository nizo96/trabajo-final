package ejercicios;

import java.util.Scanner;

public class Ejercicio10Abel {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String nombre1, nombre2;
		do {
			System.out.println("Dame un nombre");
			nombre1 = scanner.nextLine();
			System.out.println("Dame otro nombre");
			nombre2 = scanner.nextLine();
		}
		while (nombre1.isBlank() || nombre2.isBlank());
		
		int comparacion = nombre1.compareTo(nombre2);
		if (comparacion <= 0) {
			System.out.println("Orden correcto: " + nombre1 + " - " + nombre2);
		}
		else {
			System.out.println("Orden correcto: " + nombre2 + " - " + nombre1);
		}
		
		scanner.close();
	}
	
}

