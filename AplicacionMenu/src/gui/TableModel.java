package gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import modelo.City;

public class TableModel extends AbstractTableModel{
	private static final String COLUMNA_ID = "Id";
	private static final String COLUMNA_CIUDAD = "Ciudad";
	private static final String COLUMNA_IDPAIS = "Id Pais";

	/**
	 * 
	 */
	private static final long serialVersionUID = -2412407473703651652L;
	private List<City> ciudad;
	private List<String> columnas;
	
	public TableModel() {
		ciudad = new ArrayList<>();
		columnas = new ArrayList<>();
		columnas.add(COLUMNA_ID);
		columnas.add(COLUMNA_CIUDAD);
		columnas.add(COLUMNA_IDPAIS);
	}
	

	@Override
	public int getRowCount() {
		return ciudad.size();
	}

	@Override
	public int getColumnCount() {
		return columnas.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		City city = ciudad.get(rowIndex);
		String columna = columnas.get(columnIndex);
		if (columna.equals(COLUMNA_ID)) {
			return city.getId();
		}
		if (columna.equals(COLUMNA_CIUDAD)) {
			return city.getDescripcion();
		}
		if (columna.equals(COLUMNA_IDPAIS)) {
			return city.getCountryId();
		}
		return "Error";
	}
	@Override
	public String getColumnName(int columnIndex) {
		if (columnIndex == 0) {
			return "Id";
		}
		if (columnIndex == 1) {
			return "Ciudad";
		}
		if (columnIndex == 2) {
			return "Id Pais";
		}
		return "Precio";
	}
	
	public void setDatos(List<City> ciudad) {
		this.ciudad = ciudad;
	}
	
	public void actualizarColumnas (Boolean columnId, Boolean columnCiudad, Boolean columnIdPais) {
		columnas.clear();
		if (columnId) {
			columnas.add(COLUMNA_ID);
		}
		if (columnCiudad) {
			columnas.add(COLUMNA_CIUDAD);
		}
		if (columnIdPais) {
			columnas.add(COLUMNA_IDPAIS);
		}
	}

}
