package javafx.gui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;

public class TipoController extends AppController{
	
	@FXML
	public TextField txField;

	@FXML
	public void siguiente(ActionEvent event) {
		if (txField.getText().isBlank()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText(null);
			alert.setTitle("Error");
			alert.setContentText("No puede ser vacio");
			alert.showAndWait();
			
		} else {
		DatosController datos = (DatosController) cambiarVista(FxmlPaths.FXML_DATOS);
		datos.guardarDatos(txField.getText());
		}
	}

}
