package ceu.dam.javafx.examen.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ceu.dam.javafx.examen.model.Coche;

public class ExamenService{


	/** Devuelve la lista de coches de la marca indicada que estén registrados en la "base de datos" simulada.
	 * Si no hay coches para la marca indicada, devolverá una lista vacía 
	 * @param marca
	 * @return
	 */
	public List<Coche> consultarCoches(String marca){
		if (marca == null) {
			return new ArrayList<Coche>();
		}
		marca = marca.toUpperCase().trim();
		if (!bbdd.containsKey(marca)) {
			return new ArrayList<Coche>();
		}
		return bbdd.get(marca);
	}
	
	/** Devuelve la lista de todos los coches que estén registrados en la "base de datos" simulada.
	 * Si no hay coches, devolverá una lista vacía 
	 * @return
	 */
	public List<Coche> consultarCoches(){
		List<Coche> bbddTodos = new ArrayList<Coche>();
		bbddTodos.addAll(renault);
		bbddTodos.addAll(toyota);
		bbddTodos.addAll(seat);
		bbddTodos.addAll(bmw);
		return bbddTodos;
	}
	
	
	/** Guarda un nuevo coche en la "base de datos" simulada.
	 * Si el coche indicado tiene como MATRÍCULA "X", lanzará una excepción.
	 * Si el coche es null, o la marca del coche es null, o la marca del coche no esté registrada, lanzará una excepción
	 * @param coche
	 * @throws ExamenException 
	 */
	public void guardarNuevoCoche(Coche coche) throws ExamenException {
		if (coche==null) {
			System.err.println("EL COCHE NO PUEDE SER NULL");
			throw new ExamenException("EL COCHE NO PUEDE SER NULL");
		}
		if (coche.getMarca()==null) {
			System.err.println("LA MARCA DEL COCHE NO PUEDE SER NULL");
			throw new ExamenException("LA MARCA DEL COCHE NO PUEDE SER NULL");
		}
		if (!bbdd.containsKey(coche.getMarca().toUpperCase())) {
			System.err.println("LA MARCA DEL COCHE INDICADA (" + coche.getMarca().toUpperCase() + ") NO EST� REGISTRADA EN BBDD. ");
			throw new ExamenException("LA MARCA DEL COCHE INDICADA (" + coche.getMarca().toUpperCase() + ") NO ESTÁ REGISTRADA EN BBDD. ");
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (coche.getMatricula().equalsIgnoreCase("X")) {
			System.err.println("MATRÍCULA NO PERMITIDA");
			throw new ExamenException("MATRÍCULA NO PERMITIDA");
		}
		bbdd.get(coche.getMarca().toUpperCase()).add(coche);
	}
	
	
	
	
	
	
	
	
	
	// Ignorar este código...
	private static Map<String, List<Coche>> bbdd = new HashMap<String, List<Coche>>();
	private static List<Coche> seat = new ArrayList<Coche>();
	private static List<Coche> renault = new ArrayList<Coche>();
	private static List<Coche> bmw = new ArrayList<Coche>();
	private static List<Coche> toyota = new ArrayList<Coche>();
	static {
		toyota.add(new Coche("Toyota", "Auris", "2010", "1521FFD", 8532));
		toyota.add(new Coche("Toyota", "Auris", "2013", "4862DDD", 7896));
		toyota.add(new Coche("Toyota", "Rav4", "2016", "4862BRT", 15000));
		toyota.add(new Coche("Toyota", "Corolla", "2020", "8761DQT", 16100));
		toyota.add(new Coche("Toyota", "Yaris", "2012", "9661JKK", 4350));
		toyota.add(new Coche("Toyota", "Rav4", "2019", "4134NBV", 17211));
		seat.add(new Coche("Seat", "Córdoba", "2005", "2521RFD", 8000));
		seat.add(new Coche("Seat", "Ibiza", "2010", "4522RTD", 6000));
		seat.add(new Coche("Seat", "León", "2011", "0114BRT", 15500));
		seat.add(new Coche("Seat", "Córdoba", "2019", "8761DQT", 17100));
		seat.add(new Coche("Seat", "León", "2020", "9661JKK", 4200));
		seat.add(new Coche("Seat", "Ibiza", "2019", "4134BBV", 17211));
		seat.add(new Coche("Seat", "Córdoba", "2011", "1521FFD", 8532));
		seat.add(new Coche("Seat", "Ibiza", "2013", "4862DDD", 7896));
		seat.add(new Coche("Seat", "León", "2016", "4862BRT", 15000));
		seat.add(new Coche("Seat", "Toledo", "2005", "2521RFD", 8000));
		seat.add(new Coche("Seat", "Ibiza", "2010", "4522RTD", 6000));
		seat.add(new Coche("Seat", "Toledo", "2011", "0114BRT", 15500));
		seat.add(new Coche("Seat", "Córdoba", "2019", "8761DQT", 17100));
		seat.add(new Coche("Seat", "Toledo", "2020", "9661JKK", 4200));
		seat.add(new Coche("Seat", "Ibiza", "2019", "4134BBV", 17211));
		seat.add(new Coche("Seat", "Córdoba", "2011", "1521FFD", 8532));
		seat.add(new Coche("Seat", "León", "2016", "4862BRT", 15000));
		seat.add(new Coche("Seat", "Ibiza", "2013", "4862DDD", 7896));
		renault.add(new Coche("Renault", "Laguna", "2017", "5562CRT", 3245));
		renault.add(new Coche("Renault", "Clío", "2020", "8551CQC", 11500));
		renault.add(new Coche("Renault", "Megane", "2008", "1555FPD", 12000));
		renault.add(new Coche("Renault", "Megane", "2003", "4662RWD", 10012));
		renault.add(new Coche("Renault", "Megane", "2010", "9001VVF", 6588));
		renault.add(new Coche("Renault", "Clío", "2009", "0034BWW", 1500));
		bbdd.put("TOYOTA", toyota);
		bbdd.put("SEAT", seat);
		bbdd.put("RENAULT", renault);
		bbdd.put("BMW", bmw);
		
	}
	
	

	
}
