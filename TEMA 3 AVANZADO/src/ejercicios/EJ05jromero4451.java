package ejercicios;

import java.util.Scanner;

public class EJ05jromero4451 {

	public static void main(String[] args) {
		String palabra;
		Scanner leer = new Scanner(System.in);

		System.out.println("Escriba una palabra con m�s de 5 caracteres o empiece por A: ");
		palabra = leer.nextLine();
		
		while (palabra.length() < 5 || palabra.startsWith("A") == true) {
			System.out.println("Vuelva a escribir la palabra");
			palabra = leer.nextLine();
		}
		
		palabra = palabra.toLowerCase();
		palabra = palabra.trim();

		System.out.println(palabra);
		
		leer.close();

	}

}
