package modelo;

import java.util.Objects;

public class Pelicula {
	
	String titulo;
	Integer id;
	Integer longitud;
	

	public Pelicula() {
		
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getLongitud() {
		return longitud;
	}


	public void setLongitud(Integer longitud) {
		this.longitud = longitud;
	}


	@Override
	public int hashCode() {
		return Objects.hash(id);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pelicula other = (Pelicula) obj;
		return Objects.equals(id, other.id);
	}
	
	

}
