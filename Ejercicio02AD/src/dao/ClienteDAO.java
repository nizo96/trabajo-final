package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;

public class ClienteDAO {
	
	public List<Cliente> obtenerClientes(Connection conn){
		List<Cliente> lista = new ArrayList<>();
		try {
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM CUSTOMERS");
			
			
			while (rs.next()) {
				Cliente cliente = new Cliente();
	            cliente.setFirstName(rs.getString("FIRST_NAME"));
	            cliente.setLastName(rs.getString("LAST_NAME"));
	            cliente.setEmail(rs.getString("EMAIL"));
	            cliente.setActivo(rs.getBoolean("ACTIVE"));
	            lista.add(cliente);
			}
			if (stmt != null) {
				stmt.close();
			}
			
;	
		} catch (SQLException e) {
			e.getStackTrace();
		}
		return lista;
		
	}

}
