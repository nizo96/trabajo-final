package ejercicios;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String email;
		Boolean emailValido;
		do {
			System.out.println("Dame un email");
			email = scanner.nextLine();
			emailValido = true;
			Integer indiceArroba = email.indexOf("@");
			String subcadena = email.substring(indiceArroba + 1);
			
			if (!email.contains("@")){
				emailValido = false;
				System.out.println("El email tiene que tener una @");
			}
			else if (email.contains("@.")){
				emailValido = false;
				System.out.println("El email no puede contener @.");
			}
			else if (email.endsWith(".")) {
				emailValido = false;
				System.out.println("El email no puede terminar en .");
			}
			else if (!subcadena.contains(".")) {
				emailValido = false;
				System.out.println("El email no tiene un . despus de la @");
			}
			
			
		}
		while(!emailValido);
		System.out.println("Email correcto");
		scanner.close();
	}

}

