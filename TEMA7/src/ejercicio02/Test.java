package ejercicio02;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Test extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7377431923104660976L;
	private JPanel contentPane;
	private JTextFieldCheckImpl textFieldCheck;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Test frame = new Test();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Test() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(54, 65, 86, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(54, 110, 86, 14);
		contentPane.add(lblApellidos);
		
		textFieldCheck = new JTextFieldCheckImpl(true);
		textFieldCheck.setBounds(185, 65, 153, 20);
		contentPane.add(textFieldCheck);
		
		textField = new JTextField();
		textField.setBounds(185, 110, 120, 20);
		contentPane.add(textField);
		textField.setColumns(8);
		
		JButton btnActivar = new JButton("Activar");
		btnActivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldCheck.setActivo(true);
			}
		});
		btnActivar.setBounds(51, 188, 89, 23);
		contentPane.add(btnActivar);
		
		JButton btnDesactivar = new JButton("Desactivar");
		btnDesactivar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldCheck.setActivo(false);
			}
		});
		btnDesactivar.setBounds(159, 188, 109, 23);
		contentPane.add(btnDesactivar);
		
		JButton btnGetText = new JButton("Get Text");
		btnGetText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(textFieldCheck.getText());
			}
		});
		btnGetText.setBounds(290, 188, 89, 23);
		contentPane.add(btnGetText);

		JButton btnSetText = new JButton("Set Text Blas");
		btnSetText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldCheck.setText("Blas");
			}
		});
		btnSetText.setBounds(390, 188, 110, 23);
		contentPane.add(btnSetText);

	}
}
