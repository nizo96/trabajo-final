package ejercicios;

import java.util.ArrayList;
import java.util.List;

public class ColaCadenas {
	
	private List<String> cola;
	
	public ColaCadenas() {
		cola = new ArrayList<>();
	}
	public void aņadirCadena(String cadena) {
		cola.add(cadena);
	}
	public String sacarCadena() {
		if(cola.isEmpty()) {
			return null;
		}
		String cadena = cola.get(0);
		cola.remove(0);
		return cadena;
	}
	public Integer getTamao() {
		return cola.size();
	}
	
	@Override
	public String toString() {
		return cola.toString();
	}

}
