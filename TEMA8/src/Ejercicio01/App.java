package ejercicio01;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class App {
	
	private Pantalla01View pantalla1;
	private Pantalla02View pantalla2;
	private Pantalla03View pantalla3;
	private JFrame frame;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public App() {
		initialize();
	}
	
	

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		 frame = new JFrame();
	     frame.setBounds(100, 100, 450, 300);
	     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	     
	     pantalla1 = new Pantalla01View(this);
	     pantalla2 = new Pantalla02View(this);
	     pantalla3 = new Pantalla03View(this);
	     
	     irAPantalla1();
	     
	     
	}




	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});


	}
	public void irAPantalla1(String texto) {
		frame.setContentPane(pantalla1);
		frame.revalidate();
		pantalla1.setTextField(texto);
	}
	public void irAPantalla2 (String texto) {
		frame.setContentPane(pantalla2);
		frame.revalidate();
		pantalla2.setTextField(texto);
	}
	public void irAPantalla3(String texto) {
		frame.setContentPane(pantalla3);
		frame.revalidate();
		pantalla3.setTextField(texto);
	}



}
