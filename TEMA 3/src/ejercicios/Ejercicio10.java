package ejercicios;

import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		Integer x;
		Integer j = 0;
		Integer y = 1;
		do {
			System.out.println("Introduce un n�mero mayor que 2");
			Scanner scanner = new Scanner(System.in);
			x = scanner.nextInt();
		} while (x < 2);

		Integer fibonacci[] = new Integer[x];
		fibonacci[0] = 0;
		fibonacci[1] = 1;
		for (int i = 2; i < fibonacci.length; i++) {
			fibonacci[i] = fibonacci[y++] + fibonacci[j++];			
		}
		for (int i = 0; i < fibonacci.length; i++) {
			System.out.println(fibonacci[i] + " ");
		}
	}

}
