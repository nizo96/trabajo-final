package javafx.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController extends AppController{
	
	@FXML
	private Label lbAcceso;
	
	@FXML
	private TextField txUsuario;
	
	@FXML
	private PasswordField txPassword;
	
	@FXML
	public void cambiarLabel() {
		cambiarVista(FxmlPaths.FXML_MENU);
	}
	@FXML
	public void cambiarATabla() {
		cambiarVista(FxmlPaths.FXML_TABLA);
	}
	
}
