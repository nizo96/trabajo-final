package controller;

import app.AppController;
import app.FxmlPaths;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import service.MongoService;

public class LoginController extends AppController{
	
	private MongoService service;
	
	@FXML
	public TextField txIden;
	
	@FXML
	public PasswordField txPass;
	
	@FXML
	public void acceder() {
		service = new MongoService();
		recogerUsuario(Integer.parseInt(txIden.getText()));
		if (service.consultarUsuario(Integer.parseInt(txIden.getText()), txPass.getText())) {
			cambiarVista(FxmlPaths.FXML_BIENVENIDA);
		}
		
	}
	
	@FXML
	public void registrar() {
		cambiarVista(FxmlPaths.FXML_REGISTRO);
	}

}
