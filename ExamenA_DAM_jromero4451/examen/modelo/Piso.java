package modelo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class Piso {
	public Integer identificador;
	public String Ciudad;
	public BigDecimal precioTasacion;
	public LocalDate fechaCons;
	public Boolean pisoVenta;
	public Boolean pisoAlquiler;
	public BigDecimal precioAlquiler;
	public Boolean deLujo;
	public Boolean antiguo;
	public Map<Piso, String> mapaPiso;

	public Piso() {
		mapaPiso = new HashMap<>();
	}

	public BigDecimal getVenta() {
		precioTasacion = new BigDecimal(10.0);
		BigDecimal descuento = new BigDecimal(0.10);
		if (pisoVenta) {
			return precioTasacion.multiply(descuento);
		} else {
			return precioTasacion;
		}
		
	}

	public BigDecimal getAlquiler() {
		if (deLujo) {
			return precioAlquiler = new BigDecimal(800);
		} else {
			return precioAlquiler = new BigDecimal(500);
		}

	}
	
	public Boolean isDeLujo() {
		BigDecimal precio = new BigDecimal(200000);
		if (precioTasacion.equals(precio)) {
			deLujo = true;
		} else {
			deLujo = false;
		}
		return deLujo;
	}
	
	public Boolean isAntiguo() {
		if (fechaCons.getYear() >= 20) {
			antiguo = true;
		} else {
			antiguo = false;
		}
		return antiguo;
	}
	
	public void getIguales() {
		mapaPiso = new HashMap<>();
		Set<Piso> keys = mapaPiso.keySet();
		for (Piso key : keys) {
			if (mapaPiso.containsKey(keys)) {
				System.out.println("Hay IDs iguales");
			} else {
				System.out.println("No hay dos IDs iguales");
			}
		}
	}
	
	public BigDecimal getPrecio() {
		if (pisoVenta) {
			return getVenta();
		} else {
			return getAlquiler();
		}
	}
	


	@Override
	public int hashCode() {
		return Objects.hash(Ciudad, antiguo, deLujo, fechaCons, identificador, mapaPiso, pisoAlquiler, pisoVenta,
				precioAlquiler, precioTasacion);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Piso other = (Piso) obj;
		return Objects.equals(Ciudad, other.Ciudad) && Objects.equals(antiguo, other.antiguo)
				&& Objects.equals(deLujo, other.deLujo) && Objects.equals(fechaCons, other.fechaCons)
				&& Objects.equals(identificador, other.identificador) && Objects.equals(mapaPiso, other.mapaPiso)
				&& Objects.equals(pisoAlquiler, other.pisoAlquiler) && Objects.equals(pisoVenta, other.pisoVenta)
				&& Objects.equals(precioAlquiler, other.precioAlquiler)
				&& Objects.equals(precioTasacion, other.precioTasacion);
	}

	public Integer getIdentificador() {
		return identificador;
	}

	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}

	public String getCiudad() {
		return Ciudad;
	}

	public void setCiudad(String ciudad) {
		Ciudad = ciudad;
	}

	public BigDecimal getPrecioTasacion() {
		return precioTasacion;
	}

	public void setPrecioTasacion(BigDecimal precioTasacion) {
		this.precioTasacion = precioTasacion;
	}

	public LocalDate getFechaCons() {
		return fechaCons;
	}

	public void setFechaCons(LocalDate fechaCons) {
		this.fechaCons = fechaCons;
	}

	public Boolean getPisoVenta() {
		return pisoVenta;
	}

	public void setPisoVenta(Boolean pisoVenta) {
		this.pisoVenta = pisoVenta;
	}

	public Boolean getPisoAlquiler() {
		return pisoAlquiler;
	}

	public void setPisoAlquiler(Boolean pisoAlquiler) {
		this.pisoAlquiler = pisoAlquiler;
	}

	public Boolean getDeLujo() {
		return deLujo;
	}

	public void setDeLujo(Boolean deLujo) {
		this.deLujo = deLujo;
	}

	public Boolean getAntiguo() {
		return antiguo;
	}

	public void setAntiguo(Boolean antiguo) {
		this.antiguo = antiguo;
	}

	public BigDecimal getPrecioAlquiler() {
		return precioAlquiler;
	}

	public void setPrecioAlquiler(BigDecimal precioAlquiler) {
		this.precioAlquiler = precioAlquiler;
	}

	public Map<Piso, String> getMapaPiso() {
		return mapaPiso;
	}

	public void setMapaPiso(Map<Piso, String> mapaPiso) {
		this.mapaPiso = mapaPiso;
	}

	
	



}
