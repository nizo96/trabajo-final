package ejercicio02;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JTextFieldCheckImpl extends JPanel implements JTextFieldCheck {
	private static final long serialVersionUID = 9220219023095274083L;
	private JTextField textField;
	private JTextField textField_1;
	

	/**
	 * Constructor del componente. Se indica por par�metros el n�mero de columnas
	 * con el que se inicializa el campo de texto y el estado del componente
	 * 
	 * @param enabled - Boolean
	 */
	public JTextFieldCheckImpl(Boolean enabled) {
		setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(0, 0, 86, 20);
		add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(114, 156, 86, 20);
		add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Activar");
		btnNewButton.setBounds(21, 233, 89, 23);
		add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Desactivar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(120, 233, 89, 23);
		add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Probar limpiar");
		btnNewButton_2.setBounds(219, 233, 89, 23);
		add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Limpiar");
		btnNewButton_3.setBounds(90, -1, 89, 23);
		add(btnNewButton_3);
	}

	@Override
	public String getText() {
		return null;
	}

	@Override
	public void setText(String txt) {
	}

	@Override
	public Boolean isActivo() {
		return null;
	}

	@Override
	public void setActivo(Boolean activo) {
	}
}
