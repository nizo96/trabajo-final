package servicios;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.time.LocalDate;

import modelo.Piso;
import persistencia.PisosDAO;
import persistencia.ConexionNueva;

public class PisosService {
	private ConexionNueva conexion;
	Scanner scanner;
	private Piso piso;
	
	
	public PisosService() {
		this.conexion = new ConexionNueva();
	}
	
	public void exportarPisos(String ciudad, String rutaFichero) throws SQLException {
		Connection conn = conexion.getNewConnection();
		PisosDAO.consultarPisos(conn, ciudad);
		File file = new File(rutaFichero);
		Integer variable = 0;
		try {
			scanner = new Scanner(file);
			while (scanner.hasNext()) {
				String linea = scanner.nextLine();
				String [] trozosLinea = linea.split(" # ");
				piso.setCiudad(trozosLinea[1]);
				piso.getVenta(trozosLinea[2]);
				piso.getAlquiler(trozosLinea[3]);
				piso.setFechaCons(trozosLinea[4]);
				
			}
		} catch (SQLException ImportExportException) {
			System.out.println("Error consultando datos");
		}
		catch (FileNotFoundException e) {
			System.out.println("Error");
		}
		catch (IOException ImportExportException) {
			System.out.println("Error escribiendo el fichero");
		}
		
	}
	
	public Map<Piso, String> importarPisosEnVenta(String rutaFichero){
		Map<Piso, String> mapa = new HashMap<>();
		File file = new File(rutaFichero);
		try {
			
			scanner = new Scanner(file);
			Boolean ficheroNuevo = file.createNewFile();
			if (ficheroNuevo) {
				
			} else {

			}
			while (scanner.hasNext()) {
				String linea = scanner.nextLine();
				mapa.put(piso, linea);
				
			}
			scanner.close();
			
			
			
		} catch (FileNotFoundException ImportExportException) {
			System.out.println("No se encontro el fichero");
		}
		catch (IOException ImportExportException) {
			System.out.println("No se pudo leer el fichero");
		}
		return mapa;
	}
	

}
