package servicio;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import modelo.City;

public class CityServiceClientImpl {

	private RestTemplate restTemplate;
	private String urlBase;
	private String directorio;

	public CityServiceClientImpl(String urlBase, Integer msTimeout) {
		this.urlBase = urlBase;
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(msTimeout);
		requestFactory.setReadTimeout(msTimeout);
		this.restTemplate = new RestTemplate(requestFactory);

	}

	public List<City> getCities(String filtroDescripcion) throws NotFoundException {
		try {
			String url = urlBase + "/city?filtroDescripcion=" + filtroDescripcion;
			City[] respuesta = restTemplate.getForObject(url, City[].class);
			return Arrays.asList(respuesta);

		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}

	}

	public City getCity(Long id) throws NotFoundException {
		try {
			String url = urlBase + "/city/{id}";
			City respuesta = restTemplate.getForObject(url, City.class, id);
			return respuesta;

		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}
	}

	public City createCity(City city) {
		String url = urlBase + "/city";
		city = restTemplate.patchForObject(url, city, City.class);
		return city;
	}

	public void updateCity(City city) throws NotFoundException {
		try {
			String url = urlBase + "/city";
			restTemplate.put(url, city);

		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}

	}

	public City updateSelectiveCity(City city) throws NotFoundException {
		try {
			String url = urlBase + "/city";
			city = restTemplate.patchForObject(url, city, City.class);
			return city;

		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}

	}

	public void deleteCity(Long id) throws NotFoundException {
		try {
			String url = urlBase + "/city?id" + id;
			restTemplate.delete(url);

		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}

	}

}
