package gui;

import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class App implements KeyListener{

	private JFrame frame;
	private JTextField tfNombre;
	private JTextField tfApellido;
	private JTable table;
	private TableModel modelo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 550, 486);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(68, 126, 388, 267);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		modelo = new TableModel();
		table.setModel(modelo);
		
		tfNombre = new JTextField();
		tfNombre.setBounds(68, 45, 161, 20);
		frame.getContentPane().add(tfNombre);
		tfNombre.setColumns(10);
		tfNombre.addKeyListener(this);
		
		tfApellido = new JTextField();
		tfApellido.setColumns(10);
		tfApellido.setBounds(295, 45, 161, 20);
		tfApellido.addKeyListener(this);
		frame.getContentPane().add(tfApellido);
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getSource() == tfNombre 
				&& e.getKeyCode() == KeyEvent.VK_ENTER
				&& !tfNombre.getText().isEmpty()) {
			tfApellido.requestFocus();
		}
		else if (e.getSource() == tfApellido
				&& e.getKeyCode() == KeyEvent.VK_ENTER
				&& !tfApellido.getText().isEmpty()) {
			Persona persona = new Persona();
			persona.setNombre(tfNombre.getText());
			persona.setApellido(tfApellido.getText());
			modelo.addPersona(persona);
			modelo.fireTableDataChanged();
			
			tfNombre.setText("");
			tfApellido.setText("");
			tfNombre.requestFocus();
		}
			
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}
}
