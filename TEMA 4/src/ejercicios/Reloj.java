package ejercicios;

import java.util.Objects;

public class Reloj {
	private Integer horas;
	private Integer minutos;
	private Integer segundos;
	private Boolean formato24Horas;

	public Reloj() {
		this.horas = 0;
		this.minutos = 0;
		this.segundos = 0;
		this.formato24Horas = true;
	}

	@Override
	public String toString() {
		if (!check(horas, minutos, segundos)) {
			return "HORA INCORRECTA";
		} else {
			if (formato24Horas) {
				return "Hora actual ==>" + horas + ":" + minutos + ":" + segundos;
			} else {
				Integer hora = horas - 12;
				if (hora >= 0 && hora <= 12) {
					String amOrPm = "pm";
					return "Hora actual ==>" + hora + ":" + minutos + ":" + segundos + amOrPm;
				} else {
					String amOrPm = "am";
					return "Hora actual ==>" + hora + ":" + minutos + ":" + segundos + amOrPm;

				}
			}
		}

	}

	public Reloj(Integer horas, Integer minutos, Integer segundos) {
		this.formato24Horas = true;
	}

	public void ponerEnHora(Integer horas, Integer minutos) {
		this.horas = horas;
		this.minutos = minutos;
	}

	public void ponerEnHora(Integer horas, Integer minutos, Integer segundos) {
		this.horas = horas;
		this.minutos = minutos;
		this.segundos = segundos;
	}

	public Boolean check(Integer horas, Integer minutos, Integer segundos) {
		if ((horas >= 0 && horas <= 23) && (minutos >= 0 && minutos <= 59) && (segundos >= 0 && segundos <= 59)) {
			return true;
		} else {
			return false;
		}
	}


	@Override
	public int hashCode() {
		return Objects.hash(horas, minutos, segundos);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reloj other = (Reloj) obj;
		return Objects.equals(horas, other.horas) && Objects.equals(minutos, other.minutos)
				&& Objects.equals(segundos, other.segundos);
	}

	public Integer getHoras() {
		return horas;
	}

	public Integer getMinutos() {
		return minutos;
	}

	public Integer getSegundos() {
		return segundos;
	}

	public Boolean getFormato24Horas() {
		return formato24Horas;
	}

	public void setFormato24Horas(Boolean formato24Horas) {
		this.formato24Horas = formato24Horas;
	}

}
