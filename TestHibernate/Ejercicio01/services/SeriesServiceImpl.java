package services;

import java.util.List;

import org.hibernate.Session;

import modelo.Serie;
import util.HibernateUtil;

public class SeriesServiceImpl implements SeriesService {

	@Override
	public Serie consultarSerie(Long idSerie) throws SerieNotFoundException, SeriesServiceException {
		Session session = null;
		Serie serie = new Serie();
		
		try {
			session = HibernateUtil.getSessionFactoy().openSession();
			session.getTransaction().begin();
			session.persist(serie);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			
		}
		return null;
	}
	
	
	@Override
	public List<Serie> buscarSeries(String filtroDescripcion) throws SerieNotFoundException, SeriesServiceException {
		return null;
		// TODO: IMPLEMENTAR POR EL ALUMNO...
		
	}
	
	@Override
	public Serie crearSerie(Serie serie) throws SeriesServiceException {
		return null;
		// TODO: IMPLEMENTAR POR EL ALUMNO...
		
	}
	
	
	@Override
	public void elimnarSerie(Long idSerie) throws SeriesServiceException {
		// TODO: IMPLEMENTAR POR EL ALUMNO...
		
	}
	
	
	@Override
	public void actualizarSerie(Serie serie) throws SeriesServiceException {
		// TODO: IMPLEMENTAR POR EL ALUMNO...
		
	}
	
	
}
