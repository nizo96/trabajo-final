package examen;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Granja {

	private String localidad;
	private List<Animal> listaAnimales;

	public Granja(String localidad) {
		this.localidad = localidad;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public void addAnimal(Animal animal) {
		this.listaAnimales.add(animal);
	}

	public BigDecimal getCantidadTotal() {
		BigDecimal cantidadTotal = new BigDecimal(listaAnimales.size());
		return cantidadTotal;
	}

	public BigDecimal getPesoTotal() {
		BigDecimal pesoTotal = new BigDecimal(0);
		for (int i = 0; i < listaAnimales.size(); i++) {
			pesoTotal.add(listaAnimales.get(i).getPeso());
		}
		return pesoTotal;
	}

	public BigDecimal calcularPrecioVenta() {
		BigDecimal precioVentaTotal = new BigDecimal(0);
		for (int i = 0; i < listaAnimales.size(); i++) {
			Integer comparando = listaAnimales.get(i).getPeso().compareTo(listaAnimales.get(i).getPesoMinimo());
			if (comparando >= 0) {
				precioVentaTotal.add(listaAnimales.get(i).getPeso().multiply(listaAnimales.get(i).getPrecioVenta()));
			} else {
				listaAnimales.remove(i);
			}
		}
		return precioVentaTotal.setScale(2, RoundingMode.HALF_UP);
	}

	public void venderAnimales() {
		Integer comparando;
		for (int i = 0; i < listaAnimales.size(); i++) {
			comparando = listaAnimales.get(i).getPeso().compareTo(listaAnimales.get(i).getPesoMinimo());
			if (comparando >= 0) {
				listaAnimales.remove(i);
			}
		}

	}

	@Override
	public String toString() {
		return "Granja de " + localidad + "//" + getCantidadTotal() + "animales con" + getPesoTotal() + " kilos";
	}

}
