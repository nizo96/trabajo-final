package services;

import java.sql.Connection;
import java.sql.SQLException;

import dao.ConnectorProvider;
import dao.UsuarioDAO;
import gui.InitialView;
import modelo.Usuario;

public class UsuarioService {

	private InitialView pantallaInicio;

	public Usuario crearLogin() {
		Connection conn = null;
		Usuario usu = new Usuario();
		try {
			conn = new ConnectorProvider().getNewConnection();
			UsuarioDAO usuario = new UsuarioDAO();

			usu = usuario.login(pantallaInicio.getTxtUsuario(), pantallaInicio.getPasswordField().toString(), conn);

			return usu;
		} catch (SQLException e) {
			System.err.println("Error insertando datos");
			e.printStackTrace();
			return null;
		}
	}
}
