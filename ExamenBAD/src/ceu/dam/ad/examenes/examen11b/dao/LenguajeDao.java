package ceu.dam.ad.examenes.examen11b.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;

public interface LenguajeDao {

	/** Devolver� una lista de lenguajes consultados en la tabla "Language"
	 * La consulta filtrar� la columna nombre por el par�metro indicado. El filtro se aplicar�
	 * buscando los lenguajes que TERMINEN por el filtro indicado.
	 * El filtro podr�a ser NULL. En tal caso, no se filtrar�, sino que devolver�n todos.
	 * La conexi�n a la bbdd deber� realizarse con el connection recibido por par�metro	 */
	public List<Lenguaje> consultarLenguajes(Connection conn, String filtroNombre) throws SQLException;
	
	
}
