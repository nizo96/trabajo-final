package ceu.dam.ad.examenes.examen11b.restServicesClients;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;
import ceu.dam.ad.examenes.examen11b.restServices.LenguajeRestServiceImpl;

public class LenguajeClientRestServiceImpl implements LenguajeClientRestService {
	private RestTemplate restTemplate;
	private String urlBase;
	private List<Lenguaje> lista;
	private LenguajeRestServiceImpl rest;
	
	public LenguajeClientRestServiceImpl(String urlBase, Integer msTimeout) {
		this.urlBase = urlBase;
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(msTimeout);
		requestFactory.setReadTimeout(msTimeout);
		this.restTemplate = new RestTemplate(requestFactory);
	}

	@Override
	public List<Lenguaje> consultarLenguajes(String filtroNombre) throws NotFoundException, AccessDatabaseException {
		lista = new ArrayList<>();
		rest = new LenguajeRestServiceImpl();
		try {
			String url = urlBase + "/city?filtroNombre=" + filtroNombre;
			lista = rest.consultarLenguajes(filtroNombre);
			lista = Arrays.asList(restTemplate.getForObject(url, Lenguaje.class));
			if (lista == null) {
				throw new NotFoundException();
			}
				return lista;
			
		} catch (AccessDatabaseException e) {
			throw new AccessDatabaseException();
		}
	}


}
