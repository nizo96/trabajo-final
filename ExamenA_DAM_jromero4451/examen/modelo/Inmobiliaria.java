package modelo;

import modelo.Piso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

public class Inmobiliaria extends Piso{
	public String nombre;
	public List<Piso> listaPiso;
	
	public Inmobiliaria(){
		
	}
	
	public BigDecimal getAntiguedadMedia() {
		listaPiso = new ArrayList<>();
		Integer a�os = 0;
		Integer media = 0;
		for (Piso piso : listaPiso) {
			++media;
			a�os = a�os + piso.getFechaCons().getYear();
		}
		media = a�os / media;
		BigDecimal total = new BigDecimal(media);
		return total;
	}
	
	public Map<Piso, String> getMapaPisos(){
		Map<Piso, String>listaMapa1 = new HashMap<>();
		for (Entry<Piso, String> piso : listaMapa1.entrySet()) {
			Piso key = piso.getKey();
			String val = piso.getValue();
		}
		return listaMapa1;
	}
	
	public void borrarAntiguosDeLujo() {
		Integer contador = 0;
		for (Piso piso : listaPiso) {
			contador ++ ;
			if (deLujo & antiguo) {
				listaPiso.remove(contador);
			}
		}
		}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(listaPiso, nombre);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inmobiliaria other = (Inmobiliaria) obj;
		return Objects.equals(listaPiso, other.listaPiso) && Objects.equals(nombre, other.nombre);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Piso> getListaPiso() {
		return listaPiso;
	}

	public void setListaPiso(List<Piso> listaPiso) {
		this.listaPiso = listaPiso;
	}
	

}
