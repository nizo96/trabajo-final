package ventanas;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.GridLayout;

public class Ejercicio02 extends JFrame{

	private static final long serialVersionUID = -1290173206042599575L;

	public Ejercicio02() {
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Ejercicio 02");
		addComponentes();
	
		
	}
	
	public void addComponentes() {
		GridLayout layoutPrincipal = new GridLayout(4,1);
		getContentPane().setLayout(layoutPrincipal);
		
		
		JButton boton1 = new JButton();
		boton1.setText("Boton 1");
		getContentPane().add(boton1);
		boton1.setActionCommand("Boton 1");
		
		JButton boton2 = new JButton();
		boton2.setText("Boton 2");
		getContentPane().add(boton2);
		boton2.setActionCommand("Boton 2");
		
		JButton boton3 = new JButton();
		boton3.setText("Boton 3");
		getContentPane().add(boton3);
		boton3.setActionCommand("Boton 3");
		
		JLabel label = new JLabel();
		label.setText("");
		getContentPane().add(label);
		
		BotonEjercicio02 accion = new BotonEjercicio02(label);
		boton1.addActionListener(accion);
		boton2.addActionListener(accion);
		boton3.addActionListener(accion);
	}
	
	

}
