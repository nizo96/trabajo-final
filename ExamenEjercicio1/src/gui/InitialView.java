package gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import services.AccesoDenegadoException;

public class InitialView extends AbstractView {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtAcceso;
	private AppController appController;
	private TablaView tabla;
	
	public InitialView(AppController app) {
		
		setLayout(null);
		
		JLabel lbBlaSearch = new JLabel("BlaSearch");
		lbBlaSearch.setBounds(111, 30, 230, 84);
		Font fuente = new Font("Freestyle Script", Font.PLAIN, 80);
		lbBlaSearch.setFont(fuente);
		add(lbBlaSearch);
		
		JComboBox<String> comboBox = new JComboBox<>();
		comboBox.addItem("Anonimo");
		comboBox.addItem("Identificado");
		comboBox.setBounds(164, 174, 86, 22);
		add(comboBox);
		
		txtAcceso = new JTextField();
		txtAcceso.setBounds(164, 219, 86, 20);
		add(txtAcceso);
		txtAcceso.setColumns(10);
		
		JLabel lbLogin = new JLabel("Tipo de login");
		lbLogin.setBounds(91, 178, 60, 14);
		add(lbLogin);
		
		JLabel lbAcceso = new JLabel("Codigo de acceso");
		lbAcceso.setBounds(70, 222, 84, 14);
		add(lbAcceso);
		
		JButton btEntrar = new JButton("Entrar");
		btEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (comboBox.equals("Identificado") || txtAcceso.getText().equals("blas")) {
					try {
						appController.login(txtAcceso.getText());
						appController.changeView(tabla);
					} catch (AccesoDenegadoException e1) {
						e1.printStackTrace();
					}
				}
				if (comboBox.equals("Anonimo")) {
					txtAcceso.setEnabled(false);
				}
				
			}
		});
		btEntrar.setBounds(338, 201, 89, 38);
		add(btEntrar);
		
		JButton btSalir = new JButton("Salir");
		btSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btSalir.setBounds(338, 250, 89, 39);
		add(btSalir);
		
		
	}
	
	@Override
	public void initialize() {
		
	}
}
