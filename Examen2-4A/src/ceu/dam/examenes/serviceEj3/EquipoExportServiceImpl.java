package ceu.dam.examenes.serviceEj3;

import java.io.File;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Equipo;
import ceu.dam.examenes.serviceEj1.EquipoServiceImpl;

public class EquipoExportServiceImpl implements EquipoExportService{

	
	public void exportar(String filePath, String codigoEquipo) throws NotFoundException, ErrorServiceException{
		EquipoServiceImpl service = new EquipoServiceImpl();
		try {
			Equipo equipo = new Equipo();
			equipo = service.consultarEquipo(codigoEquipo);
			File file = new File(filePath);
			XmlMapper mapper = new XmlMapper();
			mapper.writeValue(file, equipo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("No se encuentra el equipo", e);
		}
	}
	
}
