package ejercicios;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {
		double precioSinIVA;
		
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Diga cual es el precio sin IVA: ");
		
		precioSinIVA = leer.nextDouble();
		
		double precioTotal;
		double precioConIVA;
		double IVA = 0.21;
		precioTotal = precioSinIVA * IVA;
		precioConIVA = precioTotal + precioSinIVA;
		
		
		System.out.println("El precio del producto es: " + precioConIVA);
		
	}

}
