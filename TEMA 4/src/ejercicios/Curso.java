package ejercicios;

public class Curso {
	private String identificador;
	private String descripcion;
	private Alumno alumnos[];

	public Curso(Integer tama�o) {
		Alumno alumnos[] = new Alumno[tama�o];
		this.alumnos = alumnos;
	}

	public void addAlumno(Alumno alumno) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] == null) {
				alumnos[i] = alumno;
				return;
			}
		}
	}

	public Alumno[] getAlumnos() {
		return alumnos;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
