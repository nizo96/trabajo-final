package ejercicios;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		int multiplo;
		int contador = -1;
		int resultado;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Ponga el n�mero del que desea la tabla de multiplicar: ");
		
		multiplo = leer.nextInt();
		
		do {
			contador++;
			resultado = multiplo * contador;
			System.out.println("El multiplo " + multiplo + " multiplicado por " + contador + " nos da: " + resultado);
		
		}
		
		while (contador < 10);
		
		leer.close();
		

	}

}
