
package ejercicios;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Ejercicio02 {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		try {
			List<Integer> numeros = new ArrayList<>();
			Integer numero = 0;
			do {
				try {
					System.out.println("Dame un nmero");
					numero = leer.nextInt();
					if (!numero.equals(-1)) {
						numeros.add(numero);
					}

				} catch (InputMismatchException e) {
					System.out.println("No puedes introducir letras");
					leer.nextLine();

				}

			} while (!numero.equals(-1));
			System.out.println("Lista: " + numeros);

		} finally {
			leer.close();
		}

	}

}
