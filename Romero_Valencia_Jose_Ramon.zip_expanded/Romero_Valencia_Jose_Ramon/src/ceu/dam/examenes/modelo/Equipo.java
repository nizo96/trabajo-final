package ceu.dam.examenes.modelo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
@JsonPropertyOrder({"codigo", "nombre", "entrenador", "jugadores"}) // TODO: ABEL: no te hace falta
public class Equipo {
	@Id
	private String codigo;

	private String nombre;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER )
	@JoinColumn(name = "id_entrenador")
	//fetch consultar
	// cascade actualizar, insertar, borrar
	// TODO: ABEL: no puedes poner un mappedBy si es unidireccional.
	// TODO: ABEL: te falta JOIN COLUMN
	private Entrenador entrenador;
	
	
	@OneToMany(cascade = CascadeType.ALL,  fetch = FetchType.EAGER) 
	@JoinColumn(name = "cod_equipo", nullable = false)
	// TODO: ABEL: no puedes poner un mappedBy si es unidireccional.
	// TODO: ABEL: te falta JOIN COLUMN. Y poner el fetch EAGER
	@JacksonXmlElementWrapper(useWrapping = false) // TODO: ABEL: sí que usamos wrapping, pero tienes que cambiarle el nombre
	@JsonProperty(value = "libro") // TODO: ABEL: ¿libro?
	private List<Jugador> jugadores;

	public Equipo() {
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Entrenador getEntrenador() {
		return entrenador;
	}

	public void setEntrenador(Entrenador entrenador) {
		this.entrenador = entrenador;
	}

	public List<Jugador> getJugadores() {
		return jugadores;
	}

	public void setJugadores(List<Jugador> jugadores) {
		this.jugadores = jugadores;
	}

	@Override
	public String toString() {
		return "Equipo [codigo=" + codigo + ", nombre=" + nombre + ", entrenador=" + entrenador + ", jugadores="
				+ jugadores + "]";
	}

}
