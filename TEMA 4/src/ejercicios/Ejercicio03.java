package ejercicios;

import java.util.Scanner;

public class Ejercicio03 {
	
public static String getMayMin(String frase, Integer numero) {
	switch (numero) {
	case 1:
		frase = Ejercicio02.getMinusculas(frase);
		break;
	case 2:
		frase = Ejercicio01.getMayusculas(frase);
		break;
	default:
		frase = "Error, numero mal";
		break;
	}
	return frase;
}

	public static void main(String[] args) {
		Integer numero;
		String pruebafrase;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Ponga una frase");
		pruebafrase = leer.nextLine();
		
		System.out.println("1 para minuscula, 2 para mayuscula");
		numero = leer.nextInt();
		
		System.out.println(getMayMin(pruebafrase, numero));
		
		leer.close();
		}

}
