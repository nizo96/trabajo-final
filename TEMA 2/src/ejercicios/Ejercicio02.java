package ejercicios;

import java.util.Scanner;
import java.lang.Math;

public class Ejercicio02 {

	public static void main(String[] args) {
		double peso;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("�Cual es tu peso?");
		
		peso = leer.nextDouble();
		
		double altura;
		System.out.println("�Cual es tu altura?");
		
		altura = leer.nextDouble();
		
		double IMC = peso / Math.pow(altura,2);
		
		System.out.println("Su IMC es de: " + IMC);
		
		
		
		if (IMC < 18.5) {
			System.out.println("/n Peso inferior al normal");	
		}
		else if ((IMC >= 18.5) && (IMC <= 24.9)) {
			System.out.println("/n Normal");
		}
		else if ((IMC >= 25.0) && (IMC <= 29.9)) {
			System.out.println("/n Superior al Normal");
		}
		else if (IMC > 30.0) {
			System.out.println("/n Normal");
		}
		leer.close();
	}

}
