package gui;

import java.awt.Dimension;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class InitialView extends AbstractView {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InitialView(AppController appController) {
		setLayout(null);
		JPanel panel = new JPanel();
		panel.setBounds(130, 88, 115, 88);
		add(panel);
		
		JLabel label1 = new JLabel();
		
		ImageIcon icon = new ImageIcon("E:\\PROGRAMACION\\programacion\\programacion\\AplicacionMenu\\imagen.jpg");
		
		Image img = icon.getImage();
		
		Dimension d = panel.getSize();
		int alto1 = d.height;
		int ancho1 = d.width;
		
		Image nuevaImagen = img.getScaledInstance(ancho1, alto1, java.awt.Image.SCALE_SMOOTH);
		
		Icon nuevoIcono = new ImageIcon(nuevaImagen);
		label1.setIcon(nuevoIcono);
		label1.setBounds(0, 0, ancho1, alto1);
		panel.add(label1);
		
		repaint();
		
		
		
	}
	
	@Override
	public void initialize() {
		
		
		
	}
}
