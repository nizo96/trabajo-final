package ejercicio03;

public interface JTextFieldClean {

	/** Limpia el contenido del texto del campo */
	public void clean();
	
	/** Activa o desactiva el bot�n de limpiar */
	public void enableButton(Boolean enable);
	
}
