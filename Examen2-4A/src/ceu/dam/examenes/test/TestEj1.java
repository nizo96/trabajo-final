package ceu.dam.examenes.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Entrenador;
import ceu.dam.examenes.modelo.Equipo;
import ceu.dam.examenes.modelo.Jugador;
import ceu.dam.examenes.serviceEj1.EquipoService;
import ceu.dam.examenes.serviceEj1.EquipoServiceImpl;

public class TestEj1 {

	public static void main(String[] args) {
		EquipoService service = new EquipoServiceImpl();
		Scanner scanner = new Scanner(System.in);

		try {
			System.out.println(">>>> Probando a crear nuevo equipo...");
			Equipo equipo = new Equipo();
			equipo.setCodigo("RM");
			equipo.setNombre("Real Madrid");
			Entrenador entrenador = new Entrenador();
			entrenador.setNombre("Friedrich Nietzsche");
			entrenador.setSueldo(new BigDecimal(45646.56));
			equipo.setEntrenador(entrenador);
			equipo.setJugadores(new ArrayList<>());
			for (int i = 0; i < 3; i++) {
				Jugador j = new Jugador();
				j.setDorsal(i+10);
				j.setNombre("blas " + i + 1);
				equipo.getJugadores().add(j);
			}
			service.crearEquipo(equipo);
			System.out.println(">>>> Equipo creado ");
			System.out.println(">>>> Repasa las tablas en BBDD. ¿Está todo OK y quieres continuar con más pruebas? (S/N)");
			String r = scanner.nextLine();
			if (!r.equalsIgnoreCase("S")) {
				return;
			}
			
			System.out.println(">>>> Probando a consultar equipo que SI existe con código = RM ");
			try {
				equipo = service.consultarEquipo("RM");
				System.out.println(">>>> Equipo consultado: ");
				System.out.println(equipo);
				System.out.println(">>>> Revisa que estén todos los datos completos y correctos. ¿Quieres continuar con más pruebas? (S/N) ");
			}
			catch(NotFoundException e) {
				System.out.println(">>>> Error al consultar equipo. Dice que el equipo RM no existe. Pero se supone que lo acabamos de insertar!! ");
				throw e;
			}
			r = scanner.nextLine();
			if (!r.equalsIgnoreCase("S")) {
				return;
			}			
			
			System.out.println(">>>> Probando a consultar equipo que NO existe con código = BLAS ");
			try {
				equipo = service.consultarEquipo("BLAS");
				System.out.println(">>>> No estás lanzando error para un equipo que no existe. Revisa tu código !!!");
			}
			catch(NotFoundException e) {
				System.out.println(">>>> Error controlado correctamente para equipo inexistente. ");
				System.out.println("TODAS LAS PRUEBAS OK !!");
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(">>>> Algo no va bien... revisa tu código");
		} 
		finally {
			scanner.close();
		}

	}

}
