package ventanas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class BotonEjercicio02 implements ActionListener {

	public JLabel label;

	public BotonEjercicio02(JLabel label) {
		this.label = label;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Boton 1")) {
			label.setText("Has pulsado el boton 1");
		}

		if (e.getActionCommand().equals("Boton 2")) {
			label.setText("Has pulsado el boton 2");
		}

		if (e.getActionCommand().equals("Boton 3")) {
			label.setText("Has pulsado el boton 3");
		}
	}
}