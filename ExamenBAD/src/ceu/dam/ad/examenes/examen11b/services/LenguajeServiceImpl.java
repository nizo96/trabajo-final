package ceu.dam.ad.examenes.examen11b.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ceu.dam.ad.examenes.examen11b.dao.ConnectorProvider;
import ceu.dam.ad.examenes.examen11b.dao.LenguajeDaoImpl;
import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;

public class LenguajeServiceImpl implements LenguajeService {

	private LenguajeDaoImpl dao;
	private ConnectorProvider proveedorConn;

	public LenguajeServiceImpl() {
		proveedorConn = new ConnectorProvider();
	}

	@Override
	public List<Lenguaje> consultarLenguajes(String filtroNombre) throws NotFoundException, AccessDatabaseException {
		List<Lenguaje> lengua = new ArrayList<>();
		Connection conn = null;

		try {
			conn = proveedorConn.getNewConnection();
			lengua = dao.consultarLenguajes(conn, filtroNombre);
			return lengua;
		} catch (SQLException e) {
			throw new AccessDatabaseException();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unused")
	@Override
	public Map<Long, Lenguaje> consultarMapaLenguajes(String filtroNombre) throws NotFoundException, AccessDatabaseException {
		List<Lenguaje> lengua = new ArrayList<>();
		Map<Long, Lenguaje> mapaLengua = new HashMap<>();
		
		try {
			lengua = consultarLenguajes(filtroNombre);
			for (Iterator<Lenguaje> iterator = lengua.iterator(); iterator.hasNext();) {
				Lenguaje lenguaje = (Lenguaje) iterator.next();
				mapaLengua.put(lenguaje.getId(), lenguaje);
			}
			if (mapaLengua == null) {
				// Nose porque me da un warning, pero lo suprimiré
				throw new NotFoundException();
			} else {
				return mapaLengua;		
			}
			
		} catch (AccessDatabaseException e) {
			throw new AccessDatabaseException();
		} 
		
	}

}
