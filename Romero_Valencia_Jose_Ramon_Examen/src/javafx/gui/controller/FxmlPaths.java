package javafx.gui.controller;

public class FxmlPaths {
	
	private static final String PATH_BASE = "/javafx/gui/fxml/";
	public static final String FXML_LOGIN = PATH_BASE + "login.fxml";
	public static final String FXML_NUEVO = PATH_BASE + "nuevo.fxml";
	public static final String FXML_TABLA = PATH_BASE + "tabla.fxml";

}
