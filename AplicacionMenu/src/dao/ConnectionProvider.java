package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
	public Connection getNewConnection() throws SQLException {
		String usuario = "sakila"; 
		String password = "sakila";

		String url = "jdbc:mariadb://localhost:3306/sakila";
		String driverClass = "org.mariadb.jdbc.Driver";

		try {
			Class.forName(driverClass);
		} catch (ClassNotFoundException e) {
			System.err.println("No se encuentra el driver JDBC. Revisa su configuración");
			throw new RuntimeException(e);
		}

		Connection conn = DriverManager.getConnection(url, usuario, password);
		return conn;
	}
}
