package gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import modelo.Coche;

public class TableModel extends AbstractTableModel{
	private static final String COLUMNA_MARCA = "Marca";
	private static final String COLUMNA_MODELO = "Modelo";
	private static final String COLUMNA_ANO = "A�O";
	private static final String COLUMNA_MATRICULA = "Matricula";
	private static final String COLUMNA_PRECIO = "Precio";

	/**
	 * 
	 */
	private static final long serialVersionUID = -2412407473703651652L;
	private List<Coche> coche;
	private List<String> columnas;
	
	public TableModel() {
		coche = new ArrayList<>();
		columnas = new ArrayList<>();
		columnas.add(COLUMNA_MARCA);
		columnas.add(COLUMNA_MODELO);
		columnas.add(COLUMNA_ANO);
		columnas.add(COLUMNA_MATRICULA);
		columnas.add(COLUMNA_PRECIO);
	}
	

	@Override
	public int getRowCount() {
		return coche.size();
	}

	@Override
	public int getColumnCount() {
		return columnas.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Coche car = coche.get(rowIndex);
		String columna = columnas.get(columnIndex);
		if (columna.equals(COLUMNA_MARCA)) {
			return car.getMarca();
		}
		if (columna.equals(COLUMNA_MODELO)) {
			return car.getModelo();
		}
		if (columna.equals(COLUMNA_ANO)) {
			return car.getA�o();
		}
		if (columna.equals(COLUMNA_MATRICULA)) {
			return car.getMatricula();
		}
		if (columna.equals(COLUMNA_PRECIO)) {
			return car.getPrecio();
		}
		return "Error";
	}
	
	public void setDatos(List<Coche> coche) {
		this.coche = coche;
	}

}
