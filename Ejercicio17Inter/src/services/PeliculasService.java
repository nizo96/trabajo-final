package services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import dao.ConnectionProvider;
import dao.PeliculasDao;
import modelo.Pelicula;

public class PeliculasService {

	public List<Pelicula> consultarPeliculasCortas() throws PeliculasException {
		Connection conn = null;
		try {
			// 1. Crear una conexin
			conn = new ConnectionProvider().getNewConnection();

			// 2. Llamar al DAO para obtener lista de todas las pelculas
			// pasando la conexin
			PeliculasDao dao = new PeliculasDao();
			List<Pelicula> peliculas = dao.consultarPeliculas(conn);

			// 4. Recorrer la lista y borrar las que tengan ms de 100 min.
			Iterator<Pelicula> it = peliculas.iterator();
			while (it.hasNext()) {
				Pelicula pelicula = (Pelicula) it.next();
				if (pelicula.getLongitud() > 100) {
					it.remove();
				}
			}
			
			// 5. Devolver la lista
			return peliculas;
			
		} 
		catch (SQLException e) {
			System.err.println("Error accediendo o consultando en BBDD " + e.getMessage());
			throw new PeliculasException("Error accediendo o consultando en BBDD", e);
		} 
		finally {
			// 3. Cerrar conexin
			try {
				conn.close();
			} catch (Exception ignore) {
			}
		}

	}

}
