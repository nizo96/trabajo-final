package ejercicios;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		String fraseUsu;
		Scanner leer = new Scanner(System.in);

		
		do {
			System.out.println("Introduzca la frase, donde comience con 'Hola' y termine con 'Hasta luego': ");
			fraseUsu = leer.nextLine();
		}
		while ((!fraseUsu.startsWith("Hola")) || (!fraseUsu.endsWith("Hasta luego")));
		
		Integer tamano = fraseUsu.length();
		Integer resultado = tamano - 11;
		String frase = fraseUsu.substring(5, resultado);
		System.out.println("Lo he entendido. Mensaje: " + frase);

		leer.close();

	}

}
