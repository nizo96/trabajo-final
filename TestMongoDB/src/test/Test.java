package test;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.InsertOneResult;

import connection.MongoUtil;
import modelo.Persona;

public class Test {
	
	public static void main(String[] args) {
		MongoDatabase db = new MongoUtil().getDatabase("test");
		MongoCollection<Document> personas = db.getCollection("persona");
		
		//Buscar 
		FindIterable<Document> it = personas.find();
		MongoCursor<Document> cursor = it.cursor();
		while (cursor.hasNext()) {
			Document d = cursor.next();
			System.out.println(d.toJson()); // pasarlo a String
		}
		
		//Filtrar
		Bson filtro = Filters.or(Filters.regex("dni", "1234"));  //Filters.eq("dni", "1234X");
		FindIterable<Document> it1 = personas.find(filtro);
		MongoCursor<Document> cursor1 = it1.cursor();
		while (cursor1.hasNext()) {
			Document d = cursor1.next();
			System.out.println("\n" + d);
		}
		
		// sacar exclusivamente las columnas mencionadas(Se a�ade en el personas.find())
		Bson projections = Projections.fields(Projections.include("dni", "nombre"), Projections.excludeId());
		
		// Update  
		Bson update = Updates.set("nombre", "Laura");
		personas.updateOne(filtro, update);
		FindIterable<Document> it2 = personas.find();
		MongoCursor<Document> cursor2 = it2.cursor();
		while (cursor2.hasNext()) {
			Document d = cursor2.next();
			System.out.println(d.toJson()); // pasarlo a String
		}
		
		//Con Pojo
		MongoDatabase db1 = new MongoUtil().getDatabase("test");
		MongoCollection<Persona> personas1 = db1.getCollection("persona", Persona.class);
		
		Persona p = new Persona();
		p.setDni("9876R");
		p.setNombre("Epi");
		p.setApellido("de la Falla");
		p.setEdad(23);
		List<String> telefonos = new ArrayList<>();
		telefonos.add("652176214");
		telefonos.add("632145784");
		telefonos.add("69874652");
		p.setTelefonos(telefonos);
		
		InsertOneResult r = personas1.insertOne(p);
		System.out.println(r.getInsertedId().asObjectId().getValue());
		
		
		//Document p = new Document();
		//p.append("dni", "1234X");
		//p.append("nombre", "Blas");
		//p.append("apellido", "de los montes");
		//List<String> telefonos = new ArrayList<>();
		//telefonos.add("567546767");
		//telefonos.add("123412342");
		//telefonos.add("567029872");
		//p.append("telefonos", telefonos);
		//personas.insertOne(p);
	}

}
