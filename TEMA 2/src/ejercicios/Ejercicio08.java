package ejercicios;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String palabra1, palabra2;
		
		do {
			System.out.println("Necesito que me des dos nombres iguales");
			System.out.println("Dame un primer nombre");
			palabra1 = scanner.nextLine();
			System.out.println("Dame un segundo nombre");
			palabra2 = scanner.nextLine();
			
			palabra1 = palabra1.trim();
			palabra2 = palabra2.trim();
			
		}
		while(!palabra1.equalsIgnoreCase(palabra2));
		
		System.out.println("Los nombres ahora s� son iguales");
		
		scanner.close();
	}

}
