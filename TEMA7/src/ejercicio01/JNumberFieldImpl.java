package ejercicio01;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

public class JNumberFieldImpl extends JTextField implements JNumberField, KeyListener {

	private static final long serialVersionUID = 1880483079420629902L;

	private Integer numero;
	private String cadena;

	public JNumberFieldImpl() {

		this.addKeyListener(this);

	}

	@Override
	public Integer getNumber() {

		if (getText().isEmpty()) {

			numero = 0;

		} else {

			numero = Integer.parseInt(getText());

		}

		return numero;
	}

	@Override
	public void setNumber(Integer number) {

		cadena = number.toString();
		setText(cadena);

	}

	@Override
	public void keyTyped(KeyEvent e) {

		Character key = e.getKeyChar();

		if ((key < '0' || key > '9')) {
			e.consume();
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {

		if (e.getKeyCode() == KeyEvent.VK_UP) {

			if (getText().isEmpty()) {

				setNumber(1);

			} else {

				setNumber(getNumber() + 1);

			}

		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {

			if (getText().isEmpty()) {

				setNumber(0 - 1);

			} else {

				setNumber(getNumber() - 1);

			}

		}

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

}
