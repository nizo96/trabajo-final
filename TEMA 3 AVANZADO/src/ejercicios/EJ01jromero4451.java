package ejercicios;

import java.util.Scanner;

public class EJ01jromero4451 {

	public static void main(String[] args) {
		Integer N;
		Scanner leer = new Scanner(System.in);
		Integer contador = 0;
		Integer condicion = 0;

		System.out.println("Diga un numero: ");
		N = leer.nextInt();

		while (N <= 0) {
			System.out.println("Repita el numero, recuerde debe ser mayor a 0: ");
			N = leer.nextInt();
		}

		contador = N;
		condicion = N;
		do {

			for (int i = 0; i <= contador; i++) {
				System.out.print(N + " ");
				N--;
			}
			System.out.println("");
			N = contador;

			condicion--;

		} while (condicion >= 0);
		
		leer.close();

	}

}
