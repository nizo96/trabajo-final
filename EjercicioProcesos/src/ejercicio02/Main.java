package ejercicio02;

import java.util.concurrent.TimeUnit;

public class Main implements Runnable{
	
	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			Main hilo = new Main();
			Thread hilo1 = new Thread(hilo);
			hilo1.start();
			hilo1.run();
			System.out.println("Sale");
		}
	}

	@Override
	public void run() {
		System.out.println("Entra y espera");
		try {
			TimeUnit.MILLISECONDS.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	

}
