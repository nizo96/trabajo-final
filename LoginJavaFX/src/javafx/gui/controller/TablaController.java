package javafx.gui.controller;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Animal;
import service.AnimalesNotFoundException;
import service.AnimalesServices;

public class TablaController extends AppController{
	
	@FXML
	public TableView<Animal> tabla;
	@FXML
	public TableColumn<Animal, String> tipo;
	@FXML
	public TableColumn<Animal, String> nombre;
	@FXML
	public TableColumn<Animal, Integer> edad;
	@FXML
	public TextField txField;
	
	private ObservableList<Animal> datos;
	
	private AnimalesServices animales;

	@FXML
	public void initialize() {
		datos = FXCollections.observableArrayList();
		tabla.setItems(datos);
		
		PropertyValueFactory<Animal, String> factTipo = new PropertyValueFactory<>("tipo");
		PropertyValueFactory<Animal, String> factNombre = new PropertyValueFactory<>("nombre");
		PropertyValueFactory<Animal, Integer> factEdad = new PropertyValueFactory<>("edad");
		tipo.setCellValueFactory(factTipo);
		nombre.setCellValueFactory(factNombre);
		edad.setCellValueFactory(factEdad);
		
		
	}
	@FXML
	public void consultar() {
		animales = new AnimalesServices();
		List<Animal> animalList = new ArrayList<>();
		try {
			animalList = animales.consultarAnimales(txField.getText());
			datos.setAll(animalList);
		} catch (AnimalesNotFoundException e) {
			e.printStackTrace();
		}
	}
	@FXML
	public void nuevo() {
		cambiarVista(FxmlPaths.FXML_TIPO);
	}
	@FXML
	public void volver() {
		cambiarVista(FxmlPaths.FXML_LOGIN);
	}

}
