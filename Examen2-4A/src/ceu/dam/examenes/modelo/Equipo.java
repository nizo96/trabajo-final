package ceu.dam.examenes.modelo;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

@Entity
public class Equipo {
	@Id
	private String codigo;

	private String nombre;

	@OneToOne(mappedBy = "entrenadores", cascade = CascadeType.ALL)
	private Entrenador entrenador;

	@OneToMany(mappedBy = "jugadores", cascade = CascadeType.ALL)
	private List<Jugador> jugadores;

	public Equipo() {
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Entrenador getEntrenador() {
		return entrenador;
	}

	public void setEntrenador(Entrenador entrenador) {
		this.entrenador = entrenador;
	}

	public List<Jugador> getJugadores() {
		return jugadores;
	}

	public void setJugadores(List<Jugador> jugadores) {
		this.jugadores = jugadores;
	}

	@Override
	public String toString() {
		return "Equipo [codigo=" + codigo + ", nombre=" + nombre + ", entrenador=" + entrenador + ", jugadores="
				+ jugadores + "]";
	}

}
