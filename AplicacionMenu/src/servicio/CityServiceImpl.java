package servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import gui.AltaView;
import modelo.City;

public class CityServiceImpl implements CityService{
	
	private AltaView alta;

	@Override
	public List<City> getCities(String filtroDescripcion) throws NotFoundException {
		List<City> lista = new ArrayList<>();
		if (filtroDescripcion == null || filtroDescripcion.isBlank()) {
			filtroDescripcion = "test";
		}
		if (filtroDescripcion.equalsIgnoreCase("blas")) {
			throw new NotFoundException("No se encontraron ciudades con descripcin " + filtroDescripcion);
		}
		for (long i = 1L; i < 20; i++) {
			City ciudad = new City();
			ciudad.setId(i);
			ciudad.setDescripcion(filtroDescripcion + "-" + i);
			ciudad.setCountryId(i*10);
			lista.add(ciudad);
		}
		return lista;
	}

	@Override
	public City getCity(Long id) throws NotFoundException {
		City ciudad = new City();
		ciudad.setId(id);
		ciudad.setDescripcion("Sevilla");
		ciudad.setCountryId(id*10);
		return ciudad;
	}

	@Override
	public City createCity(City city) {
		alta = new AltaView(null);
		City ciudad = new City();
		ciudad.setId(new Random().nextInt(1000)+100L);
		ciudad.setDescripcion(city.getDescripcion());
		ciudad.setCountryId(city.getCountryId());
		return ciudad;
	}

	@Override
	public void updateCity(City city) throws NotFoundException {
	}

	@Override
	public City updateSelectiveCity(City city) throws NotFoundException {
		return city;
	}

	@Override
	public void deleteCity(Long id) throws NotFoundException {
	}

}
