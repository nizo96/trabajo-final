package ceu.dam.ad.examenes.examen11b.modelo;

import java.util.Date;

// En esta clase, si lo necesit�is, pod�is a�adir m�s cosas.
// Pero no se pueden modificar los m�todos y atributos que ya existen.
public class Lenguaje {
	private Long id;
	private String name;
	private Date lastUpdate;
	
	public Lenguaje() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public String toString() {
		return "Lenguaje [id=" + id + ", name=" + name + ", lastUpdate=" + lastUpdate + "]";
	}

	
	
}
