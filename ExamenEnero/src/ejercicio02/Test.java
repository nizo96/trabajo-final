package ejercicio02;

import java.util.concurrent.Semaphore;

public class Test {
	public static void main(String[] args) {
		Semaphore semaforo = new Semaphore(1);
		Hilo h = new Hilo(semaforo);
		Thread h1 = new Thread(h);
		Thread h2 = new Thread(h);
		Thread h3 = new Thread(h);
		Thread h4 = new Thread(h);
		Thread h5 = new Thread(h);
		h1.start();
		h2.start();
		h3.start();
		h4.start();
		h5.start();
	}

}
