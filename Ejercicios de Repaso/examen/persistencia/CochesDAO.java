package persistencia;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.time.LocalDate;

import modelo.CocheRenting;
import modelo.CocheVenta;
import modelo.Coches;

public class CochesDAO {

	public BigDecimal cambiarCosteCoche(Connection conn, String matricula, BigDecimal costeNuevo) throws SQLException {
		Statement stmt = null;
		ResultSet rs = null;
		Integer contador = 0;
		try {
			stmt = conn.createStatement();
			String sql = "UPDATE CONCESIONARIO SET PRECIO_COSTE, ? WHERE MATRICULA = ?";
			stmt.executeUpdate(sql);
			contador = stmt.getUpdateCount();
		} finally {
			if (rs != null) {

				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		}

		return new BigDecimal(contador);
	}

	public List<Coches> buscarCoche(Connection conn, String marca) throws SQLException {
		Statement stmt = null;
		ResultSet rs = null;
		List<Coches> nuevaLista = new ArrayList<>();

		try {
			stmt = conn.createStatement();
			String sql = "SELECT * FROM CONCESIONARIO WHERE MARCA_COMERCIAL = ?";
			rs = stmt.executeQuery(sql);
			Coches coche;
			if (rs.getString("COMERCIALIZACION").equals("VENTA")) {
				coche = new CocheVenta();

			} else {
				coche = new CocheRenting();

			}

			while (rs.next()) {
				coche.setMatricula(rs.getString("MATRICULA"));
				coche.setMarca(rs.getString("MARCA_COMERCIAL"));
				Date date = rs.getDate("FABRICACION");
				LocalDate fecha = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(date));
				coche.setFechaFab(fecha);
				coche.setPrecioCoste(rs.getBigDecimal("PRECIO_COSTE"));
				nuevaLista.add(coche);

			}
		} finally {
			if (rs != null) {

				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}

		}

		return nuevaLista;

	}

}
