package modelo;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;

@Entity
public class Estadio {
	@Id
	private String codigo;
	
	private String nombre;
	
	private Integer aforo;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_equipo", nullable = false)
	private Equipo equipo;
	
	public Estadio() {
		
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getAforo() {
		return aforo;
	}

	public void setAforo(Integer aforo) {
		this.aforo = aforo;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	@Override
	public String toString() {
		return "Estadio [codigo=" + codigo + ", nombre=" + nombre + ", aforo=" + aforo + "]";
	}

}
