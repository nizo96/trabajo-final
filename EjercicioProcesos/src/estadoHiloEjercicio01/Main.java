package estadoHiloEjercicio01;

public class Main implements Runnable{
	private static boolean stopHilo = false;
	
	public static void main(String[] args) {
			Main hilo = new Main();
			Thread hilo1 = new Thread(hilo);
			hilo1.start();	
			for (int i = 0; i < 100; i++) {
				System.out.println("bucle");
			}
			
			Main.finalizarHilo();
			
	}
	
public static void finalizarHilo() {
		stopHilo = true;
		System.out.println("Fuera del hilo");
}

@Override
public void run() {
	while(!stopHilo) {
		System.out.println("Dentro del hilo");
	}
	
}

}

