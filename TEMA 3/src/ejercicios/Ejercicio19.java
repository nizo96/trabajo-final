package ejercicios;

import java.util.Scanner;

public class Ejercicio19 {

	public static void main(String[] args) {
		String frase;
		Scanner leer = new Scanner(System.in);
		int contador = 0;
		int contarLetras = 0;

		System.out.println("Ponga una frase: ");
		frase = leer.nextLine();
		String minuscula = frase.toLowerCase();
		
		String[] letras = minuscula.split("");

		for (int i = 0; i < letras.length; i++) {
			Boolean contar = letras[i].equals("a");
			if (contar == true) {
				contador++;
			}
		}

		String[] letras2 = minuscula.split(" ");
		for (int i = 0; i < letras2.length; i++) {
			contarLetras++;
		}
		System.out.println(
				"El tama�o de la frase es de: " + contarLetras + " y el numero de a que hay es de: " + contador);
	}

}
