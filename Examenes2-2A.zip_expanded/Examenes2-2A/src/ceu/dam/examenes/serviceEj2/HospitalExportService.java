package ceu.dam.examenes.serviceEj2;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Hospital;

public interface HospitalExportService {

	/** El método recibe un Hospital que debe exportar en formato XML en un fichero en la ruta indicada.
	 * El formato del fichero XML tiene que ser como el ejemplo subido en Moodle en el EJ.2
	 * La exportación a XML Tendrá que realizarse utilizando DOM
	 * Si hay cualquier error, lanzará ErrorServiceException
	 */
	public void exportarHospital(Hospital hospital, String filePath) throws ErrorServiceException;
	
}
