package ejercicio07;

public class Persona {
	
	public static final String DIRECCION 		= "direccin";
	public static final String PRODUCCION 		= "produccin";
	public static final String INTERPRETACION 	= "interpretacin";
	
	private String nombre;
	private String nacionalidad;
	
	public Persona() {

	}

	public Persona(String nombre, String nacionalidad) {
		super();
		this.nombre = nombre;
		this.nacionalidad = nacionalidad;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	
}
