package ejercicios;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		String email;
		Boolean primera, segunda, tercera, total;
		
		Scanner leer = new Scanner(System.in);
		do {
		System.out.println("Ponga un email aqui: ");
		email = leer.nextLine();
		
		primera = email.contains("@");
		segunda = !email.contains("@.");
		tercera = !email.endsWith(".");
		
		}
		while (!primera|| !segunda || !tercera); 
		
		System.out.println("Es correcto el email");
		
		leer.close();
	}

}
