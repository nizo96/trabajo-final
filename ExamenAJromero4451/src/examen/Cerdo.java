package examen;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Cerdo extends Animal {

	public String raza;

	public Cerdo(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
		this.precioVenta = new BigDecimal(4.5);
		this.peso = new BigDecimal(0);
		this.fechaNac = LocalDate.now();

	}

	public BigDecimal meterPeso(BigDecimal pesoCerdo) {
		this.peso = peso.add(pesoCerdo);
		return peso;
	}

	public BigDecimal calcularPrecio(BigDecimal pesoCerdo) {
		this.pesoMinimo = new BigDecimal(100);
		Integer comparando = pesoCerdo.compareTo(pesoMinimo);
		if (comparando >= 0) {
			return pesoCerdo.multiply(precioVenta);
		} else {
			return null;
		}
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

}
