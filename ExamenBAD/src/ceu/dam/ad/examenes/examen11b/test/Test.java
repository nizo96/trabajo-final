package ceu.dam.ad.examenes.examen11b.test;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.exportService.LenguajeExportService;
import ceu.dam.ad.examenes.examen11b.exportService.LenguajeExportServiceImpl;
import ceu.dam.ad.examenes.examen11b.restServicesClients.LenguajeClientRestService;
import ceu.dam.ad.examenes.examen11b.restServicesClients.LenguajeClientRestServiceImpl;

public class Test {

	// Aqu� se proporcionan dos m�todos para probar el ClienteRest (Ejercicio 4) y el ExportService (Ejercicio 5)
	// El alumno es libre de modificarlos o de crear sus propios TEST para lo que considere
	// Los test que se implementen no son evaluables (no suben ni bajan nota)
	
	public static void main(String[] args) {
		testRestServiceClient();
		testExportService();
	}

	private static void testExportService() {
		LenguajeExportService export = new LenguajeExportServiceImpl();
		String pathFileBase = "c:/temporal/";
		try {
			export.exportarLenguajes("e", pathFileBase + "export1.csv"); // deber�a tener s�lo una l�nea con "japanesse"
			export.exportarLenguajes(null, pathFileBase + "export2.csv"); // deber�a tener todos los lenguajes
			export.exportarLenguajes("", pathFileBase + "export3.csv"); // deber�a tener todos los lenguajes
			export.exportarLenguajes("asdfasdf", pathFileBase + "export4.csv"); // deber�a estar vac�o
		
		} catch (AccessDatabaseException e) {
			e.printStackTrace();
		}
	}
	
	private static void testRestServiceClient() {
		LenguajeClientRestService client = new LenguajeClientRestServiceImpl("http://localhost:8080", 1000);
		try {
			System.out.println(">>>>>>>>>>>>");
			System.out.println(client.consultarLenguajes("e"));
			System.out.println("Deber�a haber impreso el lenguaje JAPANESSE");
			System.out.println("<<<<<<<<<<<<");
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (AccessDatabaseException e) {
			e.printStackTrace();
		}
		try {
			System.out.println(">>>>>>>>>>>>");
			System.out.println(client.consultarLenguajes(null));
			System.out.println("Deber�a haber impreso todos los lenguajes");
			System.out.println("<<<<<<<<<<<<");
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (AccessDatabaseException e) {
			e.printStackTrace();
		}
		try {
			System.out.println(">>>>>>>>>>>>");
			System.out.println(client.consultarLenguajes(""));
			System.out.println("Deber�a haber impreso todos los lenguajes");
			System.out.println("<<<<<<<<<<<<");
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (AccessDatabaseException e) {
			e.printStackTrace();
		}
		try {
			System.out.println(">>>>>>>>>>>>");
			System.out.println(client.consultarLenguajes("asdfasdfasdf"));
			System.err.println("No es correcto. Deber�a lanzar un NotFound");
			System.out.println("<<<<<<<<<<<<");
		} catch (NotFoundException e) {
			System.out.println("Correcto!!");
			System.out.println("<<<<<<<<<<<<");
		} catch (AccessDatabaseException e) {
			e.printStackTrace();
		}
	}
	
}
