package javafx.gui.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

public class HolaMundoController {
	
	@FXML
	private TextField tfMensaje;
	
	@FXML
	private CheckBox chkModoOscuro;
	
	@FXML
	public void cambiarModoOscuro(ActionEvent e) {
		if (chkModoOscuro.isSelected()) {
			chkModoOscuro.getScene().getStylesheets().add("/javafx/gui/fxml/dark-theme.css");
		}
		else {
			chkModoOscuro.getScene().getStylesheets().clear();
		}
	}
	
	public void clickAceptar2() {
		System.out.println("Has hecho click en Aceptar");
		tfMensaje.setText("Has clickeado!");
	}

}
