package ejercicios;

import java.util.List;

import ejercicios.Persona;

public interface IUserInterface {

	/** Solicita al usuario el DNI de la persona que quiere consultar */
	public String solicitarDNI();
	
	/** Muestra al usuario los datos de la persona indicada */
	public void mostrarPersona(Persona persona);
	
	/** Muestra al usuario el error indicado */
	public void mostrarError(String error);
	
	/** Solicita al usuario la cadena por la que quiere buscar personas en la bbdd */
	public String solicitarFiltroBusqueda();
	
	/** Muestra al usuario los datos de la lista de personas indicada */
	public void mostrarListaPersona(List<Persona> personas);

	/** Cerramos interfaz de usuario */
	public void cerrarInterfaz();
}
