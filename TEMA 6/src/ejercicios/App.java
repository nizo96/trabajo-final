package ejercicios;

import java.sql.SQLException;
import java.util.List;

import ejercicios.IPersonasDao;
import ejercicios.PersonasDao;
import ejercicios.IUserInterface;
import ejercicios.UserInterface;
import ejercicios.Persona;

public class App {

	public static void main(String[] args) {
		IPersonasDao dao = new PersonasDao();
		IUserInterface gui = new UserInterface();
		
		try {
			String dni = gui.solicitarDNI();
			try {
				Persona persona = dao.consultarPersona(dni);
				gui.mostrarPersona(persona);
			} catch (SQLException e) {
				gui.mostrarError(e.getMessage());
				e.printStackTrace();
			}
			
			String filtro = gui.solicitarFiltroBusqueda();
			try {
				List<Persona> personas = dao.consultarPersonas(filtro);
				gui.mostrarListaPersona(personas);
			} catch (SQLException e) {
				gui.mostrarError(e.getMessage());
				e.printStackTrace();
			}
		}
		finally {
			gui.cerrarInterfaz();
		}
		
	}

	
}

