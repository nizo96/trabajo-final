package modelo;

import java.math.BigDecimal;
import java.util.Objects;

public class LineaPedido {
	
	String idPedido;
	Integer numeroLinea;
	Integer articulo;
	BigDecimal precio;
	
	public String getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(String idPedido) {
		this.idPedido = idPedido;
	}
	public Integer getNumeroLinea() {
		return numeroLinea;
	}
	public void setNumeroLinea(Integer numeroLinea) {
		this.numeroLinea = numeroLinea;
	}
	public Integer getArticulo() {
		return articulo;
	}
	public void setArticulo(Integer articulo) {
		this.articulo = articulo;
	}
	public BigDecimal getPrecio() {
		return precio;
	}
	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}
	@Override
	public int hashCode() {
		return Objects.hash(articulo, idPedido, numeroLinea, precio);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineaPedido other = (LineaPedido) obj;
		return Objects.equals(articulo, other.articulo) && Objects.equals(idPedido, other.idPedido)
				&& Objects.equals(numeroLinea, other.numeroLinea) && Objects.equals(precio, other.precio);
	}
	
	
	
	
	
	

}
