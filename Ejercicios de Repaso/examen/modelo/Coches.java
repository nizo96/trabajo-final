package modelo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

public abstract class Coches {
	public String matricula;
	public String marca;
	public BigDecimal precioCoste;
	public LocalDate fechaFab;

	public Coches() {

	}

	public Boolean isAntiguo() {
		Period period = fechaFab.until(LocalDate.now());
		if (period.getYears() < 3) {
			return false;
		} else {
			return true;
		}
	}

	public Boolean isGamaAlta() {
		Period period = fechaFab.until(LocalDate.now());
		if ((precioCoste.compareTo(new BigDecimal(100000)) >= 0) && period.getYears() <= 1) {
			return true;
		} else {
			return false;
		}
	}

	public BigDecimal getTarifa() {
		return precioCoste;
	}

	@Override
	public int hashCode() {
		return Objects.hash(matricula);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coches other = (Coches) obj;
		return Objects.equals(matricula, other.matricula);
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public BigDecimal getPrecioCoste() {
		return precioCoste;
	}

	public void setPrecioCoste(BigDecimal precioCoste) {
		this.precioCoste = precioCoste;
	}

	public LocalDate getFechaFab() {
		return fechaFab;
	}

	public void setFechaFab(LocalDate fechaFab) {
		this.fechaFab = fechaFab;
	}
}
