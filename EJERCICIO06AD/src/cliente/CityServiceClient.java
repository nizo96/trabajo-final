package cliente;

import java.util.List;

public interface CityServiceClient {

	/** Devuelve la lista de ciudades que devuelva el servicio REST filtrando 
	 * por la descripci�n indicada. Si el servicio devuelve un 404 
	 * lanzaremos la excepci�n NotFoundException 
	 * @param filtroDescripcion - El par�metro se pasar� con este nombre 
	 * @return
	 * @throws NotFoundException
	 */
	public List<City> getCities(String filtroDescripcion) throws NotFoundException;
	
	/** Devuelve la ciudad con el id indicado que devuelva el servicio  
	 * Si el servicio devuelve un 404, lanzaremos la excepci�n NotFoundException 
	 * @param id 
	 * @return
	 * @throws NotFoundException
	 */
	public City getCity(Long id) throws NotFoundException;
	
	/** Invoca al servicio para crear la ciudad que se indica por par�metro. 
	 * Devolver� la respuesta del servicio 
	 * @param city
	 * @return
	 */
	public City createCity(City city);
	
	/** Actualizar� la ciudad indicada por par�metro invocando al servicio. 
	 * Actualizar� todos los valores que lleguen en el objeto, aunque est�n a null
	 * Si el servicio devuelve un 404, lanzaremos la excepci�n NotFoundException 
	 * @param city
	 * @throws NotFoundException
	 */
	public void updateCity(City city) throws NotFoundException;
	
	/** Actualizar� la ciudad indicada por par�metro invocando al servicio. 
	 * Actualizar� s�lo los valores que lleguen informados en el objeto, es decir,
	 * los atributos que vengan a NULL, no se actualizan 
	 * Si el servicio devuelve un 404, lanzaremos la excepci�n NotFoundException 
	 * @param city
	 * @return
	 * @throws NotFoundException
	 */
	public City updateSelectiveCity(City city) throws NotFoundException;
	
	/** Borrar� la ciudad indicada por par�metro invocando al servicio. 
	 * Si el servicio devuelve un 404, lanzaremos la excepci�n NotFoundException 
	 * @param id
	 * @throws NotFoundException
	 */
	public void deleteCity(Long id) throws NotFoundException;
	
}
