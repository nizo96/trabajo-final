package ejercicio01;

public interface JNumberField {

	/** Devuelve el valor num�rico que tenga en ese momento
	 * escrito el campo de texto. Si est� vac�o, devolver� 0  */
	public Integer getNumber();
	
	/** Cambia el texto del campo por el n�mero indicado por
	 * par�metro */
	public void setNumber(Integer number);
}
