package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import services.AccesoDenegadoException;
import services.ExamenService;

public class AppController {

	private JFrame frame;
	private InitialView inicio;
	private ExamenService service;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppController window = new AppController();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AppController() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		inicio = new InitialView(this);
		service = new ExamenService();
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setContentPane(inicio);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		menuBar.setVisible(false);
		
		JMenu menuSesion = new JMenu("Sesi\u00F3n");
		menuBar.add(menuSesion);
		
		JMenuItem optionCerrar = new JMenuItem("Cerrar sesi\u00F3n");
		optionCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changeView(inicio);
			}
		});
		optionCerrar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		menuSesion.add(optionCerrar);
		
		JMenu menuApp = new JMenu("App");
		menuBar.add(menuApp);
		
		JMenuItem OptionSalir = new JMenuItem("Salir");
		OptionSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		OptionSalir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		menuApp.add(OptionSalir);
	}
	
	public void changeView(AbstractView view) { // AbstractView es una clase propia
		frame.setContentPane(view);
		frame.revalidate();
		view.initialize();
	} 
	
	public void login(String texto) throws AccesoDenegadoException {
		service.login(texto);
	}
}
