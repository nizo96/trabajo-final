package ejercicio01;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Pantalla02View extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1131665365206329196L;
	private JTextField textField;
	private Pantalla01View pantalla1;
	private Pantalla03View pantalla3;
	private App controller;
	

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 * @param controller 
	 */
	public Pantalla02View(App controller) {
		this.controller = controller;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(100, 100, 450, 300);
		setLayout(null);
		textField = new JTextField();
		textField.setBounds(151, 49, 86, 20);
		add(textField);
		textField.setColumns(10);

		JButton btnNewButton = new JButton("Pantalla 1");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.irAPantalla1(textField.getText());
			}
		});
		btnNewButton.setBounds(65, 141, 97, 23);
		add(btnNewButton);
		

		JButton btnNewButton_1 = new JButton("Pantalla 2");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.irAPantalla2(textField.getText());
			}
		});
		btnNewButton_1.setBounds(218, 141, 97, 23);
		add(btnNewButton_1);
		btnNewButton_1.setEnabled(false);

		JButton btnNewButton_2 = new JButton("Pantalla 3");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.irAPantalla3(textField.getText());
			}
		});
		btnNewButton_2.setBounds(148, 193, 97, 23);
		add(btnNewButton_2);
	}

	public String getTextField() {
		return textField.getText();
	}

	public void setTextField(String texto) {
		this.textField.setText(texto);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
