package conexionNueva;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectorProvider {
	
	public Connection getNewConnection() throws SQLException {
		String usuario = "usuario";
		String password = "usuario";

		String url = "jdbc:mariadb://127.0.0.1:3306/Sakila";
		String driverClass = "org.mariadb.jdbc.Driver";
		
//		String driverClass = "oracle.jdbc.driver.OracleDriver";
//		String url = "jdbc:oracle:thin:@//172.16.39.200:1521/XE";

		try {
			Class.forName(driverClass);
		} catch (ClassNotFoundException e) {
			System.err.println("No se encuentra el driver JDBC. Revisa su configuracin");
			throw new RuntimeException(e);
		}

		Connection conn = DriverManager.getConnection(url, usuario, password);
		return conn;
	}
	

}
