package ceu.dam.examenes.serviceEj1;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.exceptions.NotFoundException;
import ceu.dam.examenes.modelo.Equipo;

public interface EquipoService {

	/** El método tiene que insertar en base de datos el equipo que recibe por parámetro.
	 * Insertará el equipo, su lista de jugadores y su entrenador. Todo.
	 * Si hay algún error, lanzará ErrorServiceException (con un mensaje explicativo y el origen de la excepción)
	 */
	public void crearEquipo (Equipo equipo) throws ErrorServiceException;

	
	/** El método recibe el código de un equipo. Tendrá que consultarlo en base de datos
	 * y devolver un objeto con todas sus entidades rellenas (incluyendo la lista de jugadores y
	 * el entrenador)
	 * Si no existe el equipo con ese código, lanzará NotFoundException.
	 * Si hay algún error, lanzará ErrorServiceException
	 */
	public Equipo consultarEquipo(String codigo) throws ErrorServiceException, NotFoundException; 
	
}
