package ejercicios;

import java.util.List;
import java.util.Scanner;

public class Ejercicio41 {

	public static void main(String[] args) {

		
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Diga el nombre del equipo");
		String nombreEquipo = leer.nextLine();
		Equipo equipo1 = new Equipo(nombreEquipo);
		cargarJugadores (leer, equipo1);
		System.out.println(equipo1);
		
		System.out.println("Diga el nombre del equipo");
		nombreEquipo = leer.nextLine();
		Equipo equipo2 = new Equipo(nombreEquipo);
		cargarJugadores (leer, equipo2);
		System.out.println(equipo2);
		
		Partido partido = new Partido();
		partido.setEquipoLocal(equipo1);
		partido.setEquipoVisitante(equipo2);
		Resultado resultado = new Resultado();
		partido.setResultado(resultado);
		System.out.println(partido);
		
		System.out.println("Dime los goles del equipo" + equipo1.getNombre());
		resultado.setGolesLocal(leer.nextInt());
		System.out.println("Dime los goles del equipo" + equipo1.getNombre());
		resultado.setGolesVisitante(leer.nextInt());
		
		System.out.println("Equipo Ganador: ");
		System.out.println(partido.getEquipoGanador());
		
		Jugador infiltrado = new Jugador("Blas Infiltrado", 9);
		equipo2.getJugadores().add(infiltrado);
		System.out.println(equipo2);
		
		List<Jugador> jugadoresEquipoLocal = equipo1.getJugadores();
		Integer tamaņoLista = jugadoresEquipoLocal.size();
		Jugador ultimoJugador = jugadoresEquipoLocal.get(tamaņoLista-1);
		equipo1.setCapitan(ultimoJugador);
		System.out.println(equipo1);
		
		leer.close();
	}
		
		
		
		private static void cargarJugadores(Scanner scanner, Equipo equipo) {
			for (int i = 0; i < 3; i++) {
				System.out.println("Dime el dorsal del jugador");
				Integer dorsalJugador = scanner.nextInt();
				scanner.nextLine();
				System.out.println("Dime el nombre del jugador");
				String nombreJugador = scanner.nextLine();
				
				Jugador jugador = new Jugador(nombreJugador, dorsalJugador);
				equipo.getJugadores().add(jugador);
				if (i==0) {
					equipo.setCapitan(jugador);
				}
			}
		

		

	}

}
