package service;

import java.util.ArrayList;
import java.util.List;

import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.InsertOneResult;

import connection.MongoUtil;
import modelo.Articulo;
import modelo.Ubicaciones;
import modelo.Usuario;

public class MongoService {

	MongoDatabase db = new MongoUtil().getDatabase("test");
	MongoCollection<Usuario> usuarios = db.getCollection("usuario", Usuario.class);
	MongoCollection<Ubicaciones> ubicaciones = db.getCollection("ubicacion", Ubicaciones.class);
	MongoCollection<Articulo> articulos = db.getCollection("articulo", Articulo.class);

	public void crearUsuario(Usuario usuario) {
		InsertOneResult r = usuarios.insertOne(usuario);
		System.out.println(r.getInsertedId().asObjectId().getValue());
	}

	public void crearUbicacion(Ubicaciones ubicacion) {
		InsertOneResult r = ubicaciones.insertOne(ubicacion);
		System.out.println(r.getInsertedId().asObjectId().getValue());
	}

	public void crearArticulo(Articulo articulo) {
		InsertOneResult r = articulos.insertOne(articulo);
		System.out.println(r.getInsertedId().asObjectId().getValue());
	}

	public void updateUsuario(Usuario usuario) {
		Bson filtro = Filters.or(Filters.regex("id", usuario.getIdentificacion().toString()));
		usuarios.replaceOne(filtro, usuario);
	}

	public void updateUbicacion(Ubicaciones ubicacion) {
		Bson filtro = Filters.or(Filters.regex("id", ubicacion.getId().toString()));
		ubicaciones.replaceOne(filtro, ubicacion);
	}

	public void updateArticulo(Articulo articulo) {
		Bson filtro = Filters.or(Filters.regex("id", articulo.getId().toString()));
		articulos.replaceOne(filtro, articulo);
	}

	public void deleteUsuario(Usuario usuario) {
		Bson filtro = Filters.or(Filters.regex("id", usuario.getIdentificacion().toString()));
		usuarios.deleteOne(filtro);
	}

	public void deleteUbicacion(Ubicaciones ubicacion) {
		Bson filtro = Filters.or(Filters.regex("id", ubicacion.getId().toString()));
		ubicaciones.deleteOne(filtro);
	}

	public void deleteArticulo(Articulo articulo) {
		Bson filtro = Filters.or(Filters.regex("id", articulo.getId().toString()));
		articulos.deleteOne(filtro);
	}

	public List<Articulo> filtrarUbicacion(Articulo articulo) {
		List<Articulo> articulosLista = new ArrayList<>();
		Bson filtro = Filters.or(Filters.regex("sku", articulo.getSku()));
		FindIterable<Articulo> it1 = articulos.find(filtro);
		MongoCursor<Articulo> cursor = it1.cursor();
		while (cursor.hasNext()) {
			Articulo a = cursor.next();
			articulosLista.add(a);
		}
		return articulosLista;
	}

	public Boolean consultarUsuario(Integer id, String contrasena) {
		Bson filtro = Filters.and(Filters.eq("id", id), Filters.eq("contrasena", contrasena));
		FindIterable<Usuario> it1 = usuarios.find(filtro);
		MongoCursor<Usuario> cursor = it1.cursor();
		if (cursor != null) {
			return true;
		}
		return false;
	}

	public Usuario devuelveUsuario(Integer identificacion) {
		Bson filtro = Filters.eq("identificacion", identificacion);
		FindIterable<Usuario> it1 = usuarios.find(filtro);
		Usuario u = it1.first();
		if (u != null) {
			return u;
		} else {
			return null;
		}

	}

}
