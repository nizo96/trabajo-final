package ceu.dam.examenes.test;

import java.util.ArrayList;
import java.util.Date;

import ceu.dam.examenes.modelo.Hospital;
import ceu.dam.examenes.modelo.Medico;
import ceu.dam.examenes.modelo.Provincia;

/**
 * Esta clase NO se ejecuta ni modifica. Es una clase que se utiliza en los otros test
 */
public class DatosBuilder {

	
	
	public static Hospital getHospital() {
		Hospital hospital = new Hospital();
		hospital.setDescripcion("Centro Curativo 1");
		hospital.setFechaConstruccion(new Date());
		hospital.setLocalizacion(new Provincia("SE", "SEVILLA"));
		hospital.setCuadroMedico(new ArrayList<>());
		hospital.getCuadroMedico().add(generarMedico(9000001, "Blau", "Cardiología", hospital));
		hospital.getCuadroMedico().add(generarMedico(9000002, "Blas", "Pediatría", hospital));
		hospital.getCuadroMedico().add(generarMedico(9000003, "Laura", "Cirugía", hospital));
		return hospital;
	}

	private static Medico generarMedico(int numero, String nombre, String especialidad, Hospital hospital) {
		Medico medico = new Medico();
		medico.setNombre(nombre);
		medico.setNumColegiado(numero);
		medico.setEspecialidad(especialidad);
		medico.setHospital(hospital);
		return medico;
	}
	
}
