package ejercicios;

import java.util.Scanner;

public class Ejercicio24 {

	public static void main(String[] args) {
		Integer numeroUsu;
		Integer suma;
		Integer resto;
		Integer contador = 1;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Indique un numero: ");
		numeroUsu = leer.nextInt();
		
		for (int i = 1; i < numeroUsu; i++) {
			resto = i % 2;
			if (resto == 1) {
				contador = contador + i;
				}
		}
		
		System.out.println(contador);
	}

}
