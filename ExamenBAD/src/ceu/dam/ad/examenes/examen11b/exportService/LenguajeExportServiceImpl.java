package ceu.dam.ad.examenes.examen11b.exportService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ceu.dam.ad.examenes.examen11b.exceptions.AccessDatabaseException;
import ceu.dam.ad.examenes.examen11b.exceptions.NotFoundException;
import ceu.dam.ad.examenes.examen11b.modelo.Lenguaje;
import ceu.dam.ad.examenes.examen11b.services.LenguajeServiceImpl;

public class LenguajeExportServiceImpl implements LenguajeExportService {
	private LenguajeServiceImpl service;
	private List<Lenguaje> lista;
	private FileWriter writer;

	@Override
	public void exportarLenguajes(String filtroNombre, String pathFileName)
			throws AccessDatabaseException {
		service = new LenguajeServiceImpl();
		lista = new ArrayList<>();
		try {
			File file = new File(pathFileName);
			writer = new FileWriter(file);
			lista = service.consultarLenguajes(filtroNombre);

			for (Lenguaje lenguaje : lista) {
				writer.write(Math.toIntExact(lenguaje.getId()));
				writer.write(";");
				writer.write(lenguaje.getName());
				writer.write(";");
				writer.write(lenguaje.getLastUpdate().toString());
				writer.write(";\n");
			}
			if (file.equals(null)) {
				throw new NotFoundException();
			}
			
		} catch (IOException | NotFoundException e) {
			throw new AccessDatabaseException();
		
		}

	}
}
