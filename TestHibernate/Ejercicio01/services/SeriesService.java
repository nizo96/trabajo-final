package services;

import java.util.List;

import modelo.Serie;

public interface SeriesService {

	/** Debe crear la serie y todas sus entidades asociadas en bbdd. Si hay alg�n error, lanzar� 
	 * SeriesServiceException. 
	 * Devolver� la serie creada con todos sus datos completos.
	 */
	public Serie crearSerie(Serie serie) throws SeriesServiceException;

	/** Consulta la serie con el id indicado por par�metro y la devuelve. Tiene que incluir todas sus entidades 
	 * asociadas. Si la serie no existe, se lanzar� SerieNotFoundException. Si hay cualquier otro error, se
	 * lanzar� SeriesServiceException
	 */
	public Serie consultarSerie(Long idSerie) throws SerieNotFoundException, SeriesServiceException;


	/** Consultar� todas las series que contengan en su descripci�n la palabra indicada en el filtro por
	 * par�metro. Si no se encuentra ninguna, lanzar� SerieNotFoundException. Si hay cualquier otro error, se
	 * lanzar� SeriesServiceException
	 */
	public List<Serie> buscarSeries(String filtroDescripcion) throws SerieNotFoundException, SeriesServiceException;


	/** Debe eliminar la serie con el id indicado, y todas sus entidades asociadas de bbdd. 
	 * Si hay alg�n error, lanzar� SeriesServiceException.
	 */
	public void elimnarSerie(Long idSerie) throws SeriesServiceException;

	/** Actualizar� la serie que se pasa por par�metro y todas las entidades asociadas que est�n modificadas. 
	 * Si hay alg�n error, lanzar� SeriesServiceException
	 */
	public void actualizarSerie(Serie serie) throws SeriesServiceException;

}