package ejercicios;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		String nombre;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("Escriba su nombre completo: ");
		nombre = leer.nextLine();
		
		String [] split = nombre.split(" ");
		
		for (int i = 0; i < split.length; i++) {
			System.out.println(split[i]);
		}
		leer.close();

	}

}
