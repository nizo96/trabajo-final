package ejercicio08;

public class Edicion {
	private Integer ao;
	private String editorial;
	
	@Override
	public String toString() {
		return "Edicion [ao=" + ao + ", editorial=" + editorial + "]";
	}


	public Edicion() {
	}

	public Edicion(Integer ao, String editorial) {
		super();
		this.ao = ao;
		this.editorial = editorial;
	}

	public Integer getAo() {
		return ao;
	}

	public void setAo(Integer ao) {
		this.ao = ao;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

}
