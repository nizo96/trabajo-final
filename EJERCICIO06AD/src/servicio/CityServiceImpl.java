package servicio;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cliente.City;
import cliente.NotFoundException;
import dao.CityDao;
import dao.ProveedorConexiones;

@RestController
public class CityServiceImpl {

	public CityServiceImpl() {
	}

	@GetMapping("/city")
	public List<City> getCities(@RequestParam(required = false) String filtroDescripcion) throws NotFoundException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getNewConnection();
			if (filtroDescripcion == null) {
				filtroDescripcion = "";
			}
			List<City> ciudades = new CityDao().buscarCiudades(conn, filtroDescripcion);
			if (ciudades.isEmpty()) {
				throw new NotFoundException("No se han encontrado ciudades con los filtros indicados");
			}
			return ciudades;
		} catch (SQLException e) {
			System.err.println("Error en la conexin con la bbdd");
			e.printStackTrace();
			// Puede que adems de escribir el error tuvieramos que lanzar una nueva excepcin
			// especfica
			return null;
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@GetMapping("/city/{id}")
	public City getCity(@PathVariable Long id) throws NotFoundException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getNewConnection();
			City ciudad = new CityDao().consultarCiudad(conn, id);
			if (ciudad == null) {
				throw new NotFoundException("No se ha encontrado la ciudad con id " + id);
			}
			return ciudad;
		} catch (SQLException e) {
			System.err.println("Error en la conexin con la bbdd");
			e.printStackTrace();
			// Puede que adems de escribir el error tuvieramos que lanzar una nueva excepcin
			// especfica
			throw new NotFoundException("No se ha encontrado la ciudad porque ha habido un error al consultar");
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@PostMapping("/city")
	public City createCity(@RequestBody City city) {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getNewConnection();
			new CityDao().insertarCiudad(conn, city);
			return city;
		} catch (SQLException e) {
			System.err.println("Error en la conexin con la bbdd");
			e.printStackTrace();
			// Puede que adems de escribir el error tuvieramos que lanzar una nueva excepcin
			// especfica
			return null;
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@PutMapping("/city")
	public void updateCity(@RequestBody City city) throws NotFoundException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getNewConnection();
			Integer numActualizados = new CityDao().updateCiudad(conn, city);
			if (numActualizados == 0) {
				throw new NotFoundException("No se ha podido borrar la ciudad con id " + city.getId());
			}
		} catch (SQLException e) {
			System.err.println("Error en la conexin con la bbdd");
			e.printStackTrace();
			// Puede que adems de escribir el error tuvieramos que lanzar una nueva excepcin
			// especfica
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@PatchMapping("/city")
	public City updateSelectiveCity(@RequestBody City city) throws NotFoundException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getNewConnection();
			if (city.getCountryId() != null || city.getDescripcion() != null) {
				Integer numActualizados = new CityDao().updateSelectivoCiudad(conn, city);
				if (numActualizados == 0) {
					throw new NotFoundException("No se ha podido borrar la ciudad con id " + city.getId());
				}
			}
			City ciudad = new CityDao().consultarCiudad(conn, city.getId());
			return ciudad;
		} catch (SQLException e) {
			System.err.println("Error en la conexin con la bbdd");
			e.printStackTrace();
			// Puede que adems de escribir el error tuvieramos que lanzar una nueva excepcin
			// especfica
			return null;
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@DeleteMapping("/city/{id}")
	public void deleteCity(@PathVariable Long id) throws NotFoundException {
		Connection conn = null;
		try {
			conn = new ProveedorConexiones().getNewConnection();
			Integer numActualizados = new CityDao().deleteCiudad(conn, id);
			if (numActualizados == 0) {
				throw new NotFoundException("No se ha podido borrar la ciudad con id " + id);
			}
		} catch (SQLException e) {
			System.err.println("Error en la conexin con la bbdd");
			e.printStackTrace();
			// Puede que adems de escribir el error tuvieramos que lanzar una nueva excepcin
			// especfica
		} finally {
			try {
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
