package examen;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Pollo extends Animal {
	public Integer numeroRegistro;
	public LocalDate fechaNac;
	public BigDecimal peso;
	public BigDecimal precioVenta;

	public Pollo(Integer numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
		this.precioVenta = new BigDecimal(1.5);
		this.peso = new BigDecimal(0);
		this.fechaNac = LocalDate.now();

	}

	public BigDecimal meterPeso(BigDecimal pesoCerdo) {
		this.peso = peso.add(pesoCerdo);
		return peso;
	}

	public BigDecimal calcularPrecio(BigDecimal pesoPollo) {
		this.pesoMinimo = new BigDecimal(5);
		Integer comparando = pesoPollo.compareTo(pesoMinimo);
		if (comparando >= 0) {
			return pesoPollo.multiply(precioVenta);
		} else {
			return null;
		}
	}

}
