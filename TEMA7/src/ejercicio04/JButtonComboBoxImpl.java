package ejercicio04;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

public class JButtonComboBoxImpl extends JPanel implements JButtonComboBox, ActionListener, ItemListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4699916545097723496L;
	public JButton enviar;
	public JComboBox<String> eleccion;

	public JButtonComboBoxImpl() {
		setLayout(null);

		eleccion = new JComboBox<>();
		eleccion.setBounds(0, 0, 56, 22);
		add(eleccion);
		eleccion.addItem("1");
		eleccion.addItem("2");
		eleccion.addItem("3");
		eleccion.addItem("4");

		enviar = new JButton("Enviar");
		enviar.setBounds(66, 0, 89, 23);
		add(enviar);

		enviar.addActionListener(this);

		eleccion.setSelectedIndex(-1);
		enviar.setEnabled(false);

		eleccion.addItemListener(this);
	}

	@Override
	public void clean() {
		eleccion.setSelectedIndex(-1);
		enviar.setEnabled(false);
	}

	@Override
	public String getValue() {
		String valor;
		valor = (String) eleccion.getSelectedItem();
		return valor;
	}

	@Override
	public void addValue(String value) {
		if (!(eleccion.getSelectedIndex() == -1)) {
			eleccion.addItem(value);
			enviar.setEnabled(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String pintar = (String) eleccion.getSelectedItem();
		System.out.println(pintar);

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (eleccion.isEnabled()) {
			enviar.setEnabled(true);
		}

	}
}
