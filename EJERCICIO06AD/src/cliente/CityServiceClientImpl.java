package cliente;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;



public class CityServiceClientImpl implements CityServiceClient{
	
	private RestTemplate restTemplate;
	private String urlBase;
	
	public CityServiceClientImpl(String urlBase, Integer msTimeout) {
		this.urlBase = urlBase;
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(msTimeout);
		requestFactory.setReadTimeout(msTimeout);
		this.restTemplate = new RestTemplate(requestFactory);
		
	}
	
	@Override
	public List<City> getCities(String filtroDescripcion) throws NotFoundException {
		try {
			String url = urlBase + "/city?filtroDescripcion=" + filtroDescripcion;
			City[] respuesta = restTemplate.getForObject(url, City[].class);
			return Arrays.asList(respuesta);
			
		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}
		
		
	}

	@Override
	public City getCity(Long id) throws NotFoundException {
		try {
			String url = urlBase + "/city/{id}";
			City respuesta = restTemplate.getForObject(url, City.class, id);
			return respuesta;
			
		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}
	}

	@Override
	public City createCity(City city) {
			String url = urlBase + "/city";
			city = restTemplate.patchForObject(url,city, City.class);
			return city;
	}

	@Override
	public void updateCity(City city) throws NotFoundException {
		try {
			String url = urlBase + "/city";
			restTemplate.put(url, city);
			
		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}
		
	}

	@Override
	public City updateSelectiveCity(City city) throws NotFoundException {
		try {
			String url = urlBase + "/city";
			city = restTemplate.patchForObject(url, city, City.class);
			return city;
			
		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}
		
	}

	@Override
	public void deleteCity(Long id) throws NotFoundException {
		try {
			String url = urlBase + "/city?id" + id;
			restTemplate.delete(url);
			
		} catch (HttpStatusCodeException e) {
			System.err.println("Error, fallo en la bbdd" + e.getMessage());
			e.printStackTrace();
			throw new NotFoundException();
		}
		
	}

}
