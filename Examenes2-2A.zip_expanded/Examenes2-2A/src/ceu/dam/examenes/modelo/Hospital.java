package ceu.dam.examenes.modelo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
@Entity
@Table(name = "hospitales")
public class Hospital {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_hospital")
	private Long id;
	
	private String descripcion;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "cod_provincia", nullable = true)
	private Provincia localizacion;
	
	@Column(name = "fecha_construccion")
	@JsonIgnore
	private Date fechaConstruccion;
	
	@OneToMany(mappedBy = "numColegiado", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Column(name = "num_colegiado")
	@JsonIgnore
	private List<Medico> cuadroMedico;
	
	public Hospital() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Provincia getLocalizacion() {
		return localizacion;
	}

	public void setLocalizacion(Provincia localizacion) {
		this.localizacion = localizacion;
	}

	public Date getFechaConstruccion() {
		return fechaConstruccion;
	}

	public void setFechaConstruccion(Date fechaConstruccion) {
		this.fechaConstruccion = fechaConstruccion;
	}

	public List<Medico> getCuadroMedico() {
		return cuadroMedico;
	}

	public void setCuadroMedico(List<Medico> cuadroMedico) {
		this.cuadroMedico = cuadroMedico;
	}

	@Override
	public String toString() {
		return "Hospital [id=" + id + ", descripcion=" + descripcion + ", localizacion=" + localizacion
				+ ", fechaConstruccion=" + fechaConstruccion + ", totalMedicos=" + cuadroMedico.size() + "]";
	}
	
	

}
