package ejercicio08;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class CreateXML extends DefaultHandler {
	private Boolean tagOpen = false;
	private String textoAcumulado = "";
	private List<Libro> lista;
	private Libro libroActual;
	private Edicion edicionActual;
	private List<Edicion> listaEdicion;
	private List<String> listaAutor;

	public CreateXML() {
		lista = new ArrayList<>();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		tagOpen = true;
		textoAcumulado = "";
		if (qName.equals("libro")) {
			libroActual = new Libro();
			Integer isbn = Integer.parseInt(attributes.getValue("isbn"));
			libroActual.setIsbn(isbn);
			lista.add(libroActual);
		} else if (qName.equals("autores")) {
			listaAutor = new ArrayList<>();
			libroActual.setAutores(listaAutor);
		} else if (qName.equals("ediciones")) {
			listaEdicion = new ArrayList<>();
			libroActual.setEdiciones(listaEdicion);
		} else if (qName.equals("edicion")) {
			edicionActual = new Edicion();
			listaEdicion.add(edicionActual);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
		tagOpen = false;
		if (qName.equals("titulo")) {
			libroActual.setTitulo(textoAcumulado);
		} else if (qName.equals("autor")) {
			listaAutor.add(textoAcumulado);
		} else if (qName.equals("a�o")) {
			edicionActual.setAo(Integer.parseInt(textoAcumulado));
			listaEdicion.add(edicionActual);
		} else if (qName.equals("editorial")) {
			edicionActual.setEditorial(textoAcumulado);
		}

	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		super.characters(ch, start, length);
		if (tagOpen) {
			String texto = new String(ch, start, length);
			textoAcumulado += texto;
		}
	}

	public List<Libro> getListaLibro() {
		return lista;
	}

}
