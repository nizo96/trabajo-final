package ejercicios;

import java.util.Scanner;

public class Calculadora {

	private static final String Sumar = "ADD";
	private static final String Restar = "RES";
	private static final String Multiplicar = "MUL";
	private static final String Dividir = "DIV";

	public static Integer sumar(Integer sumar1, Integer sumar2) {
		return (sumar1 + sumar2);
	}

	public static Integer restar(Integer restar1, Integer restar2) {
		return (restar1 - restar2);
	}

	public static Integer multiplicar(Integer multiplicar1, Integer multiplicar2) {
		return (multiplicar1 * multiplicar2);
	}

	public static Integer dividir(Integer dividir1, Integer dividir2) {
		return (dividir1 / dividir2);
	}

	public static Integer calcular(String operacion, Integer numero1, Integer numero2) {
		switch (operacion) {
		case Sumar:
			return sumar(numero1, numero2);
		case Restar:
			return restar(numero1, numero2);
		case Multiplicar:
			return multiplicar(numero1, numero2);
		case Dividir:
			return dividir(numero1, numero2);
		default:
			return 0;
		}

	}

public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		String operacion;
		Integer numero1;
		Integer numero2;

		do {
			System.out.println("Introduzca ADD, para sumar, RES para restar, MUL para multiplicar y DIV, para dividir");
			operacion = leer.nextLine();
			
			System.out.println("Introduzca un numero");
			numero1 = leer.nextInt();
			
			System.out.println("Introduzca otro numero");
			numero2 = leer.nextInt();
			
			leer.nextLine();
			
			
			} while (!operacion.equals("ADD") && !operacion.equals("RES") && !operacion.equals("MUL") && !operacion.equals("DIV"));
		
		System.out.println(calcular(operacion, numero1, numero2));
		
		leer.close();
			
		}

}
