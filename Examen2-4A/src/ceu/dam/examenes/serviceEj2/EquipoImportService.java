package ceu.dam.examenes.serviceEj2;

import java.util.List;

import ceu.dam.examenes.exceptions.ErrorServiceException;
import ceu.dam.examenes.modelo.Equipo;

public interface EquipoImportService {

	/** El método recibe la ruta a un fichero que tendrá que estar localizado ahí. 
	 * El fichero es un XML. El formato y estructura es igual al del ejemplo subido 
	 * en Moodle para este ejercicio.
	 * El servicio tendrá que leer los datos de ese XML y crear una lista de Equipo con ellos.
	 * Tendrá que devolver la lista creada.
	 * NO TIENES QUE LLAMAR A BASE DE DATOS PARA NADA (ni para consultar ni para crear)
	 * Para leer el XML podrás utilizar o DOM o SAX. Lo que prefieras. 
	 * Si hay cualquier error, lanzará ErrorServiceException
	 */
	public List<Equipo> importar(String filePath) throws ErrorServiceException;
	
}
