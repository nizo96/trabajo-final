package javafx.gui.controller;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		URL url = App.class.getResource("/javafx/gui/fxml/holamundo.fxml");
		FXMLLoader loader = new FXMLLoader(url);
		Scene scene = new Scene(loader.load(), 700, 500);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch();
	}

}
