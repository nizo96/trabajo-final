package ejercicios;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		SacoNumeros saco = new SacoNumeros();
		try {
			solicitarNumeros(leer, saco);

			System.out.println("Muchas gracias. Ahora vamos con la segunda parte...");

			solicitarPosiciones(leer, saco);

		} finally {
			leer.close();
			System.out.println("Scanner cerrado correctamente");
		}
	}

	private static void solicitarPosiciones(Scanner leer, SacoNumeros saco) {
		Integer posicion = 0;
		do {
			try {
				System.out.println("Dame una posicin");
				posicion = leer.nextInt();
				if (!posicion.equals(-1)) {
					System.out.println("Lo que hay en esa posicin es: " + saco.getNumero(posicion));
				}
			} catch (InputMismatchException e) {
				System.out.println("Slo se admiten nmeros");
				leer.nextLine();
			}
		} while (!posicion.equals(-1));
	}

	private static void solicitarNumeros(Scanner leer, SacoNumeros saco) {
		Integer numero = 0;
		do {
			try {
				System.out.println("Dame un nmero");
				numero = leer.nextInt();
				if (!numero.equals(-1)) {
					saco.addNumeros(numero);
				}
			} catch (InputMismatchException e) {
				System.out.println("Slo se admiten nmeros");
				leer.nextLine();
			}
		} while (!numero.equals(-1));
		System.out.println("Lista: " + saco);
	}

}
