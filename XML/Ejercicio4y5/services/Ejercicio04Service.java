package services;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import modelo.Edicion;
import modelo.Libro;

public class Ejercicio04Service {

	public void exportXML(List<Libro> libros, String pathFile)
			throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document xml = builder.newDocument();
		
		Element root = xml.createElement("libros");
		xml.appendChild(root);
		
		for (Libro libro : libros) {
			Element libroTag = xml.createElement("libro");
			libroTag.setAttribute("isbn", libro.getIsbn().toString());
			root.appendChild(libroTag);
			
			Element tituloTag = xml.createElement("titulo");
			tituloTag.setTextContent(libro.getTitulo());
			libroTag.appendChild(tituloTag);
			
			Element autoresTag = xml.createElement("autores");
		}
		
		
		
		
		
		
	}
}

