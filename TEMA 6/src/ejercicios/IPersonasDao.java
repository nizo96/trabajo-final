package ejercicios;

import java.sql.SQLException;
import java.util.List;

public interface IPersonasDao {

	/** Recibe un String con el dni de una persona. Debe consultar 
	 * en base de datos la persona con dicho DNI y devolver un objeto con 
	 * todos sus datos.
	 * @param dni
	 * @return Persona
	 * @throws SQLException
	 */
	public Persona consultarPersona(String dni) throws SQLException;
	
	
	/** Recibe un String con un filtro indicado por el usuario. Debe consultar en 
	 * base de datos todas las personas cuyo nombre o apellidos contengan esa cadena
	 * usando LIKE en la SQL. Devolverá una lista con todas las personas que cumplan
	 * este criterio.
	 * @param filtro
	 * @return Lista de Personas
	 * @throws SQLException
	 */
	public List<Persona> consultarPersonas(String filtro) throws SQLException;
}
