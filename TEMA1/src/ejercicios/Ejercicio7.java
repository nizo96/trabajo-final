package ejercicios;

import java.util.Scanner;
import java.lang.Math;

public class Ejercicio7 {

	public static void main(String[] args) {
		double peso;
		Scanner leer = new Scanner(System.in);
		
		System.out.println("�Cual es tu peso?");
		
		peso = leer.nextDouble();
		
		double altura;
		System.out.println("�Cual es tu altura?");
		
		altura = leer.nextDouble();
		
		double IMC = peso / Math.pow(altura,2);
		
		System.out.println("Su IMC es de: " + IMC);
	}

}
