package ejercicio04;

public interface JButtonComboBox {

	/** Hace que en el combobox no haya nada seleccionado */
	public void clean();
	
	/** Devuelve el valor seleccionado en el combobox. Si no hay nada, devuelve null */
	public String getValue();

	/** A�ade el valor indicado a la lista de posibles opciones del combobox */
	public void addValue(String value);

}
