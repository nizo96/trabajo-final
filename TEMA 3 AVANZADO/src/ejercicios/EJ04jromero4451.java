package ejercicios;

import java.util.Scanner;

public class EJ04jromero4451 {

	public static void main(String[] args) {
		Integer[] numeros = new Integer[] { 101, 202, 303, 404, 505 };
		Integer numUsuario;
		Scanner leer = new Scanner(System.in);
		Boolean condicion = false;
		do {

			System.out.println("Ponga un numero: ");
			numUsuario = leer.nextInt();

			for (int i = 0; i < numeros.length; i++) {
				if (numeros[i].equals(numUsuario)) {
					System.out.println("Esta en la posicion: " + i);
					condicion = true;
					break;
				} else {
					condicion = false;
					
				}
			}
			System.out.println("Vuelva a intentarlo");
		} while (condicion == true);
		
		leer.close(); 

	}

}
