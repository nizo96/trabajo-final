package persistencia;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.Articulo;
import modelo.ArticuloNormal;
import modelo.ArticuloSanitario;

public class ArticulosDao {

	public ArticulosDao() {

	}

	public void insertarArticulo(Connection conn, Articulo articulo) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement(
					"INSERT INTO ARTICULOS(CODIGO_BARRAS, DESCRIPCION, PRECIO_SIN_IVA, PORCENTAJE_IVA) VALUES (?, ?, ?, ?)");
			stmt.setString(1, articulo.getCodigoBarra());
			stmt.setString(2, articulo.getDescripcion());
			stmt.setBigDecimal(3, articulo.getPrecioSinIva());
			stmt.setBigDecimal(4, articulo.getIva());
			stmt.execute();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
	}

	public List<Articulo> buscarArticulos(Connection conn, BigDecimal precioInferior, BigDecimal precioSuperior)
			throws SQLException {
		Statement stmt = null;
		ResultSet rs = null;
		List<Articulo> nuevaLista = new ArrayList<>();

		try {
			stmt = conn.createStatement();
			String sql = "SELECT * FROM ARTICULOS WHERE PRECIO_SIN_IVA >" + precioInferior + "AND PRECIO_SIN_IVA <"
					+ precioSuperior;
			rs = stmt.executeQuery(sql);
			Articulo articulo;
			if (rs.getString("PORCENTAJE_IVA").equals("21")) {
				articulo = new ArticuloNormal();

			} else {
				articulo = new ArticuloSanitario();

			}

			while (rs.next()) {
				articulo.setCodigoBarra(rs.getString("CODIGO_BARRAS"));
				articulo.setDescripcion(rs.getString("DESCRIPCION"));
				articulo.setPrecioSinIva(rs.getBigDecimal("PRECIO_SIN_IVA"));
				nuevaLista.add(articulo);
			}

		} catch (SQLException e) {
			System.out.println("Hay un error");

		} finally {
			if (rs != null) {

				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}

		}

		return nuevaLista;

	}

}
