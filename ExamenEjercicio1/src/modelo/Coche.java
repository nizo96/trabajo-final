package modelo;

public class Coche {

	private String marca;
	private String modelo;
	private String a�o;
	private String matricula;
	private Integer precio;
	
	public Coche(String marca, String modelo, String a�o, String matricula, Integer precio) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.a�o = a�o;
		this.matricula = matricula;
		this.precio = precio;
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getA�o() {
		return a�o;
	}
	public void setA�o(String a�o) {
		this.a�o = a�o;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	
	

}
