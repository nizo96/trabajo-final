package ejercicios;

import java.util.Scanner;

public class Ejercicio12 {
	public static void main(String[] args) {
		Integer numUsuario;
		String lineaArriba = " __ ";
		String casillas = "|__";
		System.out.println("Ponga un n�mero para el tablero: ");
		Scanner leer = new Scanner(System.in);
		numUsuario = leer.nextInt();

		while (numUsuario < 1) {
			System.out.println("ERROR: El n�mero no puede ser menor a 1, vuelve a escribirlo: ");
			numUsuario = leer.nextInt();
		}

		for (int z = 1; z < numUsuario; z++) {
			casillas += "|__";
			lineaArriba += "__ ";

		}

		casillas += "|";

		System.out.println(lineaArriba);
		for (int i = 0; i < numUsuario; i++) {
			System.out.println(casillas);
		}

	}
}
