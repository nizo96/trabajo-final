package servicio;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import modelo.City;

public class ImportCSV {
	
	private Scanner leer;

	public void ImportarDatos(String url) {
		CityServiceClientImpl clientService = new CityServiceClientImpl("http://localhost:8080", 3000);
		List<City> lista = new ArrayList<>();
		String lectura;
		File file = new File(url);
		try {
			leer = new Scanner(file);
			while (leer.hasNext()) {
				lectura = leer.nextLine();
				String[] ciudad = lectura.split("\t");
				City ciudades = new City();
				ciudades.setDescripcion(ciudad[0]);
				ciudades.setCountryId(Long.parseLong(ciudad[1]));
				clientService.createCity(ciudades);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
