package ceu.dam.examenes.modelo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "medicos")
@JsonRootName(value = "medico")
public class Medico {
	@Id
	@Column(name = "num_colegiado")
	@JsonProperty("colegiado")
	private Integer numColegiado;
	
	private String nombre;
	
	private String especialidad;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_hospital")
	private Hospital hospital;
	
	public Medico() {
	}

	public Integer getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(Integer numColegiado) {
		this.numColegiado = numColegiado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	@Override
	public String toString() {
		return "Medico [numColegiado=" + numColegiado + ", nombre=" + nombre + ", especialidad=" + especialidad
				+ ", hospital=" + hospital + "]";
	}
	
	

}
