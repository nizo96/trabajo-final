package ejercicios;

public class ArrayUtils {
	public static void imprimirArray(String[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
	}

	public static Integer buscarPalabra(String[] array, String palabra) {
		for (Integer i = 0; i < array.length; i++) {
			if (array[i].equals(palabra)) {
				return i;
			}
		}
		return -1;
	}
	public static String obtenerPalabra(String[] array, Integer n) {
		String array1;
		array1 = (n >= array.length) ? "" : array[n];
		return array1;
	}
}
