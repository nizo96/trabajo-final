package service;

import java.util.List;

import modelo.City;

public interface CityService {

	/** Devuelve la lista de ciudades de la bbdd filtrando por la descripci�n indicada. 
	 * - El filtro se debe aplicar como un contiene. Es decir, si se filtra por 
	 *   "ara", resultados v�lidos ser�an: "arak", "okara", "caracas", etc.
	 * - Se debe invocar mediante un GET a la URL /city?filtroDescripcion=valor 
	 * - El par�metro filtroDescripci�n es opcional. Si no llega, no se aplicar� filtro.
	 * - Tendr� que devolver un 404 si la lista de resultados es vac�a
	 * @param filtroDescripcion - El par�metro se pasar� con este nombre 
	 * @return
	 * @throws NotFoundException
	 */
	public List<City> getCities(String filtroDescripcion) throws NotFoundException;
	
	/** Devuelve la ciudad con el id indicado. 
	 * - Se debe invocar mediante un GET a la URL /city/id (siendo "id" el par�metro) 
	 * - Tendr� que devolver un 404 si no existe ciudad con ese id
	 * @param id - formar� parte de la URL
	 * @return
	 * @throws NotFoundException
	 */
	public City getCity(Long id) throws NotFoundException;
	
	/** Crear� en base de datos la ciudad indicada por par�metro. Despu�s
	 * devolver� esa ciudad como respuesta.
	 * - Se debe invocar mediante un POST a la URL /city
	 * - El par�metro city vendr� en body de la petici�n
	 * @param city
	 * @return
	 */
	public City createCity(City city);
	
	/** Actualizar� en base de datos la ciudad indicada por par�metro. 
	 * Actualizar� todos los valores que lleguen en el objeto, aunque est�n a null
	 * - Se debe invocar mediante un PUT a la URL /city
	 * - El par�metro city vendr� en body de la petici�n
	 * - Tendr� que devolver un 404 si no existe ciudad con ese id
	 * @param city
	 * @throws NotFoundException
	 */
	public void updateCity(City city) throws NotFoundException;
	
	/** Actualizar� en base de datos la ciudad indicada por par�metro. 
	 * Actualizar� s�lo los valores que lleguen informados en el objeto, es decir,
	 * los atributos que vengan a NULL, no se actualizan 
	 * - Se debe invocar mediante un PATCH a la URL /city
	 * - El par�metro city vendr� en body de la petici�n
	 * - Tendr� que devolver un 404 si no existe ciudad con ese id
	 * - Si la actualizaci�n es correcta, devolver� el objeto city actualizado y completo
	 * @param city
	 * @return
	 * @throws NotFoundException
	 */
	public City updateSelectiveCity(City city) throws NotFoundException;
	
	/** Borrar� en base de datos la ciudad indicada por par�metro. 
	 * - Se debe invocar mediante un DELETE a la URL /city/id (siendo "id" el par�metro)
	 * - Tendr� que devolver un 404 si no existe ciudad con ese id
	 * @param id
	 * @throws NotFoundException
	 */
	public void deleteCity(Long id) throws NotFoundException;
	
}
