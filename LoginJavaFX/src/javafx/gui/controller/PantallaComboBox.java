package javafx.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class PantallaComboBox extends AppController {

	@FXML
	public TextField txField;

	@FXML
	public ComboBox<String> combo;

	@FXML
	public void initialize() {
		combo.getItems().add("Blas");
		combo.getItems().add("Blau");
		combo.getItems().add("Yokse");
	}

	@FXML
	public void limpiar() {
		txField.clear();
		combo.getSelectionModel().clearSelection();
	}

	@FXML
	public void imprimir() {
		txField.setText(combo.getSelectionModel().getSelectedItem());
	}

}
