package service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;



public class PeliculasService {
	@GetMapping("/")
	public List<Pelicula> consultarPeliculasCortas() throws PeliculasException {
		Connection conn = null;
		try {
			// 1. Crear una conexi�n
			conn = new ConnectionProvider().getNewConnection();

			// 2. Llamar al DAO para obtener lista de todas las pel�culas
			// pasando la conexi�n
			PeliculasDao dao = new PeliculasDao();
			List<Pelicula> peliculas = dao.consultarPeliculas(conn);

			// 4. Recorrer la lista y borrar las que tengan m�s de 100 min.
			Iterator<Pelicula> it = peliculas.iterator();
			while (it.hasNext()) {
				Pelicula pelicula = (Pelicula) it.next();
				if (pelicula.getLongitud() > 100) {
					it.remove();
				}
			}
			
			// 5. Devolver la lista
			return peliculas;
			
		} 
		catch (SQLException e) {
			System.err.println("Error accediendo o consultando en BBDD " + e.getMessage());
			throw new PeliculasException("Error accediendo o consultando en BBDD", e);
		} 
		finally {
			// 3. Cerrar conexi�n
			try {
				conn.close();
			} catch (Exception ignore) {
			}
		}

	}

}

