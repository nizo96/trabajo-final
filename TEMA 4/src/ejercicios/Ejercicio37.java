package ejercicios;

public class Ejercicio37 {

	public static void main(String[] args) {
		ColaCadenas cola = new ColaCadenas();
		System.out.println(cola);
		
		cola.aņadirCadena("primero");
		cola.aņadirCadena("segundo");
		System.out.println(cola);
		
		String elemento = cola.sacarCadena();
		System.out.println(elemento);
		System.out.println(cola);
		
		cola.aņadirCadena("tercero");
		System.out.println(cola);
		
		while (cola.getTamao() > 0) {
			elemento = cola.sacarCadena();
			System.out.println(elemento);
		}
		System.out.println(cola);
		
		cola.aņadirCadena("cuarto");
		System.out.println(cola);
	}

}
