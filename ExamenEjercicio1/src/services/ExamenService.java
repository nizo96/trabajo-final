package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.Coche;

public class ExamenService{

	/** Permite hacer login con el c�digo indicado. Si es correcto, no devuelve nada.
	 * Si es incorrecto, lanza la excepci�n AccesoDenegadoException.
	 * El c�digo correcto es: blas
	 * @param codigo
	 * @throws AccesoDenegadoException
	 */
	public void login(String codigo) throws AccesoDenegadoException {
		if ("blas".equals(codigo)) {
			return;
		}
		throw new AccesoDenegadoException("C�digo de identificaci�n incorrecto");
	}

	/** Devuelve la lista de coches de la marca indicada que est�n registrados en la "base de datos" simulada.
	 * Si no hay coches para la marca indicada, devolver� una lista vac�a 
	 * @param marca
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Coche> consultarCoches(String marca){
		if (marca == null) {
			return new ArrayList<Coche>();
		}
		marca = marca.toUpperCase().trim();
		if (!bbdd.containsKey(marca)) {
			return new ArrayList<Coche>();
		}
		ArrayList<Coche> coches = (ArrayList<Coche>) bbdd.get(marca);
		return (List<Coche>) coches.clone();
		
	}
	
	
	// Ignorar este c�digo...
	private static Map<String, List<Coche>> bbdd = new HashMap<String, List<Coche>>();
	static {
		List<Coche> toyota = new ArrayList<Coche>();
		toyota.add(new Coche("Toyota", "Auris", "2010", "1521FFD", 8532));
		toyota.add(new Coche("Toyota", "Auris", "2013", "4862DDD", 7896));
		toyota.add(new Coche("Toyota", "Rav4", "2016", "4862BRT", 15000));
		toyota.add(new Coche("Toyota", "Corolla", "2020", "8761DQT", 16100));
		toyota.add(new Coche("Toyota", "Yaris", "2012", "9661JKK", 4350));
		toyota.add(new Coche("Toyota", "Rav4", "2019", "4134NBV", 17211));
		List<Coche> seat = new ArrayList<Coche>();
		seat.add(new Coche("Seat", "C�rdoba", "2005", "2521RFD", 8000));
		seat.add(new Coche("Seat", "Ibiza", "2010", "4522RTD", 6000));
		seat.add(new Coche("Seat", "Le�n", "2011", "0114BRT", 15500));
		seat.add(new Coche("Seat", "C�rdoba", "2019", "8761DQT", 17100));
		seat.add(new Coche("Seat", "Le�n", "2020", "9661JKK", 4200));
		seat.add(new Coche("Seat", "Ibiza", "2019", "4134BBV", 17211));
		seat.add(new Coche("Seat", "C�rdoba", "2011", "1521FFD", 8532));
		seat.add(new Coche("Seat", "Ibiza", "2013", "4862DDD", 7896));
		seat.add(new Coche("Seat", "Le�n", "2016", "4862BRT", 15000));
		seat.add(new Coche("Seat", "Toledo", "2005", "2521RFD", 8000));
		seat.add(new Coche("Seat", "Ibiza", "2010", "4522RTD", 6000));
		seat.add(new Coche("Seat", "Toledo", "2011", "0114BRT", 15500));
		seat.add(new Coche("Seat", "C�rdoba", "2019", "8761DQT", 17100));
		seat.add(new Coche("Seat", "Toledo", "2020", "9661JKK", 4200));
		seat.add(new Coche("Seat", "Ibiza", "2019", "4134BBV", 17211));
		seat.add(new Coche("Seat", "C�rdoba", "2011", "1521FFD", 8532));
		seat.add(new Coche("Seat", "Le�n", "2016", "4862BRT", 15000));
		seat.add(new Coche("Seat", "Ibiza", "2013", "4862DDD", 7896));
		List<Coche> renault = new ArrayList<Coche>();
		renault.add(new Coche("Renault", "Laguna", "2017", "5562CRT", 3245));
		renault.add(new Coche("Renault", "Cl�o", "2020", "8551CQC", 11500));
		renault.add(new Coche("Renault", "Megane", "2008", "1555FPD", 12000));
		renault.add(new Coche("Renault", "Megane", "2003", "4662RWD", 10012));
		renault.add(new Coche("Renault", "Megane", "2010", "9001VVF", 6588));
		renault.add(new Coche("Renault", "Cl�o", "2009", "0034BWW", 1500));
		bbdd.put("TOYOTA", toyota);
		bbdd.put("SEAT", seat);
		bbdd.put("RENAULT", renault);
		
	}
	
	

	
}
