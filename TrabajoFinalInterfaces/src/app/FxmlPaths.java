package app;

public class FxmlPaths {
	private static final String PATH_BASE = "/javafx/gui/fxml/";
	public static final String FXML_LOGIN = PATH_BASE + "login.fxml";
	public static final String FXML_NUEVO= PATH_BASE + "nuevo.fxml";
	public static final String FXML_BIENVENIDA = PATH_BASE + "bienvenida.fxml";
	public static final String FXML_REGISTRO = PATH_BASE + "registro.fxml";
	public static final String FXML_TABLA = PATH_BASE + "tabla.fxml";
	public static final String FXML_TIPO = PATH_BASE + "tipo.fxml";
	public static final String FXML_DATOS = PATH_BASE + "datos.fxml";


}
