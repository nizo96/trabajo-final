package ejercicios;

import java.util.Scanner;

public class Ejercicio23 {

	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		Profesor profesor = new Profesor();
		
		System.out.println("Introduzca el nombre");
		profesor.setNombre(leer.nextLine()); 
		
		System.out.println("Introduce la edad");
		profesor.setEdad(leer.nextInt());
		
		System.out.println(profesor.getNombre());
		System.out.println(profesor.getEdad());
		
		leer.close();

	}

}
