package ejercicio08;

import java.io.File;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

public class XMLService extends DefaultHandler{
	
	public List<Libro> leerXML(String pathFile){
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser sax = factory.newSAXParser();
			
			CreateXML handler = new CreateXML();
			
			File file = new File(pathFile);
			sax.parse(file, handler);
			
			return handler.getListaLibro();
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	
	
	}

}
